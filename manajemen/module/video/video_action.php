<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "video";

if($_POST['mod']=="simpanData")
{
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_video = mysql_real_escape_string($_POST['nama_video']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$url = mysql_real_escape_string($_POST['url']);
	$publikasi = $_POST['publikasi'];
	
	if(empty($_POST['nama_video']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Video Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['url']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'URL Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_kelas, id_mapel, nama_video, keterangan, url, publikasi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$id_mapel', '$nama_video', '$keterangan', '$url', '$publikasi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Video";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_video' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_video = mysql_real_escape_string($_POST['nama_video']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$url = mysql_real_escape_string($_POST['url']);
	$publikasi = $_POST['publikasi'];
	
	if(empty($_POST['nama_video']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama video Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['url']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'URL Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_kelas = '$id_kelas', id_mapel = '$id_mapel', nama_video = '$nama_video', keterangan = '$keterangan', url = '$url', publikasi = '$publikasi', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Video";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_video' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="simpanSalinanData")
{
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_video = mysql_real_escape_string($_POST['nama_video']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$url = mysql_real_escape_string($_POST['url']);
	$publikasi = $_POST['publikasi'];
	
	if(empty($_POST['nama_video']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Video Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['url']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'URL Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_kelas, id_mapel, nama_video, keterangan, url, publikasi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$id_mapel', '$nama_video', '$keterangan', '$url', '$publikasi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Video";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_video' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_video FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_video'];
		
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
	$hapusPengumuman = mysql_query("DELETE FROM pengumuman WHERE jenis_pengumuman = '1' AND jenis_materi = '2' AND id_materi = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Video";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
	$hapusPengumuman = mysql_query("DELETE FROM pengumuman WHERE jenis_pengumuman = '1' AND jenis_materi = '2' AND id_materi IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Video Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Video Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="publikasiDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$publikasiDataTerpilih = mysql_query("UPDATE $nama_tabel SET publikasi = 'Y', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($publikasiDataTerpilih)
	{
		$aktivitas = "Publikasi Video Terpilih";
		$keterangan = mysql_real_escape_string("Mempublikasikan Video Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="sembunyikanDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$sembunyikanDataTerpilih = mysql_query("UPDATE $nama_tabel SET publikasi = 'N', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($sembunyikanDataTerpilih)
	{
		$aktivitas = "Sembunyikan Video Terpilih";
		$keterangan = mysql_real_escape_string("Menyembunyikan Video Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Video Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="simpanPengumuman")
{
	$id_materi = $_POST['id_video'];
	$nama_pengumuman = mysql_real_escape_string($_POST['nama_pengumuman']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tanggal_mulai = mysql_real_escape_string($_POST['tanggal_mulai']);
	$tanggal_selesai = mysql_real_escape_string($_POST['tanggal_selesai']);
	$penerima = $_POST['penerima'];
	$id_rombel = $_POST['id_rombel'];
	
	if(empty($_POST['nama_pengumuman']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Pengumuman Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$cekPengumuman = mysql_num_rows(mysql_query("SELECT * FROM pengumuman WHERE tanggal_mulai = '$tanggal_mulai' AND penerima = '$penerima' AND id_rombel = '$id_rombel' AND jenis_pengumuman = '1' AND jenis_materi = '2' AND id_materi = '$id_materi'"));
		if($cekPengumuman > 0)
		{
			$hapusData = mysql_query("DELETE FROM pengumuman WHERE tanggal_mulai = '$tanggal_mulai' AND penerima = '$penerima' AND id_rombel = '$id_rombel' AND jenis_pengumuman = '1' AND jenis_materi = '2' AND id_materi = '$id_materi'");
		}
		
		$simpanData = mysql_query("INSERT INTO pengumuman (nama_pengumuman, keterangan, tanggal_mulai, tanggal_selesai, penerima, id_rombel, jenis_pengumuman, jenis_materi, id_materi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_pengumuman', '$keterangan', '$tanggal_mulai', '$tanggal_selesai', '$penerima', '$id_rombel', '1', '2', '$id_materi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Pengumuman";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_pengumuman' Pada Tabel 'pengumuman'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Ditambahkan!');
				$('#form__').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}
?>