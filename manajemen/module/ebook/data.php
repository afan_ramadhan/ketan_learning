<?php
session_start();
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$nama_menu = "ebook";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$sTable = "ebook";
$sTableJoin1 = "kelas";
$sTableJoin2 = "mapel";
$sTableJoin3 = "user";

if($getHakAkses['s'] == 0)
{
	$aColumns = array('id', 'nama_kelas', 'nama_mapel', 'nama_ebook', 'keterangan', 'file', 'publikasi');
}
else
{
	$aColumns = array('id', 'nama_kelas', 'nama_mapel', 'nama_ebook', 'keterangan', 'file', 'publikasi', 'nama_lengkap');
}

$sIndexColumn = "$sTable.id";

$s = ($getHakAkses['s'] == 0 ? "AND $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
$ss = ($getHakAkses['s'] == 0 ? "WHERE $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sWhere = "WHERE
	$sTableJoin1.nama_kelas LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin2.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.nama_ebook LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.keterangan LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.file LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.publikasi LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	
	if($getHakAkses['s'] != 0)
	{
		$sWhere .= "OR $sTableJoin3.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	}
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 4)
		{
			$sWhere .= $sTable . "." . $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTableJoin1.nama_kelas, $sTableJoin2.nama_mapel, $sTable.nama_ebook, $sTable.keterangan, $sTable.file, $sTable.publikasi, $sTableJoin3.nama_lengkap
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_kelas = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.ditambah_oleh = $sTableJoin3.username
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_kelas = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.ditambah_oleh = $sTableJoin3.username";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>