<?php
$data_siswa = mysql_query("SELECT siswa.*, rombel.nama_rombel, rombel.id_kelas FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$_SESSION[id]'");
$ambil_data_siswa = mysql_fetch_array($data_siswa);
?>

<script>
	//Logout
	function logout(){
		var mod = "logout";
		$.ajax({
			type	: "POST",
			url		: "verifikasi.php",
			data	: "mod=" + mod,
			success: function(html)
			{
				$("#notifikasi").html(html);
			}
		})
	}
</script>

<header class="main-header">
	<nav class="navbar navbar-static-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="index.php" class="navbar-brand"><b><?=$lihat_konfigurasi['nama_aplikasi'];?></b> <small><?=$lihat_konfigurasi['versi'];?></small></a>
			</div>
			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li class="dropdown user user-menu">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<?php
							if($ambil_data_siswa['gambar']=="")
							{
								echo "<img class='user-image' src='manajemen/images/user_kosong.jpg'/>";
							}
							else
							{
								echo "<img class='user-image' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
							}
							?>
							<span class="hidden-xs"><?=$ambil_data_siswa['nama_lengkap'];?></span>
						</a>
						<ul class="dropdown-menu">
							<li class="user-header">
								<?php
								if($ambil_data_siswa['gambar']=="")
								{
									echo "<img class='img-circle' src='manajemen/images/user_kosong.jpg'/>";
								}
								else
								{
									echo "<img class='img-circle' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
								}
								?>
								<p>
									<?=$ambil_data_siswa['nama_lengkap'];?>
									<small><?=$ambil_data_siswa['username'];?></small>
								</p>
							</li>
							<li class="user-footer">
								<div class="pull-left">
									<a href="index.php" class="btn btn-default btn-flat">Beranda</a>
								</div>
								<div class="pull-right">
									<a href="#" class="btn btn-default btn-flat" onclick="logout()">Log out</a>
								</div>
							</li>
						</ul>
					</li>
					<!--li>
						<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
					</li-->
				</ul>
			</div>
		</div>
    </nav>
</header>

<div id="notifikasi"></div>