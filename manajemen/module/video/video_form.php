<?php
session_start();
include "../../config/database.php";

$nama_menu = "video";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = $getHakAkses['s'];

if($_POST['mod']=="editData" or $_POST['mod']=="salinData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM video WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['publikasi'] = "Y";
}
?>

<script>
	$(document).ready(function(){
		var mod = "ambilMapel";
		var id_video = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/video/video_response.php",
			data	: "mod=" + mod +
					  "&id_video=" + id_video +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
			}
		})
	})
	
	function ambilMapel()
	{
		var mod = "ambilMapel";
		var id_video = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/video/video_response.php",
			data	: "mod=" + mod +
					  "&id_video=" + id_video +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Video";}else if($_POST['mod'] == "salinData"){echo "Salin Video";}else{echo "Tambah Video";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kelas" onchange="ambilMapel()" required>
					<?php
					if($s == 1)
					{
						$kelas = mysql_query("SELECT * FROM kelas ORDER BY nama_kelas");
					}
					else
					{
						$kelas = mysql_query("SELECT kelas.* FROM kelas JOIN mapel_user ON kelas.id = mapel_user.id_kelas AND mapel_user.id_user = '$_SESSION[id]' GROUP BY mapel_user.id_kelas ORDER BY nama_kelas");
					}
					
					while($getKelas = mysql_fetch_array($kelas))
					{
						$selected = ($getData['id_kelas'] == $getKelas['id'] ? "selected" : "");
					?>
						<option value="<?=$getKelas['id'];?>" <?=$selected;?>><?=$getKelas['nama_kelas'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_mapel" required>
					<?php
					if($s == 1)
					{
						$mapel = mysql_query("SELECT * FROM mapel ORDER BY nama_mapel");
					}
					else
					{
						$mapel = mysql_query("SELECT mapel.* FROM mapel JOIN mapel_user ON mapel.id = mapel_user.id_mapel AND mapel_user.id_user = '$_SESSION[id]' ORDER BY nama_mapel");
					}
					
					while($getMapel = mysql_fetch_array($mapel))
					{
						$selected = ($getData['id_mapel'] == $getMapel['id'] ? "selected" : "");
					?>
						<option value="<?=$getMapel['id'];?>" <?=$selected;?>><?=$getMapel['nama_mapel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Video</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_video" maxlength="100" value="<?php if($_POST['mod']=="editData" or $_POST['mod']=="salinData"){echo $getData['nama_video'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="keterangan" maxlength="200" value="<?php if($_POST['mod']=="editData" or $_POST['mod']=="salinData"){echo $getData['keterangan'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">URL</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<?php
				if($_POST['mod']=="editData")
				{
					echo "
					<input type='text' class='form-control' id='url' value='$getData[url]' required/>";
				}
				else if($_POST['mod']=="salinData")
				{
					echo "
					<input type='text' class='form-control' id='url' value='$getData[url]' disabled required/>";
				}
				else
				{
					echo "
					<input type='text' class='form-control' id='url' required/>";
				}
				?>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Publikasi</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="radio" name="publikasi" value="Y" style="margin-right: 10px;" <?php if($getData['publikasi'] == "Y"){echo "checked";} ?>> Ya
				<input type="radio" name="publikasi" value="N" style="margin-left: 20px; margin-right: 10px;" <?php if($getData['publikasi'] == "N"){echo "checked";} ?>> Tidak
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else if($_POST['mod']=="salinData")
	{
		echo "<button type='button' class='btn btn-success' id='simpanSalinanData' onclick='simpanSalinanData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>