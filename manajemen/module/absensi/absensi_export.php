<?php
ob_start();
session_start();

include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$id = $_GET['id'];
	
$data = mysql_query("SELECT absensi.*, rombel.nama_rombel, mapel.nama_mapel, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 1) AS jumlah_hadir, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 2) AS jumlah_sakit, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 3) AS jumlah_izin, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 4) AS jumlah_alpa FROM absensi LEFT JOIN rombel ON absensi.id_rombel = rombel.id LEFT JOIN mapel ON absensi.id_mapel = mapel.id WHERE absensi.id = '$id'");
$getData = mysql_fetch_array($data);
	
$semester = ($getData['semester'] == 1 ? "Ganjil" : "Genap");
$id_rombel = $getData['id_rombel'];

$namafile = "absensi_detail_export";

if($_GET['format'] == "excel")
{
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=$namafile.xls");
}
?>
 
<!DOCTYPE html>

<html lang="id">

<head>
	
	<meta charset="utf-8" />
	
	<title><?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Absensi</title>
	
	<?php
	if($_GET['format'] == "excel")
	{
		echo "
		<style>
			table tr th {
				text-align: left;
				font-weight: bold;
			}
		</style>";
	}
	else
	{
		echo "
		<style>
			table {
				 border-collapse: collapse;
			}
			
			table tr th {
				padding: 5px;
				font-weight: bold;
			}
			
			table tr td {
				padding: 5px;
			}
		</style>";
	}
	?>

</head>

<body>
	
	<table>
		<tr>
			<th>Rombel</th>
			<th>:</th>
			<td><?=$getData['nama_rombel'];?></td>
		</tr>
		<tr>
			<th>Mapel</th>
			<th>:</th>
			<td><?=$getData['nama_mapel'];?></td>
		</tr>
		<tr>
			<th>Tahun Ajaran</th>
			<th>:</th>
			<td><font style="color:#FFFFFF;">'</font><?=$getData['tahun_ajaran'];?></td>
		</tr>
		<tr>
			<th>Semester</th>
			<th>:</th>
			<td><?=$semester;?></td>
		</tr>
		<tr>
			<th>Tanggal</th>
			<th>:</th>
			<td><?=tanggal_indonesia($getData['tanggal']);?></td>
		</tr>
		<tr>
			<th>Jam</th>
			<th>:</th>
			<td><?=substr($getData['jam'], 0, 5);?> WIB</td>
		</tr>
		<tr>
			<th>Pertemuan Ke</th>
			<th>:</th>
			<td><font style="color:#FFFFFF;">'</font><?=$getData['pertemuan_ke'];?></td>
		</tr>
		<tr>
			<th>Uraian Materi</th>
			<th>:</th>
			<td><?=$getData['uraian_materi'];?></td>
		</tr>
		<tr>
			<th>Total Hadir</th>
			<th>:</th>
			<td><?=$getData['jumlah_hadir'];?> Siswa</td>
		</tr>
		<tr>
			<th>Total Sakit</th>
			<th>:</th>
			<td><?=$getData['jumlah_sakit'];?> Siswa</td>
		</tr>
		<tr>
			<th>Total Izin</th>
			<th>:</th>
			<td><?=$getData['jumlah_izin'];?> Siswa</td>
		</tr>
		<tr>
			<th>Total Alpa</th>
			<th>:</th>
			<td><?=$getData['jumlah_alpa'];?> Siswa</td>
		</tr>
	</table>
	
	<h3>Daftar Siswa</h3>
	
	<table border="1">
		<tr>
			<th>No.</th>
			<th>NIS</th>
			<th>NISN</th>
			<th>Nama Lengkap</th>
			<th>Kehadiran</th>
			<th>Keterangan</th>
			<th>Sikap</th>
			<th>Catatan</th>
		</tr>
		
		<?php
		$kehadiran = array("", "Hadir", "Sakit", "Izin", "Alpa");
		$sikap = array("", "Sangat Baik", "Baik", "Cukup", "Kurang");
						
		$x = 1;
		$siswa = mysql_query("SELECT siswa.*, absensi_detail.kehadiran, absensi_detail.keterangan, absensi_detail.sikap, absensi_detail.catatan FROM siswa LEFT JOIN absensi_detail ON siswa.id = absensi_detail.id_siswa AND absensi_detail.id_absensi = '$id' WHERE siswa.id_rombel = '$id_rombel'");
		while($getSiswa = mysql_fetch_array($siswa))
		{
			echo "
			<tr>
				<td align='center'>$x</td>
				<td><font style='color:#FFFFFF;'>'</font>$getSiswa[nis]</td>
				<td><font style='color:#FFFFFF;'>'</font>$getSiswa[nisn]</td>
				<td>$getSiswa[nama_lengkap]</td>
				<td>".$kehadiran[$getSiswa['kehadiran']]."</td>
				<td>$getSiswa[keterangan]</td>
				<td>".$sikap[$getSiswa['sikap']]."</td>
				<td>$getSiswa[catatan]</td>
			</tr>";
			$x++;
		}
		?>				
	</table>
	
</body>

</html>

<?php
if($_GET['format'] == "pdf")
{
	$content = ob_get_clean();
	require_once(dirname(__FILE__).'/../../assets/plugins/html2pdf/html2pdf.class.php');
	try
	{
		$html2pdf = new HTML2PDF('P','A4','en', false, 'ISO-8859-15',array(10, 10, 10, 10));
		$html2pdf->setDefaultFont('Arial');
		$html2pdf->pdf->SetDisplayMode('fullpage');
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		$html2pdf->Output($namafile . ".pdf");
	}
	catch(HTML2PDF_exception $e)
	{
		echo $e;
	}
}
?>