<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";
include "../../libraries/fungsi_user_agent.php";

if($_POST['mod']=="tambahPengumuman")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT video.*, mapel.nama_mapel, kelas.id AS id_kelas FROM video LEFT JOIN mapel ON video.id_mapel = mapel.id LEFT JOIN kelas ON mapel.id_kelas = kelas.id WHERE video.id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script src="assets/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>	

<script>
	tinymce.init({
		selector: '.textEditor',
		
		<?php
		if($mobile == true)
		{
		?>
		
			height: 200,
			menubar: false,
			statusbar: false,
			toolbar: ' removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist ',
		
		<?php
		}
		else
		{
		?>
			
			height: 300,
			theme: 'modern',
			plugins: [
						'advlist autolink lists link image jbimages charmap print preview hr anchor pagebreak ',
						'searchreplace wordcount visualblocks visualchars code fullscreen',
						'insertdatetime media nonbreaking save table contextmenu directionality',
						'template paste textcolor colorpicker textpattern imagetools'
					],
			toolbar1: 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link media image jbimages | print preview ',
			relative_urls: false,
		
		<?php
		}
		?>
		
	});
	
	$(document).on('focusin', function(e){
		if ($(event.target).closest(".mce-window").length){
			e.stopImmediatePropagation();
		}
	});
	
	$(function(){
		$('#tanggal_mulai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_selesai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
	})
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Tambah Pengumuman</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama&nbsp;Pengumuman</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_pengumuman" maxlength="100" value="Tugas Materi Video <?=$getData['nama_mapel'];?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<textarea id="keterangan" class="textEditor">
					Silahkan pelajari materi video dengan judul <b><?=$getData['nama_video'];?></b>.
				</textarea>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Mulai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_mulai_">
					<input type="text" class="form-control" id="tanggal_mulai" value="<?=$tanggal_sekarang;?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Selesai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_selesai_">
					<input type="text" class="form-control" id="tanggal_selesai" value="<?=$tanggal_sekarang;?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_rombel" required>
					<?php
					$rombel = mysql_query("SELECT * FROM rombel WHERE id_kelas = '$getData[id_kelas]' ORDER BY nama_rombel");
					while($getRombel = mysql_fetch_array($rombel))
					{
					?>
						<option value="<?=$getRombel['id'];?>"><?=$getRombel['nama_rombel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanData" onclick="simpanPengumuman(<?=$id;?>)"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Simpan</button>
</div>