<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "video";
$sTableJoin1 = "mapel_user";
$sTableJoin2 = "mapel";
$sTableJoin3 = "user";
$sTableJoin4 = "jadwal_rombel";

if($mobile == false)
{
	$aColumns = array('nama_mapel', 'nama_video', 'keterangan', 'nama_lengkap', 'id');
}
else
{
	$aColumns = array('detail_video', 'id'); 
}


$sIndexColumn = "$sTable.id";

$siswa = mysql_fetch_array(mysql_query("SELECT id_rombel FROM siswa WHERE username = '$_SESSION[username]'"));

$s = (isset($_SESSION['username']) ? "AND $sTableJoin4.id_rombel = '$siswa[id_rombel]' AND $sTableJoin3.id IS NOT NULL AND $sTable.publikasi = 'Y'" : "");
$ss = (isset($_SESSION['username']) ? "WHERE $sTableJoin4.id_rombel = '$siswa[id_rombel]' AND $sTableJoin3.id IS NOT NULL AND $sTable.publikasi = 'Y'" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sWhere = "WHERE
	$sTableJoin2.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.nama_video LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.keterangan LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin3.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS DISTINCT $sTable.id, $sTableJoin2.nama_mapel, $sTable.nama_video, $sTable.keterangan, $sTableJoin3.nama_lengkap,
	CONCAT(
		'<tr>
			<td class=customHeader>Mapel</td>
			<td class=customHeader>:</td>
			<td>', nama_mapel, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Video</td>
			<td class=customHeader>:</td>
			<td>', nama_video, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Keterangan</td>
			<td class=customHeader>:</td>
			<td>', keterangan, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Guru</td>
			<td class=customHeader>:</td>
			<td>', nama_lengkap, '</td>
		</tr>'
	) AS detail_video
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_mapel = $sTableJoin1.id_mapel
	LEFT JOIN $sTableJoin2 ON $sTableJoin1.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTableJoin1.id_user = $sTableJoin3.id AND $sTable.ditambah_oleh = $sTableJoin3.username
	LEFT JOIN $sTableJoin4 ON $sTableJoin1.id = $sTableJoin4.id_mapel_user
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_mapel = $sTableJoin1.id_mapel
	LEFT JOIN $sTableJoin2 ON $sTableJoin1.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTableJoin1.id_user = $sTableJoin3.id AND $sTable.ditambah_oleh = $sTableJoin3.username
	LEFT JOIN $sTableJoin4 ON $sTableJoin1.id = $sTableJoin4.id_mapel_user";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>