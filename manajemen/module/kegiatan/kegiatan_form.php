<?php
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM kegiatan WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	$(function(){
		$('#tanggal_mulai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_selesai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#jam_mulai_').datetimepicker({
			format: 'HH:mm'
		});
		$('#jam_selesai_').datetimepicker({
			format: 'HH:mm'
		});
	})
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Kegiatan";}else{echo "Tambah Kegiatan";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Mulai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_mulai_">
					<input type="text" class="form-control" id="tanggal_mulai" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_mulai'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Selesai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_selesai_">
					<input type="text" class="form-control" id="tanggal_selesai" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_selesai'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jam Mulai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="jam_mulai_">
					<input type="text" class="form-control" id="jam_mulai" value="<?php if($_POST['mod']=="editData"){echo $getData['jam_mulai'];}else{echo $jam_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-clock-o"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jam Selesai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="jam_selesai_">
					<input type="text" class="form-control" id="jam_selesai" value="<?php if($_POST['mod']=="editData"){echo $getData['jam_selesai'];}else{echo $jam_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-clock-o"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Kegiatan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_kegiatan" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_kegiatan'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="keterangan" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['keterangan'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Warna</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="color" class="form-control" id="warna" maxlength="10" value="<?php if($_POST['mod']=="editData"){echo $getData['warna'];} ?>" required/>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>