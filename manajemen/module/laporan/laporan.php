<?php
$nama_menu = "laporan";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Laporan";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/laporan/laporan_database.php");
		})
		
		//Lihat Detail
		function lihatDetail(id)
		{
			var mod = "lihatDetail";
			var id_ujian = id;
			$.ajax({
				type	: "POST",
				url		: "module/laporan/laporan_form_detail.php",
				data	: "mod=" + mod +
						  "&id_ujian=" + id_ujian,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Lihat Detail
		function lihatAnalisis(id_ujian, id_siswa)
		{
			var mod = "lihatAnalisis";
			var id_ujian = id_ujian;
			var id_siswa = id_siswa;
			$.ajax({
				type	: "POST",
				url		: "module/laporan/laporan_form_analisis.php",
				data	: "mod=" + mod +
						  "&id_ujian=" + id_ujian +
						  "&id_siswa=" + id_siswa,
				success: function(html)
				{
					$("#formContent_").html(html);
					$("#form_").modal();
				}
			})
		}
	
		//Hapus Nilai
		function hapusNilai(id, id_siswa)
		{
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusNilai";
				var id = id;
				var id_siswa = id_siswa;
				$.ajax({
					type	: "POST",
					url		: "module/laporan/laporan_action.php",
					data	: "mod=" + mod +
							  "&id=" + id +
							  "&id_siswa=" + id_siswa,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$(".nilai_" + id_siswa).text("-");
						$("#lihatAnalisis_" + id_siswa).hide();
						$("#hapus_" + id_siswa).hide();
					}
				})
			}
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Laporan</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Laporan</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Laporan</h3>
						</div>
						<div class="box-body">
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog modal-lg">
									<div id="formContent" class="modal-content"></div>
								</div>
							</div>
							<div class="modal fade" id="form_" role="dialog">
								<div class="modal-dialog modal-lg">
									<div id="formContent_" class="modal-content"></div>
								</div>
							</div>
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Laporan</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Laporan</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>