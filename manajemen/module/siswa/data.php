<?php
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "siswa";
$sTableJoin = "rombel";
$aColumns = array('id', 'nama_rombel', 'nis', 'nisn', 'nama_lengkap', 'username', 'jenis_kelamin', 'agama', 'email', 'nomor_telepon', 'blokir'); 
$sIndexColumn = "$sTable.id";
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

$lakiLakiArray = array("l", "la", "lak", "laki", "laki ", "laki -", "laki - ", "laki - l", "laki - la", "laki - lak", "laki - laki");
$perempuanArray = array("p", "pe", "per", "pere", "perem", "peremp", "perempu", "perempua", "perempuan");

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(in_array($sKeyword, $lakiLakiArray))
	{
		$sValue = "L";
	}
	else if(in_array($sKeyword, $perempuanArray))
	{
		$sValue = "P";
	}
	else
	{
		$sValue = "@";
	}
			
	$sWhere = "WHERE
	$sTableJoin.nama_rombel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.nis LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.nisn LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.username LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.jenis_kelamin LIKE '%" . $sValue . "%' OR
	$sTable.agama LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.email LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.nomor_telepon LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' OR
	$sTable.blokir LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%'";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 6)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(in_array($bKeyword, $lakiLakiArray))
			{
				$bValue = "L";
			}
			else if(in_array($bKeyword, $perempuanArray))
			{
				$bValue = "P";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTableJoin.nama_rombel, $sTable.nis, $sTable.nisn, $sTable.nama_lengkap, $sTable.username, $sTable.jenis_kelamin, $sTable.agama, $sTable.email, $sTable.nomor_telepon, $sTable.blokir
	FROM
	$sTable
	LEFT JOIN $sTableJoin ON $sTable.id_rombel = $sTableJoin.id
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin ON $sTable.id_rombel = $sTableJoin.id";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>