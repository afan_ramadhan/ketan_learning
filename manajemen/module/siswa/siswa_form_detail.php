<?php
include "../../config/database.php";

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Siswa</h4>
</div>
<div class="modal-body">
	<table class="table table-stripped table-hover data">
		<tbody>
			<?php
			$data = mysql_query("SELECT siswa.*, rombel.nama_rombel, rombel.id_kelas FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$id'");
			$getData = mysql_fetch_array($data);

			$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki - Laki" : "Perempuan");
			
			if($getData['gambar']=="")
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/user_kosong.jpg' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			else
			{
				echo "
				<tr>
					<th colspan='3'>
						<center><img class='img-thumbnail' src='images/siswa/$getData[gambar]' style='width: 250px; margin: 20px;'/></center>
					</th>
				</tr>";
			}
			
			echo "
			<tr>
				<th style='width: 1px;'>NIS</th>
				<th style='width: 1px;'>:</th>
				<td>$getData[nis]</td>
			</tr>
			<tr>
				<th>NISN</th>
				<th>:</th>
				<td>$getData[nisn]</td>
			</tr>
			<tr>
				<th>Nama&nbsp;Lengkap</th>
				<th>:</th>
				<td>$getData[nama_lengkap]</td>
			</tr>
			<tr>
				<th>Username</th>
				<th>:</th>
				<td>$getData[username]</td>
			</tr>
			<tr>
				<th>Jenis&nbsp;Kelamin</th>
				<th>:</th>
				<td>".($getData['jenis_kelamin'] == "L" ? "Laki - Laki" : "Perempuan")."</td>
			</tr>
			<tr>
				<th>Tempat&nbsp;Lahir</th>
				<th>:</th>
				<td>$getData[tempat_lahir]</td>
			</tr>
			<tr>
				<th>Tanggal&nbsp;Lahir</th>
				<th>:</th>
				<td>$getData[tanggal_lahir]</td>
			</tr>
			<tr>
				<th>Agama</th>
				<th>:</th>
				<td>$getData[agama]</td>
			</tr>
			<tr>
				<th>Alamat</th>
				<th>:</th>
				<td>$getData[alamat]</td>
			</tr>
			<tr>
				<th>Email</th>
				<th>:</th>
				<td>$getData[email]</td>
			</tr>
			<tr>
				<th>Nomor&nbsp;Telepon</th>
				<th>:</th>
				<td>$getData[nomor_telepon] <a class='btn btn-success btn-sm' href='https://api.whatsapp.com/send?phone=".str_replace("08", "628", substr($getData['nomor_telepon'], 0, 2)).substr($getData['nomor_telepon'], 2)."' target='_blank' style='margin-left: 10px;'><i class='fa fa-whatsapp' aria-hidden='true' style='margin-right: 10px;'></i>Kirim Pesan</a></td>
			</tr>
			<tr>
				<th>Nama&nbsp;Ayah</th>
				<th>:</th>
				<td>$getData[nama_ayah]</td>
			</tr>
			<tr>
				<th>Nama&nbsp;Ibu</th>
				<th>:</th>
				<td>$getData[nama_ibu]</td>
			</tr>
			<tr>
				<th>Rombel</th>
				<th>:</th>
				<td>$getData[nama_rombel]</td>
			</tr>";
			?>
		
		</tbody>
		
	</table>
	
</div>
<div class="modal-footer">
</div>