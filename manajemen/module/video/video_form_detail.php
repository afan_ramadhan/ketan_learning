<?php
include "../../config/database.php";

if($_POST['mod']=="lihatStatusBelajar")
{
	$id = $_POST['id'];
}
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 1, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Status Belajar</h4>
</div>
<div class="modal-body">

	<div class="scrolling">
		<table class="table table-bordered table-hover data_custom">

			<thead>
				<tr>
					<th style="width: 1px;">No.</th>
					<th>NIS</th>
					<th>NISN</th>
					<th>Nama Lengkap</th>
					<th>Username</th>
					<th>Rombel</th>
					<th>Durasi</th>
					<th>Download</th>
					<th>Tanggal Dipelajari</th>
					<th>Jam Dipelajari</th>
					<th>Tanggal Terakhir Dipelajari</th>
					<th>Jam Terakhir Dipelajari</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$data = mysql_query("SELECT status_belajar.*, siswa.nis, siswa.nisn, siswa.nama_lengkap, siswa.username, rombel.nama_rombel FROM status_belajar JOIN siswa ON status_belajar.id_siswa = siswa.id JOIN rombel ON siswa.id_rombel = rombel.id WHERE jenis_materi = 'video' AND id_materi = '$id' ORDER BY rombel.nama_rombel, siswa.nis");
				while($getData = mysql_fetch_array($data))
				{
					echo "
					<tr>
						<td align='center'>$x</td>
						<td>$getData[nis]</td>
						<td>$getData[nisn]</td>
						<td>$getData[nama_lengkap]</td>
						<td>$getData[username]</td>
						<td>$getData[nama_rombel]</td>
						<td>$getData[durasi] Menit</td>
						<td>$getData[download] Kali</td>
						<td>$getData[tanggal_ditambah]</td>
						<td>$getData[jam_ditambah]</td>
						<td>$getData[tanggal_diperbarui]</td>
						<td>$getData[jam_diperbarui]</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
		</table>
	</div>
	
</div>
<div class="modal-footer">
</div>