<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_waktu.php";

$nama_tabel = "siswa";

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nis = mysql_real_escape_string($_POST['nis']);
	$nisn = mysql_real_escape_string($_POST['nisn']);
	$nama_lengkap = mysql_real_escape_string($_POST['nama_lengkap']);
	$username = mysql_real_escape_string($_POST['username']);
	$password = md5(mysql_real_escape_string($_POST['password']));
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$tempat_lahir = mysql_real_escape_string($_POST['tempat_lahir']);
	$tanggal_lahir = mysql_real_escape_string($_POST['tanggal_lahir']);
	$agama = $_POST['agama'];
	$alamat = mysql_real_escape_string($_POST['alamat']);
	$email = mysql_real_escape_string($_POST['email']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$nama_ayah = mysql_real_escape_string($_POST['nama_ayah']);
	$nama_ibu = mysql_real_escape_string($_POST['nama_ibu']);
	$gambar = (isset($_FILES['gambar']['name']) ? $_FILES['gambar']['name'] : "");
	$tema = $_POST['tema'];
	
	if(empty($_POST['nama_lengkap']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Lengkap Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['username']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$siswa = mysql_query("SELECT username FROM siswa WHERE id = '$id'");
		$ambilSiswa = mysql_fetch_array($siswa);
		$cekSiswa = mysql_num_rows(mysql_query("SELECT username FROM siswa WHERE username = '$username'"));
		if($ambilSiswa['username'] != $username and $cekSiswa > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Sudah Digunakan!');
			</script>";
		}	
		else
		{
			$perbaruiSaran = mysql_query("UPDATE saran SET ditambah_oleh = '$username' WHERE ditambah_oleh = '$ambilUser[username]'");
			
			$perbaruiData = mysql_query("UPDATE $nama_tabel SET nis = '$nis', nisn = '$nisn', nama_lengkap = '$nama_lengkap', username = '$username', jenis_kelamin = '$jenis_kelamin', tempat_lahir = '$tempat_lahir', tanggal_lahir = '$tanggal_lahir', agama = '$agama', alamat = '$alamat', email = '$email', nomor_telepon = '$nomor_telepon', nama_ayah = '$nama_ayah', nama_ibu = '$nama_ibu', tema = '$tema', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
			if(!empty($_FILES['gambar']['name']))
			{
				$ambilSiswa = mysql_fetch_array(mysql_query("SELECT * FROM $nama_tabel WHERE id = '$id'"));
			
				if($ambilSiswa['gambar'] != "")
				{
					unlink ("../../manajemen/images/siswa/$ambilSiswa[gambar]");		
				}
			
				function UploadGambar($gambar)
				{
					$vdir_upload = "../../manajemen/images/siswa/";
					$vfile_upload = $vdir_upload . $gambar;
					
					move_uploaded_file($_FILES["gambar"]["tmp_name"], $vfile_upload);
				}
					
				$lokasi_file = $_FILES['gambar']['tmp_name'];
				$nama_file = $_FILES['gambar']['name'];
				$acak = rand(1,99);
				$nama_file_unik = $acak . $nama_file;
				UploadGambar($nama_file_unik);
				
				mysql_query("UPDATE $nama_tabel SET gambar = '$nama_file_unik' WHERE id = '$id'");
			}
			
			if(!empty($_POST['password']))
			{
				mysql_query("UPDATE $nama_tabel SET password = '$password' WHERE id = '$id'");
			}
			
			if($perbaruiData)
			{
				$aktivitas = "Perbarui Siswa";
				$keterangan = mysql_real_escape_string("Memperbarui '$nama_lengkap' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Profil Diperbarui!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
				</script>
				";
			}
		}
	}
}
?>