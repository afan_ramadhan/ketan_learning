<?php
session_start();
include "manajemen/config/database.php";
include "manajemen/libraries/fungsi_waktu.php";

if($_POST['mod']=="login")
{	
	$username = mysql_real_escape_string($_POST['username']);
	$password = mysql_real_escape_string($_POST['password']);
	$password = md5($password);
	
	$data_siswa = mysql_query("SELECT * FROM siswa WHERE username = '$username'");
	$ambil_data_siswa = mysql_fetch_array($data_siswa);
	
	if($ambil_data_siswa['blokir'] == "Y")
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Sedang Dinonaktifkan!');
		</script>";
	}
	else if($password == $ambil_data_siswa['password'])
	{
		$_SESSION['id'] = $ambil_data_siswa['id'];
		$_SESSION['username'] = $ambil_data_siswa['username'];
		$_SESSION['level'] = "siswa";
		
		$aktivitas = "Login";
		$keterangan = mysql_real_escape_string("Login Siswa '$_SESSION[username]'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Berhasil Login!').set({onshow: null, onclose: function(){window.location.href='index.php'}});
		</script>";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Atau Password Salah!');
		</script>";
	}
}

if($_POST['mod']=="loginMobile")
{	
	echo "
	<!DOCTYPE html>
	
	<html>
	
		<head></head>
		
		<style>
			body {
				background-color: #3C8DBC;
			}
		</style>
		
		<body>";
	
	$username = mysql_real_escape_string($_POST['username']);
	$password = mysql_real_escape_string($_POST['password']);
	$password = md5($password);
	
	$data_siswa = mysql_query("SELECT * FROM siswa WHERE username = '$username'");
	$ambil_data_siswa = mysql_fetch_array($data_siswa);
	
	if($ambil_data_siswa['blokir'] == "Y")
	{
		echo "
		<script>
			alert('Username Sedang Dinonaktifkan!');
			window.location.href='login.php'
		</script>";
	}
	else if($password == $ambil_data_siswa['password'])
	{
		$_SESSION['id'] = $ambil_data_siswa['id'];
		$_SESSION['username'] = $ambil_data_siswa['username'];
		$_SESSION['level'] = "siswa";
		
		$aktivitas = "Login";
		$keterangan = mysql_real_escape_string("Login Siswa '$_SESSION[username]'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alert('Berhasil Login!');
			window.location.href='index.php'
		</script>";
	}
	else
	{
		echo "
		<script>
			alert('Username Atau Password Salah!');
			window.location.href='login.php'
		</script>";
	}
	
	echo "
		</body>
		
	</html>";
}

if($_POST['mod']=="logout")
{	
	$aktivitas = "Logout";
	$keterangan = mysql_real_escape_string("Logout Siswa '$_SESSION[username]'");
	$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
	session_destroy();

	echo "
	<script>
		alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Berhasil Logout!').set({onshow: null, onclose: function(){window.location.href='login.php'}});
	</script>";
}
?>