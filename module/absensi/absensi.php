<?php
if(isset($_SESSION['username']))
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | Absensi";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/absensi/absensi_database.php");
		})
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Absensi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Absensi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Absensi</h3>
						</div>
						<div class="box-body">
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog modal-lg">
									<div id="formContent" class="modal-content"></div>
								</div>
							</div>
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>absensi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">absensi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="manajemen/images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>