<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px; vertical-align: middle !important;}" : "");
?>

<style>
	<?=$mobileSize;?>
	
	.customHeader {
		font-weight: bold;
		padding-right: 10px;
	}
</style>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 0, "desc" ]],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/laporan/data.php",
			"aoColumns": [
				<?php
				if($mobile == false)
				{
				?>
					null,
					null,
					null,
					{
						"mData": "3",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Tugas Harian";
							}
							else if(data == 2)
							{
								return "Ulangan Harian";
							}
							else if(data == 3)
							{
								return "Ujian Tengah Semester (UTS)";
							}
							else if(data == 4)
							{
								return "Ujian Akhir Semester (UAS)";
							}
							else if(data == 5)
							{
								return "Try Out USBN";
							}
							else if(data == 6)
							{
								return "Ujian Sekolah Berstandar Nasional (USBN)";
							}
						}
					},
					null,
					{
						"mData": "5",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Ganjil";
							}
							else if(data == 2)
							{
								return "Genap";
							}
						}
					},
					null,
					null,
					null,
					null,
					{
						"mData": "10",
						"mRender": function(data, type, full)
						{
							var nilai = parseInt(data);
							var kkm = parseInt(full[11]);
							
							if(nilai >= kkm)
							{
								return "<p class='text-success'>" + data + "</p>";
							}
							else
							{
								return "<p class='text-danger'>" + data + "</p>";
							}
						}
					},
					null,
					{
						"mData": "12",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<button type='button' class='btn btn-info btn-sm' onclick='lihatAnalisis(" + data + ")'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>Lihat Analisis</button>";
							
							return aksi;
						}
					}
				<?php
				}
				else
				{
				?>
					{
						"mData": "0",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<table>" + data + "</table>";
							
							return aksi;
						}
					},
					{
						"mData": "1",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<button type='button' class='btn btn-info btn-sm' onclick='lihatAnalisis(" + data + ")'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>Lihat Analisis</button>";
							
							return aksi;
						}
					}
				<?php
				}
				?>
			]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<?php
			if($mobile == false)
			{
			?>
				<th>Tanggal Ujian</th>
				<th>Kelas</th>
				<th>Rombel</th>
				<th>Jenis Ujian</th>
				<th>Tahun Ajaran</th>
				<th>Semester</th>
				<th>Mapel</th>
				<th>Benar</th>
				<th>Salah</th>
				<th>Kosong</th>
				<th>Nilai</th>
				<th>KKM</th>
			<?php
			}
			else
			{
			?>
				<th>Laporan</th>
			<?php
			}
			?>
			
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<?php
	if($mobile == false)
	{
	?>
		<tfoot>
			<tr>
				<th class="filter">Tanggal Ujian</th>
				<th class="filter">Kelas</th>
				<th class="filter">Rombel</th>
				<th class="filter">Jenis Ujian</th>
				<th class="filter">Tahun Ajaran</th>
				<th class="filter">Semester</th>
				<th class="filter">Mapel</th>
				<th class="filter">Benar</th>
				<th class="filter">Salah</th>
				<th class="filter">Kosong</th>
				<th class="filter">Nilai</th>
				<th class="filter">KKM</th>
				<th>#</th>
			</tr>
		</tfoot>
	<?php
	}
	?>
	
</table>