<?php
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]'");
while($getHakAkses = mysql_fetch_array($hakAkses))
{
	$dashboardWidget[$getHakAkses['nama_menu']]['r'] = $getHakAkses['r'];
	$dashboardWidget[$getHakAkses['nama_menu']]['s'] = $getHakAkses['s'];
}

$filterRombel = ($dashboardWidget['rombel']['s'] == 1 ? "" : "WHERE id_user = '$_SESSION[id]'");
$filterAbsensi = ($dashboardWidget['absensi']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");
$filterEbook = ($dashboardWidget['ebook']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");
$filterVideo = ($dashboardWidget['ebook']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");
$filterPaketSoal = ($dashboardWidget['paket_soal']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");
$filterUjian = ($dashboardWidget['ujian']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");
$filterPengumuman = ($dashboardWidget['pengumuman']['s'] == 1 ? "" : "WHERE ditambah_oleh = '$_SESSION[username]'");

$jumlahKelas = mysql_num_rows(mysql_query("SELECT * FROM kelas"));
$jumlahMapel = mysql_num_rows(mysql_query("SELECT * FROM mapel"));
$jumlahRombel = mysql_num_rows(mysql_query("SELECT * FROM rombel $filterRombel"));
$jumlahJadwalKelas = mysql_num_rows(mysql_query("SELECT * FROM jadwal_rombel"));
$jumlahSiswa = mysql_num_rows(mysql_query("SELECT * FROM siswa"));
$jumlahAbsensi = mysql_num_rows(mysql_query("SELECT * FROM absensi $filterAbsensi"));
$jumlahEbook = mysql_num_rows(mysql_query("SELECT * FROM ebook $filterEbook"));
$jumlahVideo = mysql_num_rows(mysql_query("SELECT * FROM video $filterVideo"));
$jumlahPaketSoal = mysql_num_rows(mysql_query("SELECT * FROM paket_soal $filterPaketSoal"));
$jumlahUjian = mysql_num_rows(mysql_query("SELECT * FROM ujian $filterUjian"));
$jumlahKegiatan = mysql_num_rows(mysql_query("SELECT * FROM kegiatan"));
$jumlahPengumuman = mysql_num_rows(mysql_query("SELECT * FROM pengumuman $filterPengumuman"));
$jumlahLevel = mysql_num_rows(mysql_query("SELECT * FROM level"));
$jumlahUser = mysql_num_rows(mysql_query("SELECT * FROM user"));
$jumlahMapelUser = mysql_num_rows(mysql_query("SELECT * FROM mapel_user"));

function background($skin)
{
	if($skin == "skin-blue" or $skin == "skin-blue-light")
	{
		return "bg-aqua";
	}
	else if($skin == "skin-black" or $skin == "skin-black-light")
	{
		return "bg-gray";
	}
	else if($skin == "skin-purple" or $skin == "skin-purple-light")
	{
		return "bg-fuchsia";
	}
	else if($skin == "skin-green" or $skin == "skin-green-light")
	{
		return "bg-lime";
	}
	else if($skin == "skin-red" or $skin == "skin-red-light")
	{
		return "bg-maroon";
	}
	else if($skin == "skin-yellow" or $skin == "skin-yellow-light")
	{
		return "bg-red";
	}
}

$namaHari = array("1" => "Senin", "2" => "Selasa", "3" => "Rabu", "4" => "Kamis", "5" => "Jum'at", "6" => "Sabtu", "0" => "Minggu");
?>

<script language="JavaScript">
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Dashboard";
	}
	GantiTitle();
	
	var jadwalKegiatan = {
		"monthly": [
								
			<?php
			$dataKegiatan = mysql_query("SELECT * FROM kegiatan");
			while($ambilDataKegiatan = mysql_fetch_array($dataKegiatan))
			{
				echo "
				{
					'name': '$ambilDataKegiatan[nama_kegiatan]',
					'startdate': '$ambilDataKegiatan[tanggal_mulai]',
					'enddate': '$ambilDataKegiatan[tanggal_selesai]',
					'starttime': '$ambilDataKegiatan[jam_mulai]',
					'endtime': '$ambilDataKegiatan[jam_selesai]',
					'color': '$ambilDataKegiatan[warna]',
					'url': ''
				},";
			}
			?>
		
		]
	};

	$(window).load(function(){
		$('#jadwal_kegiatan').monthly({
			mode: 'event',
			dataType: 'json',
			events: jadwalKegiatan
		});
	});
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Dashboard</h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<?php
			$dataPengumuman = mysql_query("SELECT pengumuman.*, user.nama_lengkap FROM pengumuman JOIN user ON pengumuman.ditambah_oleh = user.username WHERE (pengumuman.penerima = 1 OR pengumuman.penerima = 2) AND (CURRENT_DATE() BETWEEN pengumuman.tanggal_mulai AND pengumuman.tanggal_selesai) ORDER BY pengumuman.id DESC");
			while($ambilDataPengumuman = mysql_fetch_array($dataPengumuman))
			{
				//$keterangan = str_replace("/uploads/", "manajemen/uploads/", $ambilDataPengumuman['keterangan']);
				$keterangan = $ambilDataPengumuman['keterangan'];
				
				echo "
				<div class='col-md-12'>
					<div class='alert alert-warning alert-dismissible'>
						<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
						<h4><i class='icon fa fa-info'></i>$ambilDataPengumuman[nama_pengumuman] ($ambilDataPengumuman[nama_lengkap])</h4>
						".str_replace("<img ", "<img style='max-width: 100% !important;' ", $keterangan)."
					</div>
				</div>";
			}
			?>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Total Data</h3>
					</div>
					<div class="box-body">
						
						<div class="row">
						
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['kelas']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahKelas;?></h3>
										<p>Total Kelas</p>
									</div>
									<div class="icon">
										<i class="fa fa-cube"></i>
									</div>
									<a href="?mod=kelas" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['mapel']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahMapel;?></h3>
										<p>Total Mapel</p>
									</div>
									<div class="icon">
										<i class="fa fa-briefcase"></i>
									</div>
									<a href="?mod=mapel" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['rombel']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahRombel;?></h3>
										<p>Total Rombel</p>
									</div>
									<div class="icon">
										<i class="fa fa-cubes"></i>
									</div>
									<a href="?mod=rombel" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['jadwal_rombel']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahJadwalKelas;?></h3>
										<p>Total Jadwal Rombel</p>
									</div>
									<div class="icon">
										<i class="fa fa-cubes"></i>
									</div>
									<a href="?mod=jadwal_rombel" class="small-box-footer">Lebih Detail<i class="fa fa-calendar-check-o" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['siswa']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahSiswa;?></h3>
										<p>Total Siswa</p>
									</div>
									<div class="icon">
										<i class="fa fa-users"></i>
									</div>
									<a href="?mod=siswa" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['absensi']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahAbsensi;?></h3>
										<p>Total Absensi</p>
									</div>
									<div class="icon">
										<i class="fa fa-calendar-check-o"></i>
									</div>
									<a href="?mod=absensi" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['ebook']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahEbook;?></h3>
										<p>Total Ebook</p>
									</div>
									<div class="icon">
										<i class="fa fa-book"></i>
									</div>
									<a href="?mod=ebook" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['video']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahVideo;?></h3>
										<p>Total Video</p>
									</div>
									<div class="icon">
										<i class="fa fa-file-video-o"></i>
									</div>
									<a href="?mod=video" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['paket_soal']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahPaketSoal;?></h3>
										<p>Total Paket Soal</p>
									</div>
									<div class="icon">
										<i class="fa fa-archive"></i>
									</div>
									<a href="?mod=paket_soal" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['ujian']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahUjian;?></h3>
										<p>Total Ujian</p>
									</div>
									<div class="icon">
										<i class="fa fa-flag-checkered"></i>
									</div>
									<a href="?mod=ujian" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['kegiatan']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahKegiatan;?></h3>
										<p>Total Kegiatan</p>
									</div>
									<div class="icon">
										<i class="fa fa-calendar"></i>
									</div>
									<a href="?mod=kegiatan" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['pengumuman']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahPengumuman;?></h3>
										<p>Total Pengumuman</p>
									</div>
									<div class="icon">
										<i class="fa fa-bullhorn"></i>
									</div>
									<a href="?mod=pengumuman" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['level']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahLevel;?></h3>
										<p>Total Level</p>
									</div>
									<div class="icon">
										<i class="fa fa-diamond"></i>
									</div>
									<a href="?mod=level" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['user']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahUser;?></h3>
										<p>Total User</p>
									</div>
									<div class="icon">
										<i class="fa fa-group"></i>
									</div>
									<a href="?mod=user" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
							
							<div class="col-lg-3 col-xs-6" style="<?=($dashboardWidget['mapel_user']['r'] == 1 ? "" : "display: none;");?>">
								<div class="small-box <?=background($ambilTema[0]);?>">
									<div class="inner">
										<h3><?=$jumlahMapelUser;?></h3>
										<p>Total Mapel User</p>
									</div>
									<div class="icon">
										<i class="fa fa-bookmark-o"></i>
									</div>
									<a href="?mod=mapel_user" class="small-box-footer">Lebih Detail<i class="fa fa-arrow-circle-right" style="margin-left: 10px;"></i></a>
								</div>
							</div>
        
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-6">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Jadwal Mengajar</h3>
					</div>
					<div class="box-body">
						<div class="scrolling">
							<table class="table table-bordered table-hover data_custom">

								<thead>
									<tr>
										<th style="width: 1px;">No.</th>
										<th>Hari</th>
										<th>Rombel</th>
										<th>Mapel</th>
									</tr>
								</thead>

								<tbody>

									<?php
									$x = 1;
									$jadwalRombel = mysql_query("SELECT jadwal_rombel.*, rombel.nama_rombel, mapel.nama_mapel FROM jadwal_rombel LEFT JOIN rombel ON jadwal_rombel.id_rombel = rombel.id LEFT JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id LEFT JOIN mapel ON mapel_user.id_mapel = mapel.id WHERE mapel_user.id_user = '$_SESSION[id]' ORDER BY jadwal_rombel.hari");
									while($getJadwalRombel = mysql_fetch_array($jadwalRombel))
									{
										echo "
										<tr>
											<td align='center'>$x</td>
											<td>".$namaHari[$getJadwalRombel['hari']]."</td>
											<td>$getJadwalRombel[nama_rombel]</td>
											<td>$getJadwalRombel[nama_mapel]</td>
										</tr>";
										$x++;
									}
									?>
													
								</tbody>
												
							</table>
						</div>
					</div>
				</div>
			</div>
		
			<div class="col-md-6">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Jadwal Kegiatan</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-lg-12 col-xs-12">
								<div id="jadwal_kegiatan" class="monthly"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
</div>