<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "kegiatan";

if($_POST['mod']=="simpanData")
{
	$tanggal_mulai = mysql_real_escape_string($_POST['tanggal_mulai']);
	$tanggal_selesai = mysql_real_escape_string($_POST['tanggal_selesai']);
	$jam_mulai = mysql_real_escape_string($_POST['jam_mulai']);
	$jam_selesai = mysql_real_escape_string($_POST['jam_selesai']);
	$nama_kegiatan = mysql_real_escape_string($_POST['nama_kegiatan']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$warna = $_POST['warna'];
	
	if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['nama_kegiatan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kegiatan Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (tanggal_mulai, tanggal_selesai, jam_mulai, jam_selesai, nama_kegiatan, keterangan, warna, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$tanggal_mulai', '$tanggal_selesai', '$jam_mulai', '$jam_selesai', '$nama_kegiatan', '$keterangan', '$warna', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Kegiatan";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_kegiatan' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kegiatan Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$tanggal_mulai = mysql_real_escape_string($_POST['tanggal_mulai']);
	$tanggal_selesai = mysql_real_escape_string($_POST['tanggal_selesai']);
	$jam_mulai = mysql_real_escape_string($_POST['jam_mulai']);
	$jam_selesai = mysql_real_escape_string($_POST['jam_selesai']);
	$nama_kegiatan = mysql_real_escape_string($_POST['nama_kegiatan']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$warna = $_POST['warna'];
	
	if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['nama_kegiatan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kegiatan Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET tanggal_mulai = '$tanggal_mulai', tanggal_selesai = '$tanggal_selesai', jam_mulai = '$jam_mulai', jam_selesai = '$jam_selesai', nama_kegiatan = '$nama_kegiatan', keterangan = '$keterangan', warna = '$warna', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Kegiatan";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_kegiatan' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kegiatan Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_kegiatan FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_kegiatan'];
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Kegiatan";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kegiatan Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Kegiatan Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Kegiatan Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kegiatan Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}
?>