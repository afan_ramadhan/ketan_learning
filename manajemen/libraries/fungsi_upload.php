<?php
function UploadGambar($upload_gambar_name)
{
	$vdir_upload = "../images/upload/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadGaleri($upload_gambar_name)
{
	$vdir_upload = "../images/galeri/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadSiswa($upload_gambar_name)
{
	$vdir_upload = "../images/siswa/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadPegawai($upload_gambar_name)
{
	$vdir_upload = "../images/pegawai/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadProfil($upload_gambar_name)
{
	$vdir_upload = "../images/data/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadUser($upload_gambar_name)
{
	$vdir_upload = "../images/user/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya

	$gambar_original = imagecreatefromjpeg($vfile_upload); //identitas file asli
	$src_width = imageSX($gambar_original);
	$src_height = imageSY($gambar_original);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
	
	$dst_width = 300; //Simpan dengan lebar 300 pixel
	$dst_height = ($dst_width/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_thumb = imagecreatetruecolor($dst_width,$dst_height); //proses perubahan ukuran
	imagecopyresampled($gambar_thumb, $gambar_original, 0, 0, 0, 0, $dst_width, $dst_height, $src_width, $src_height);

	imagejpeg($gambar_thumb,$vdir_upload . "thumb_" . $upload_gambar_name); //Simpan gambar dengan nama depan thumb_
	
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/	
 
	$dst_width2 = 900; //Simpan dengan lebar 900 pixel
	$dst_height2 = ($dst_width2/$src_width)*$src_height; //Set ukuran gambar hasil perubahan

	$gambar_optimized = imagecreatetruecolor($dst_width2,$dst_height2); //proses perubahan ukuran
	imagecopyresampled($gambar_optimized, $gambar_original, 0, 0, 0, 0, $dst_width2, $dst_height2, $src_width, $src_height);

	imagejpeg($gambar_optimized,$vdir_upload . "optimized_" . $upload_gambar_name); //Simpan gambar dengan nama depan optimized_
 
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
 
	imagedestroy($gambar_original); //Hapus gambar di memori komputer
	imagedestroy($gambar_thumb); //Hapus gambar di memori komputer
	imagedestroy($gambar_optimized); //Hapus gambar di memori komputer
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadHeader($upload_gambar_name)
{
	$vdir_upload = "../images/konfigurasi/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["gambar_header"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadBackground($upload_gambar_name)
{
	$vdir_upload = "../images/konfigurasi/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["background"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadFile($upload_file)
{
	$vdir_upload = "../file/"; //direktori file
	$vfile_upload = $vdir_upload . $upload_file;
	
	move_uploaded_file($_FILES["upload_file"]["tmp_name"], $vfile_upload); //Simpan file
}
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadSponsor($upload_gambar_name)
{
	$vdir_upload = "../images/sponsor/"; //direktori gambar
	$vfile_upload = $vdir_upload . $upload_gambar_name;
	
	move_uploaded_file($_FILES["upload_gambar"]["tmp_name"], $vfile_upload); //Simpan gambar dalam ukuran sebenarnya
}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/

function UploadLogo($upload_logo_name_1, $upload_logo_name_2)
{
	$vdir_upload_1 = "../images/jadwal_pertandingan/"; //direktori gambar
	$vfile_upload_1 = $vdir_upload_1 . $upload_logo_name_1;
	
	move_uploaded_file($_FILES["upload_logo_1"]["tmp_name"], $vfile_upload_1); //Simpan gambar dalam ukuran sebenarnya
	
	$vdir_upload_2 = "../images/jadwal_pertandingan/"; //direktori gambar
	$vfile_upload_2 = $vdir_upload_2 . $upload_logo_name_2;
	
	move_uploaded_file($_FILES["upload_logo_2"]["tmp_name"], $vfile_upload_2); //Simpan gambar dalam ukuran sebenarnya

}

/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*------------------------------------------------------------------------------------------------------------------------------------------------------------*/
?>
