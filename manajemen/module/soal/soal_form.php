<?php
include "../../config/database.php";
include "../../libraries/fungsi_user_agent.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM soal WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<script src="assets/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>	

<script>
	tinymce.init({
		selector: '.textEditor',
		
		<?php
		if($mobile == true)
		{
		?>
		
			height: 200,
			menubar: false,
			statusbar: false,
			toolbar: ' removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist ',
		
		<?php
		}
		else
		{
		?>
			
			height: 300,
			theme: 'modern',
			plugins: [
						'advlist autolink lists link image jbimages charmap print preview hr anchor pagebreak ',
						'searchreplace wordcount visualblocks visualchars code fullscreen',
						'insertdatetime media nonbreaking save table contextmenu directionality',
						'template paste textcolor colorpicker textpattern imagetools'
					],
			toolbar1: 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link media image jbimages | print preview ',
			relative_urls: false,
		
		<?php
		}
		?>
		
	});
	
	$(document).on('focusin', function(e){
		if ($(event.target).closest(".mce-window").length){
			e.stopImmediatePropagation();
		}
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Soal";}else{echo "Tambah Soal";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Bab</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="bab" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['bab'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pertanyaan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<textarea id="pertanyaan" class="textEditor"><?php if($_POST['mod']=="editData"){echo $getData['pertanyaan'];} ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">File</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="file" name="file"/>
				<p class="help-block">File Audio.</p>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pilhan A</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="pilihan_1" value="<?php if($_POST['mod']=="editData"){echo $getData['pilihan_1'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pilhan B</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="pilihan_2" value="<?php if($_POST['mod']=="editData"){echo $getData['pilihan_2'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pilhan C</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="pilihan_3" value="<?php if($_POST['mod']=="editData"){echo $getData['pilihan_3'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pilhan D</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="pilihan_4" value="<?php if($_POST['mod']=="editData"){echo $getData['pilihan_4'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pilhan E</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="pilihan_5" value="<?php if($_POST['mod']=="editData"){echo $getData['pilihan_5'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jawaban</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jawaban" required>
					<option value="1" <?php if($_POST['mod']=="editData"){echo ($getData['jawaban'] == 1 ? "selected" : "");} ?>>A</option>
					<option value="2" <?php if($_POST['mod']=="editData"){echo ($getData['jawaban'] == 2 ? "selected" : "");} ?>>B</option>
					<option value="3" <?php if($_POST['mod']=="editData"){echo ($getData['jawaban'] == 3 ? "selected" : "");} ?>>C</option>
					<option value="4" <?php if($_POST['mod']=="editData"){echo ($getData['jawaban'] == 4 ? "selected" : "");} ?>>D</option>
					<option value="5" <?php if($_POST['mod']=="editData"){echo ($getData['jawaban'] == 5 ? "selected" : "");} ?>>E</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Pembahasan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<textarea id="pembahasan" class="textEditor"><?php if($_POST['mod']=="editData"){echo $getData['pembahasan'];} ?></textarea>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>