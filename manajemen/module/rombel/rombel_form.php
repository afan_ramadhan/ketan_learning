<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM rombel WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Rombel";}else{echo "Tambah Rombel";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kelas" required>
					<?php
					$kelas = mysql_query("SELECT * FROM kelas ORDER BY nama_kelas");
					while($getKelas = mysql_fetch_array($kelas))
					{
						$selected = ($getData['id_kelas'] == $getKelas['id'] ? "selected" : "");
					?>
						<option value="<?=$getKelas['id'];?>" <?=$selected;?>><?=$getKelas['nama_kelas'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_rombel" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_rombel'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="keterangan" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['keterangan'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Wali Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_user" required>
					<?php
					$user = mysql_query("SELECT * FROM user ORDER BY nama_lengkap");
					while($getUser = mysql_fetch_array($user))
					{
						$selected = ($getData['id_user'] == $getUser['id'] ? "selected" : "");
					?>
						<option value="<?=$getUser['id'];?>" <?=$selected;?>><?=$getUser['nama_lengkap'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>