<?php
include "../../config/database.php";

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
}
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 0, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Rombel</h4>
</div>
<div class="modal-body">
	<h3 style="margin-top: 0;">Jadwal Rombel</h3>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#jadwal_rombel_1">Senin</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_2">Selasa</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_3">Rabu</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_4">Kamis</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_5">Jum'at</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_6">Sabtu</a></li>
		<li><a data-toggle="tab" href="#jadwal_rombel_7">Minggu</a></li>
	</ul>
	<div class="tab-content">
		<div id="jadwal_rombel_1" class="tab-pane fade in active">
			<h4>Senin</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_1">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas1 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '1' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas1 = mysql_fetch_array($jadwalKelas1))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas1[nama_mapel]</td>
									<td>$getJadwalKelas1[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_2" class="tab-pane fade">
			<h4>Selasa</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_2">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas2 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '2' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas2 = mysql_fetch_array($jadwalKelas2))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas2[nama_mapel]</td>
									<td>$getJadwalKelas2[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_3" class="tab-pane fade">
			<h4>Rabu</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_3">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas3 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '3' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas3 = mysql_fetch_array($jadwalKelas3))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas3[nama_mapel]</td>
									<td>$getJadwalKelas3[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_4" class="tab-pane fade">
			<h4>Kamis</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_4">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas4 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '4' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas4 = mysql_fetch_array($jadwalKelas4))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas4[nama_mapel]</td>
									<td>$getJadwalKelas4[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_5" class="tab-pane fade">
			<h4>Jum'at</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_5">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas5 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '5' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas5 = mysql_fetch_array($jadwalKelas5))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas5[nama_mapel]</td>
									<td>$getJadwalKelas5[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_6" class="tab-pane fade">
			<h4>Sabtu</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_6">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas6 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '6' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas6 = mysql_fetch_array($jadwalKelas6))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas6[nama_mapel]</td>
									<td>$getJadwalKelas6[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
		<div id="jadwal_rombel_7" class="tab-pane fade">
			<h4>Minggu</h4>
			<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
				<div id="jadwal_rombel_nilai_6">
					<table class="table table-bordered table-hover data_custom">

						<thead>
							<tr>
								<th style="width: 1px;">No.</th>
								<th>Mapel</th>
								<th>Guru</th>
							</tr>
						</thead>

						<tbody>

							<?php
							$x = 1;
							$jadwalKelas7 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.nama_lengkap FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$id' AND jadwal_rombel.hari = '0' ORDER BY jadwal_rombel.id");
							while($getJadwalKelas7 = mysql_fetch_array($jadwalKelas7))
							{
								echo "
								<tr>
									<td align='center'>$x</td>
									<td>$getJadwalKelas7[nama_mapel]</td>
									<td>$getJadwalKelas7[nama_lengkap]</td>
								</tr>";
								$x++;
							}
							?>
		
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<h3 style="margin-top: 0;">Daftar Siswa</h3>
	<div class="scrolling">
		<table class="table table-bordered table-hover data_custom">

			<thead>
				<tr>
					<th style="width: 1px;">No.</th>
					<th>NIS</th>
					<th>NISN</th>
					<th>Nama Lengkap</th>
					<th>Username</th>
					<th>Jenis Kelamin</th>
					<th>Agama</th>
					<th>Email</th>
					<th>Nomor Telepon</th>
					<th>Progres Belajar</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$data = mysql_query("SELECT * FROM siswa WHERE id_rombel = '$id' ORDER BY siswa.nama_lengkap");
				while($getData = mysql_fetch_array($data))
				{
					$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki - Laki" : "Perempuan");
					echo "
					<tr>
						<td align='center'>$x</td>
						<td>$getData[nis]</td>
						<td>$getData[nisn]</td>
						<td>$getData[nama_lengkap]</td>
						<td>$getData[username]</td>
						<td>$jenis_kelamin</td>
						<td>$getData[agama]</td>
						<td>$getData[email]</td>
						<td>$getData[nomor_telepon]</td>
						<td>
							<button type='button' class='btn btn-info btn-sm' onclick='progresBelajar($getData[id])' style='margin-bottom: 10px;'><i class='fa fa-tasks' aria-hidden='true' style='margin-right: 10px;'></i>Progres Belajar</button>
						</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
		</table>
	</div>
	
</div>
<div class="modal-footer">
</div>