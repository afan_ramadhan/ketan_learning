<header class="main-header">
    <a href="#" class="logo">
		<span class="logo-mini"><b><?=substr($lihat_konfigurasi['nama_aplikasi'], 0, 1);?></b></span>
		<span class="logo-lg"><b><?=$lihat_konfigurasi['nama_aplikasi'];?></b> <small><?=$lihat_konfigurasi['versi'];?></small></span>
    </a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<?php
						if($ambil_data_siswa['gambar']=="")
						{
							echo "<img class='user-image' src='manajemen/images/user_kosong.jpg'/>";
						}
						else
						{
							echo "<img class='user-image' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
						}
						?>
						<span class="hidden-xs"><?=$ambil_data_siswa['nama_lengkap'];?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<?php
							if($ambil_data_siswa['gambar']=="")
							{
								echo "<img class='img-circle' src='manajemen/images/user_kosong.jpg'/>";
							}
							else
							{
								echo "<img class='img-circle' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
							}
							?>
							<p>
								<?=$ambil_data_siswa['nama_lengkap'];?>
								<small><?=$ambil_data_siswa['username'];?></small>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a class="btn btn-default btn-flat waktu_ujian"></a>
							</div>
							<div class="pull-right">
								<a class="btn btn-default btn-flat" onclick="if(confirm('Akhiri Ujian?'))simpanNilai()">Akhiri Ujian</a>
							</div>
						</li>
					</ul>
				</li>
				<!--li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li-->
			</ul>
		</div>
    </nav>
</header>

<div id="notifikasi"></div>