<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "absensi";

if($_POST['mod']=="simpanData")
{
	$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
	$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

	$id_rombel = $_POST['id_rombel'];
	$id_mapel = $_POST['id_mapel'];
	$tahun_ajaran = $lihat_konfigurasi['tahun_ajaran'];
	$semester = $lihat_konfigurasi['semester'];
	$tanggal = mysql_real_escape_string($_POST['tanggal']);
	$jam = mysql_real_escape_string($_POST['jam']);
	$pertemuan_ke = mysql_real_escape_string($_POST['pertemuan_ke']);
	$uraian_materi = mysql_real_escape_string($_POST['uraian_materi']);
	
	if(empty($_POST['tanggal']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_rombel, id_mapel, tahun_ajaran, semester, tanggal, jam, pertemuan_ke, uraian_materi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_rombel', '$id_mapel', '$tahun_ajaran', '$semester', '$tanggal', '$jam', '$pertemuan_ke', '$uraian_materi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
		
		$id_absensi = mysql_insert_id();
		
		$kehadiran = $_POST['kehadiran'];
		$keterangan = $_POST['keterangan'];
		$sikap = $_POST['sikap'];
		$catatan = $_POST['catatan'];
		
		$arrayKe = 0;
		foreach($_POST['id_siswa'] as $key => $value)
		{
			$simpanAbsensiDetail = mysql_query("INSERT INTO absensi_detail (id_absensi, id_siswa, kehadiran, keterangan, sikap, catatan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_absensi', '$value', '".$kehadiran[$arrayKe]."', '".$keterangan[$arrayKe]."', '".$sikap[$arrayKe]."', '".$catatan[$arrayKe]."', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			$arrayKe++;
		}
			
		if($simpanData)
		{
			$aktivitas = "Tambah Absensi";
			$keterangan = mysql_real_escape_string("Menambahkan '$tanggal' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Absensi Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_rombel = $_POST['id_rombel'];
	$id_mapel = $_POST['id_mapel'];
	$tanggal = mysql_real_escape_string($_POST['tanggal']);
	$jam = mysql_real_escape_string($_POST['jam']);
	$pertemuan_ke = mysql_real_escape_string($_POST['pertemuan_ke']);
	$uraian_materi = mysql_real_escape_string($_POST['uraian_materi']);
	
	if(empty($_POST['tanggal']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_rombel = '$id_rombel', id_mapel = '$id_mapel', tanggal = '$tanggal', jam = '$jam', pertemuan_ke = '$pertemuan_ke', uraian_materi = '$uraian_materi', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		$id_absensi = $id;
		
		$kehadiran = $_POST['kehadiran'];
		$keterangan = $_POST['keterangan'];
		$sikap = $_POST['sikap'];
		$catatan = $_POST['catatan'];
		
		$hapusDataAbsensiDetail = mysql_query("DELETE FROM absensi_detail WHERE id_absensi = '$id_absensi'");
		
		$arrayKe = 0;
		foreach($_POST['id_siswa'] as $key => $value)
		{
			$simpanAbsensiDetail = mysql_query("INSERT INTO absensi_detail (id_absensi, id_siswa, kehadiran, keterangan, sikap, catatan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_absensi', '$value', '".$kehadiran[$arrayKe]."', '".$keterangan[$arrayKe]."', '".$sikap[$arrayKe]."', '".$catatan[$arrayKe]."', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			$arrayKe++;
		}
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Absensi";
			$keterangan = mysql_real_escape_string("Memperbarui '$tanggal' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Absensi Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT tanggal FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['tanggal'];
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
	$hapusDataAbsensiDetail = mysql_query("DELETE FROM absensi_detail WHERE id_absensi = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Absensi";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Absensi Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
	$hapusDataAbsensiDetailTerpilih = mysql_query("DELETE FROM absensi_detail WHERE id_absensi IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Absensi Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Absensi Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Absensi Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}
?>