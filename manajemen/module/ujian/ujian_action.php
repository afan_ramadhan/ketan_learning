<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "ujian";

if($_POST['mod']=="simpanData")
{
	$tanggal_ujian = mysql_real_escape_string($_POST['tanggal_ujian']);
	$jam_mulai = mysql_real_escape_string($_POST['jam_mulai']);
	$jam_selesai = mysql_real_escape_string($_POST['jam_selesai']);
	$jenis_ujian = $_POST['jenis_ujian'];
	$tahun_ajaran = $_POST['tahun_ajaran'];
	$semester = $_POST['semester'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$id_rombel = $_POST['id_rombel'];
	$id_paket_soal = $_POST['id_paket_soal'];
	$suspect_aktif = $_POST['suspect_aktif'];
	$acak_soal = $_POST['acak_soal'];
	
	if(empty($_POST['tanggal_ujian']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Ujian Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (tanggal_ujian, jam_mulai, jam_selesai, jenis_ujian, tahun_ajaran, semester, id_kelas, id_mapel, id_rombel, id_paket_soal, suspect_aktif, acak_soal, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$tanggal_ujian', '$jam_mulai', '$jam_selesai', '$jenis_ujian', '$tahun_ajaran', '$semester', '$id_kelas', '$id_mapel', '$id_rombel', '$id_paket_soal', '$suspect_aktif', '$acak_soal', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Ujian";
			$keterangan = mysql_real_escape_string("Menambahkan '$tanggal_ujian' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$tanggal_ujian = mysql_real_escape_string($_POST['tanggal_ujian']);
	$jam_mulai = mysql_real_escape_string($_POST['jam_mulai']);
	$jam_selesai = mysql_real_escape_string($_POST['jam_selesai']);
	$jenis_ujian = $_POST['jenis_ujian'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$id_rombel = $_POST['id_rombel'];
	$id_paket_soal = $_POST['id_paket_soal'];
	$suspect_aktif = $_POST['suspect_aktif'];
	$acak_soal = $_POST['acak_soal'];
	
	if(empty($_POST['tanggal_ujian']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Ujian Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['jam_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jam Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET tanggal_ujian = '$tanggal_ujian', jam_mulai = '$jam_mulai', jam_selesai = '$jam_selesai', jenis_ujian = '$jenis_ujian', id_kelas = '$id_kelas', id_mapel = '$id_mapel', id_rombel = '$id_rombel', id_paket_soal = '$id_paket_soal', suspect_aktif = '$suspect_aktif', acak_soal = '$acak_soal', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Ujian";
			$keterangan = mysql_real_escape_string("Memperbarui '$tanggal_ujian' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT tanggal_ujian FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['tanggal_ujian'];
	
	// $cekUjian = mysql_num_rows(mysql_query("SELECT id_ujian FROM nilai WHERE id_ujian = '$id'"));
	// if($cekUjian > 0)
	// {
		// echo "
		// <script>
			// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Sedang Digunakan Nilai!');
		// </script>";
	// }
	// else
	// {
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		$hapusDataKantong = mysql_query("DELETE FROM kantong_jawaban WHERE id_ujian = '$id'");
		$hapusDataNilai = mysql_query("DELETE FROM nilai WHERE id_ujian = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Ujian";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	// }
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	// $cekUjian = mysql_num_rows(mysql_query("SELECT id_ujian FROM nilai WHERE id_ujian IN ($data_terpilih)"));
	// if($cekUjian > 0)
	// {
		// echo "
		// <script>
			// alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Sedang Digunakan Nilai!');
		// </script>";
	// }
	// else
	// {
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		$hapusDataKantongTerpilih = mysql_query("DELETE FROM kantong_jawaban WHERE id_ujian IN ($data_terpilih)");
		$hapusDataNilaiTerpilih = mysql_query("DELETE FROM nilai WHERE id_ujian IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Ujian Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Ujian Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ujian Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	// }
}

if($_POST['mod']=="prosesNilai")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['tanggal_ujian'];
	
	$jumlahSoal = mysql_num_rows(mysql_query("SELECT * FROM soal WHERE id_paket_soal = '$ambilData[id_paket_soal]'"));
	
	$dataSiswa = mysql_query("SELECT * FROM siswa WHERE id_rombel = '$ambilData[id_rombel]' ORDER BY nama_lengkap");
	while($ambilDataSiswa = mysql_fetch_array($dataSiswa))
	{
		$queryNilaiTemp = "
		SELECT SUM(benar) AS jumlah_benar, SUM(salah) AS jumlah_salah
		FROM
			(SELECT
				CASE WHEN kantong_jawaban.jawaban_siswa = soal.jawaban THEN 1 ELSE 0 END AS benar,
				CASE WHEN kantong_jawaban.jawaban_siswa <> soal.jawaban THEN 1 ELSE 0 END AS salah
			FROM soal
			LEFT JOIN kantong_jawaban
				ON soal.id = kantong_jawaban.id_soal
				AND kantong_jawaban.id_ujian = '$id'
				AND kantong_jawaban.id_siswa = '$ambilDataSiswa[id]')
		AS nilai_temp";
		
		$kantongJawaban = mysql_fetch_array(mysql_query($queryNilaiTemp));
		
		$benar = $kantongJawaban['jumlah_benar'];
		$salah = $kantongJawaban['jumlah_salah'];
		$kosong = $jumlahSoal - $benar - $salah;
		$persentase = ($benar / $jumlahSoal ) * 100;
		
		$cekNilai = mysql_num_rows(mysql_query("SELECT * FROM nilai WHERE id_ujian = '$id' AND id_paket_soal = '$ambilData[id_paket_soal]' AND id_siswa = '$ambilDataSiswa[id]'"));
		if($cekNilai == 0)
		{
			mysql_query("INSERT INTO nilai(id_ujian, id_kelas, id_mapel, id_rombel, id_paket_soal, id_siswa, benar, salah, kosong, persentase, tanggal_ditambah, jam_ditambah, ditambah_oleh) values('$id', '$ambilData[id_kelas]', '$ambilData[id_mapel]', '$ambilData[id_rombel]', '$ambilData[id_paket_soal]', '$ambilDataSiswa[id]', '$benar', '$salah', '$kosong', '$persentase', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
		}
		else
		{
			mysql_query("UPDATE nilai SET benar = '$benar', salah = '$salah', kosong = '$kosong', persentase = '$persentase', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id_ujian = '$id' AND id_kelas = '$ambilData[id_kelas]' AND id_mapel = '$ambilData[id_mapel]' AND id_rombel = '$ambilData[id_rombel]' AND id_paket_soal = '$ambilData[id_paket_soal]' AND id_siswa = '$ambilDataSiswa[id]'");
		}
	}
	
	$aktivitas = "Proses Nilai";
	$keterangan = mysql_real_escape_string("Memproses Nilai '$field' Pada Tabel 'nilai'");
	$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
	echo "
	<script>
		alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Diproses!');
	</script>
	";
}
?>