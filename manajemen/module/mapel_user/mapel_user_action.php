<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "mapel_user";

if($_POST['mod']=="simpanData")
{
	$id_user = $_POST['id_user'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	
	$data = mysql_query("SELECT nama_lengkap FROM user WHERE id = '$id_user'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_lengkap'];
	
	$simpanData = mysql_query("INSERT INTO $nama_tabel (id_user, id_kelas, id_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_user', '$id_kelas', '$id_mapel', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
	if($simpanData)
	{
		$aktivitas = "Tambah Mapel User";
		$keterangan = mysql_real_escape_string("Menambahkan '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Ditambahkan!');
			$('#form').modal('hide');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
		</script>
		";
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_user = $_POST['id_user'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	
	$data = mysql_query("SELECT nama_lengkap FROM user WHERE id = '$id_user'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_lengkap'];
	
	$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_user = '$id_user', id_kelas = '$id_kelas', id_mapel = '$id_mapel', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
	if($perbaruiData)
	{
		$aktivitas = "Perbarui Mapel User";
		$keterangan = mysql_real_escape_string("Memperbarui '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Diperbarui!');
			$('#form').modal('hide');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_lengkap FROM $nama_tabel LEFT JOIN user ON $nama_tabel.id_user = user.id WHERE $nama_tabel.id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_lengkap'];

	$cekMapelUser = mysql_num_rows(mysql_query("SELECT id_mapel_user FROM jadwal_rombel WHERE id_mapel_user = '$id'"));
	if($cekMapelUser > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Sedang Digunakan Jadwal Rombel!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
			
		if($hapusData)
		{
			$aktivitas = "Hapus Mapel User";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekMapelUser = mysql_num_rows(mysql_query("SELECT id_mapel_user FROM jadwal_rombel WHERE id_mapel_user IN ($data_terpilih)"));
	if($cekMapelUser > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Sedang Digunakan Jadwal Rombel!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
			
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Mapel User Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Mapel User Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel User Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}
?>