<?php
include "../../config/database.php";

$id_user = $_POST['id'];
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 1, "asc" ]]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Tambah Mapel User</h4>
</div>
<div class="modal-body">

	<form id="formMapel">

		<input type="hidden" name="mod" value="simpanMapel">
		<input type="hidden" name="id_user" value="<?=$id_user;?>">

		<table class="table table-bordered table-hover data_custom">

			<thead>
				<tr>
					<th style="width: 1px;">#</th>
					<th>Kelas</th>
					<th>Kode Mapel</th>
					<th>Nama Mapel</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$data = mysql_query("SELECT mapel.*, kelas.nama_kelas, mapel_user.id_user FROM mapel LEFT JOIN kelas ON mapel.id_kelas = kelas.id LEFT JOIN mapel_user ON mapel.id = mapel_user.id_mapel AND mapel_user.id_user = '$id_user' ORDER BY kelas.nama_kelas ASC, mapel.kode_mapel ASC");
				while($getData = mysql_fetch_array($data))
				{
					$selected = ($getData['id_user'] != "" ? "checked" : "");
					echo "
					<tr>
						<td align='center'>
							<input type='checkbox' name='id_mapel[]' value='$getData[id]' $selected>
						</td>
						<td>$getData[nama_kelas]</td>
						<td>$getData[kode_mapel]</td>
						<td>$getData[nama_mapel]</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
		</table>
		
	</form>
		
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanMapel" onclick="simpanMapel(<?=$id_user;?>)"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Simpan</button>
</div>