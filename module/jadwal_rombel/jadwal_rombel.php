<?php
include "manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px; vertical-align: middle !important;}" : "");
?>

<style>
	<?=$mobileSize;?>
</style>

<script language="JavaScript">
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | Jadwal Rombel";
	}
	GantiTitle();
	
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data_custom tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			//"order": [[ 1, "asc" ]],
			// "dom": 'lBfrtip',
        	// "buttons": [
				// 'colvis',
				// {
                	// "extend": 'print',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// },
				// {
                	// "extend": 'excel',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// },
				// {
                	// "extend": 'pdf',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// }
       		// ]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
		
	//Lihat Detail
	function lihatDetail(id)
	{
		var mod = "lihatDetail";
		var id = id;
		$.ajax({
			type	: "POST",
			url		: "module/jadwal_rombel/jadwal_rombel_form_detail.php",
			data	: "mod=" + mod +
					  "&id=" + id,
			success: function(html)
			{
				$("#formContent").html(html);
				$("#form").modal();
			}
		})
	}
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Jadwal Rombel</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
			<li class="active">Jadwal Rombel</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Jadwal Rombel</h3>
					</div>
					<div class="box-body">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#jadwal_rombel_1">Senin</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_2">Selasa</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_3">Rabu</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_4">Kamis</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_5">Jum'at</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_6">Sabtu</a></li>
							<li><a data-toggle="tab" href="#jadwal_rombel_7">Minggu</a></li>
						</ul>
						<div class="tab-content">
							<div id="jadwal_rombel_1" class="tab-pane fade in active">
								<h3>Senin</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_1">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas1 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '1' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas1 = mysql_fetch_array($jadwalKelas1))
												{
													$jenis_kelamin = ($getJadwalKelas1['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas1[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas1[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas1[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_2" class="tab-pane fade">
								<h3>Selasa</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_2">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas2 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '2' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas2 = mysql_fetch_array($jadwalKelas2))
												{
													$jenis_kelamin = ($getJadwalKelas2['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas2[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas2[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas2[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_3" class="tab-pane fade">
								<h3>Rabu</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_3">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas3 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '3' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas3 = mysql_fetch_array($jadwalKelas3))
												{
													$jenis_kelamin = ($getJadwalKelas3['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas3[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas3[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas3[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_4" class="tab-pane fade">
								<h3>Kamis</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_4">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas4 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '4' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas4 = mysql_fetch_array($jadwalKelas4))
												{
													$jenis_kelamin = ($getJadwalKelas4['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas4[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas4[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas4[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_5" class="tab-pane fade">
								<h3>Jum'at</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_5">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas5 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '5' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas5 = mysql_fetch_array($jadwalKelas5))
												{
													$jenis_kelamin = ($getJadwalKelas5['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas5[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas5[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas5[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_6" class="tab-pane fade">
								<h3>Sabtu</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_6">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas6 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '6' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas6 = mysql_fetch_array($jadwalKelas6))
												{
													$jenis_kelamin = ($getJadwalKelas6['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas6[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas6[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas6[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
							<div id="jadwal_rombel_7" class="tab-pane fade">
								<h3>Minggu</h3>
								<div class="scrolling" style="margin-top: 15px; padding-bottom: 45px;">
									<div id="jadwal_rombel_nilai_6">
										<table class="table table-bordered table-hover data_custom">

											<thead>
												<tr>
													<th style="width: 1px;">No.</th>
													<th>Mapel</th>
													<th>Guru</th>
												</tr>
											</thead>

											<tbody>

												<?php
												$x = 1;
												$jadwalKelas7 = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin  FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '0' ORDER BY jadwal_rombel.id");
												while($getJadwalKelas7 = mysql_fetch_array($jadwalKelas7))
												{
													$jenis_kelamin = ($getJadwalKelas7['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
													echo "
													<tr>
														<td align='center'>$x</td>
														<td>$getJadwalKelas7[nama_mapel]</td>
														<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas7[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas7[nama_lengkap]</button></td>
													</tr>";
													$x++;
												}
												?>
												
											</tbody>
											
											<?php
											if($mobile == false)
											{
											?>
												<tfoot>
													<tr>
														<th>#</th>
														<th class="filter">Mapel</th>
														<th class="filter">Guru</th>
													</tr>
												</tfoot>
											<?php
											}
											?>
											
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="form" role="dialog">
							<div class="modal-dialog">
								<div id="formContent" class="modal-content"></div>
								<div id="notifikasi" class="modal-content"></div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</section>	
</div>