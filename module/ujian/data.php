<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_waktu.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "ujian";
$sTableJoin1 = "mapel";
$sTableJoin2 = "nilai";

if($mobile == false)
{
	$aColumns = array('tanggal_ujian', 'jam_mulai', 'jam_selesai', 'jenis_ujian', 'tahun_ajaran', 'semester', 'nama_mapel', 'suspect_aktif', 'id'); 
}
else
{
	$aColumns = array('tanggal_ujian', 'jam_mulai', 'jam_selesai', 'detail_ujian', 'id'); 
}

$sIndexColumn = "$sTable.id";

$siswa = mysql_fetch_array(mysql_query("SELECT id_rombel FROM siswa WHERE username = '$_SESSION[username]'"));

$s = (isset($_SESSION['username']) ? "AND $sTable.id_rombel = '$siswa[id_rombel]' AND $sTable.tanggal_ujian >= '$tanggal_sekarang' AND $sTableJoin2.id IS NULL" : "");
$ss = (isset($_SESSION['username']) ? "WHERE $sTable.id_rombel = '$siswa[id_rombel]' AND $sTable.tanggal_ujian >= '$tanggal_sekarang' AND $sTableJoin2.id IS NULL" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(strpos("tugas harian", $sKeyword) !== false)
	{
		$sValue1 = "1";
	}
	else if(strpos("ulangan harian", $sKeyword) !== false)
	{
		$sValue1 = "2";
	}
	else if(strpos("ujian tengah semester (uts)", $sKeyword) !== false)
	{
		$sValue1 = "3";
	}
	else if(strpos("ujian akhir semester (uas)", $sKeyword) !== false)
	{
		$sValue1 = "4";
	}
	else if(strpos("try out usbn", $sKeyword) !== false)
	{
		$sValue1 = "5";
	}
	else if(strpos("ujian sekolah berstandar nasional (usbn)", $sKeyword) !== false)
	{
		$sValue1 = "6";
	}
	else
	{
		$sValue1 = "@";
	}

	if(strpos("ganjil", $sKeyword) !== false)
	{
		$sValue2 = "1";
	}
	else if(strpos("genap", $sKeyword) !== false)
	{
		$sValue2 = "2";
	}
	else
	{
		$sValue2 = "@";
	}
	
	$sWhere = "WHERE
	$sTable.tanggal_ujian LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jam_mulai LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jam_selesai LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jenis_ujian LIKE '%" . $sValue1 . "%' $s OR
	$sTable.tahun_ajaran LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.semester LIKE '%" . $sValue2 . "%' $s OR
	$sTableJoin1.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.suspect_aktif LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 3)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("tugas harian", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("ulangan harian", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else if(strpos("ujian tengah semester (uts)", $bKeyword) !== false)
			{
				$bValue = "3";
			}
			else if(strpos("ujian akhir semester (uas)", $bKeyword) !== false)
			{
				$bValue = "4";
			}
			else if(strpos("try out usbn", $bKeyword) !== false)
			{
				$bValue = "5";
			}
			else if(strpos("ujian sekolah berstandar nasional (usbn)", $bKeyword) !== false)
			{
				$bValue = "6";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else if($i == 5)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("ganjil", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("genap", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTable.tanggal_ujian, $sTable.jam_mulai, $sTable.jam_selesai, $sTable.jenis_ujian, $sTable.tahun_ajaran, $sTable.semester, $sTableJoin1.nama_mapel, $sTable.suspect_aktif,
	CONCAT(
		'<tr>
			<td class=customHeader>Tahun Ajaran</td>
			<td class=customHeader>:</td>
			<td>', tahun_ajaran, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Semester</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN semester = 1 THEN 'Ganjil' ELSE 'Genap'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Jenis Ujian</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN jenis_ujian = 1 THEN 'Tugas Harian'
					WHEN jenis_ujian = 2 THEN 'Ulangan Harian'
					WHEN jenis_ujian = 3 THEN 'Ujian Tengah Semester (UTS)'
					WHEN jenis_ujian = 4 THEN 'Ujian Akhir Semester (UAS)'
					WHEN jenis_ujian = 5 THEN 'Try Out USBN'
					WHEN jenis_ujian = 6 THEN 'Ujian Sekolah Berstandar Nasional (USBN)'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Mapel</td>
			<td class=customHeader>:</td>
			<td>', nama_mapel, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Suspect</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN suspect_aktif = 'Y' THEN 'Aktif' ELSE 'Tidak Aktif'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Acak Soal</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN acak_soal = 'Y' THEN 'Aktif' ELSE 'Tidak Aktif'
				END,
			'</td>
		</tr>'
	) AS detail_ujian
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_mapel = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id = $sTableJoin2.id_ujian AND $sTableJoin2.id_siswa = '$_SESSION[id]'
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_mapel = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id = $sTableJoin2.id_ujian AND $sTableJoin2.id_siswa = '$_SESSION[id]'";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>