<?php
session_start();
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$nama_menu = "ujian";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$sTable = "absensi";
$sTableJoin1 = "rombel";
$sTableJoin2 = "mapel";
$sTableJoin3 = "user";
$sTableJoin4 = "absensi_detail";

if($getHakAkses['s'] == 0)
{
	$aColumns = array('id', 'nama_rombel', 'nama_mapel', 'tahun_ajaran', 'semester', 'tanggal', 'jam', 'pertemuan_ke', 'uraian_materi', 'jumlah_hadir', 'jumlah_sakit', 'jumlah_izin', 'jumlah_alpa');
}
else
{
	$aColumns = array('id', 'nama_rombel', 'nama_mapel', 'tahun_ajaran', 'semester', 'tanggal', 'jam', 'pertemuan_ke', 'uraian_materi', 'jumlah_hadir', 'jumlah_sakit', 'jumlah_izin', 'jumlah_alpa', 'nama_lengkap');
}

$sIndexColumn = "$sTable.id"; 

$s = ($getHakAkses['s'] == 0 ? "AND $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
$ss = ($getHakAkses['s'] == 0 ? "WHERE $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(strpos("ganjil", $sKeyword) !== false)
	{
		$sValue = "1";
	}
	else if(strpos("genap", $sKeyword) !== false)
	{
		$sValue = "2";
	}
	else
	{
		$sValue = "@";
	}
			
	$sWhere = "WHERE
	$sTableJoin1.nama_rombel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin2.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.tahun_ajaran LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.semester LIKE '%" . $sValue . "%' $s OR
	$sTable.tanggal LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jam LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.pertemuan_ke LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.uraian_materi LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	
	if($getHakAkses['s'] != 0)
	{
		$sWhere .= "OR $sTableJoin3.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	}
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 4)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("ganjil", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("genap", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTableJoin1.nama_rombel, $sTableJoin2.nama_mapel, $sTable.tahun_ajaran, $sTable.semester, $sTable.tanggal, $sTable.jam, $sTable.pertemuan_ke, $sTable.uraian_materi, $sTableJoin3.nama_lengkap, (SELECT COUNT($sTableJoin4.id_siswa) FROM $sTableJoin4 WHERE $sTableJoin4.id_$sTable = $sTable.id AND $sTableJoin4.kehadiran = 1) AS jumlah_hadir, (SELECT COUNT($sTableJoin4.id_siswa) FROM $sTableJoin4 WHERE $sTableJoin4.id_$sTable = $sTable.id AND $sTableJoin4.kehadiran = 2) AS jumlah_sakit, (SELECT COUNT($sTableJoin4.id_siswa) FROM $sTableJoin4 WHERE $sTableJoin4.id_$sTable = $sTable.id AND $sTableJoin4.kehadiran = 3) AS jumlah_izin, (SELECT COUNT($sTableJoin4.id_siswa) FROM $sTableJoin4 WHERE $sTableJoin4.id_$sTable = $sTable.id AND $sTableJoin4.kehadiran = 4) AS jumlah_alpa
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_rombel = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.ditambah_oleh = $sTableJoin3.username
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_rombel = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.ditambah_oleh = $sTableJoin3.username";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>