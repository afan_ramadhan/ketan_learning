<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";
include "../../libraries/fungsi_user_agent.php";

$nama_menu = "pengumuman";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = $getHakAkses['s'];

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM pengumuman WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['penerima'] = 1;
	$getData['id_rombel'] = 0;
}
?>

<script src="assets/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>	

<script>
	tinymce.init({
		selector: '.textEditor',
		
		<?php
		if($mobile == true)
		{
		?>
		
			height: 200,
			menubar: false,
			statusbar: false,
			toolbar: ' removeformat | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist ',
		
		<?php
		}
		else
		{
		?>
			
			height: 300,
			theme: 'modern',
			plugins: [
						'advlist autolink lists link image jbimages charmap print preview hr anchor pagebreak ',
						'searchreplace wordcount visualblocks visualchars code fullscreen',
						'insertdatetime media nonbreaking save table contextmenu directionality',
						'template paste textcolor colorpicker textpattern imagetools'
					],
			toolbar1: 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link media image jbimages | print preview ',
			relative_urls: false,
		
		<?php
		}
		?>
		
	});
	
	$(document).on('focusin', function(e){
		if ($(event.target).closest(".mce-window").length){
			e.stopImmediatePropagation();
		}
	});
	
	$(function(){
		$('#tanggal_mulai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#tanggal_selesai_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		
		if($('#rombel').is(':checked'))
		{
            $('#id_rombel').addClass("disabled").prop('disabled', false);
        }
		else
		{
            $('#id_rombel').addClass("disabled").prop('disabled', true);
        }
	})
	
	function penerima()
	{
		if($('#rombel').is(':checked'))
		{
            $('#id_rombel').addClass("disabled").prop('disabled', false);
        }
		else
		{
            $('#id_rombel').addClass("disabled").prop('disabled', true);
        }
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Pengumuman";}else{echo "Tambah Pengumuman";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama&nbsp;Pengumuman</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_pengumuman" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_pengumuman'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Keterangan</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<textarea id="keterangan" class="textEditor"><?php if($_POST['mod']=="editData"){echo $getData['keterangan'];} ?></textarea>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Mulai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_mulai_">
					<input type="text" class="form-control" id="tanggal_mulai" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_mulai'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Selesai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_selesai_">
					<input type="text" class="form-control" id="tanggal_selesai" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_selesai'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Penerima</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="radio" id="semua" name="penerima" value="1" onclick="penerima()" style="margin-right: 10px;" <?php if($getData['penerima'] == 1){echo "checked";} ?>> Semua
				<br/>
				<input type="radio" id="guru" name="penerima" value="2" onclick="penerima()" style="margin-right: 10px;" <?php if($getData['penerima'] == 2){echo "checked";} ?>> Guru
				<br/>
				<input type="radio"id="rombel" name="penerima" value="3" onclick="penerima()" style="margin-right: 10px;" <?php if($getData['penerima'] == 3){echo "checked";} ?>> Rombel
				<br/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_rombel" disabled required>
					<option value="0" <?php if($getData['id_rombel'] == "0"){echo "selected";} ?>>Semua Rombel</option>
					<?php
					$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
					while($getRombel = mysql_fetch_array($rombel))
					{
						$selected = ($getData['id_rombel'] == $getRombel['id'] ? "selected" : "");
					?>
						<option value="<?=$getRombel['id'];?>" <?=$selected;?>><?=$getRombel['nama_rombel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>