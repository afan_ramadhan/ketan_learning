<?php
if(isset($_GET['mod']))
{
	switch($_GET['mod'])
	{
		case '404':
			include("module/404/404.php");
			break;
		case 'profil':
			include("module/profil/profil.php");
			break;
		case 'jadwal_rombel':
			include("module/jadwal_rombel/jadwal_rombel.php");
			break;
		case 'absensi':
			include("module/absensi/absensi.php");
			break;
		case 'ebook':
			include("module/ebook/ebook.php");
			break;
		case 'video':
			include("module/video/video.php");
			break;
		case 'ujian':
			include("module/ujian/ujian.php");
			break;
		case 'laporan':
			include("module/laporan/laporan.php");
			break;
		case 'progres_belajar':
			include("module/progres_belajar/progres_belajar.php");
			break;
		case 'saran':
			include("module/saran/saran.php");
			break;
		default:
			include ("module/dashboard/dashboard.php");
			break;
	}   
}
else
{
	include ("module/dashboard/dashboard.php");
}
?>	