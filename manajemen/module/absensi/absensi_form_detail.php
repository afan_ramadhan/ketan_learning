<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$nama_menu = "absensi";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = ($getHakAkses['s'] == 0 ? "WHERE ditambah_oleh = '$_SESSION[username]'" : "");

if($_POST['mod']=="lihatDetail")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT absensi.*, rombel.nama_rombel, mapel.nama_mapel, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 1) AS jumlah_hadir, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 2) AS jumlah_sakit, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 3) AS jumlah_izin, (SELECT COUNT(absensi_detail.id_siswa) FROM absensi_detail WHERE absensi_detail.id_absensi = absensi.id AND absensi_detail.kehadiran = 4) AS jumlah_alpa FROM absensi LEFT JOIN rombel ON absensi.id_rombel = rombel.id LEFT JOIN mapel ON absensi.id_mapel = mapel.id WHERE absensi.id = '$id'");
	$getData = mysql_fetch_array($data);
	
	$semester = ($getData['semester'] == 1 ? "Ganjil" : "Genap");
	$id_rombel = $getData['id_rombel'];
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Absensi</h4>
</div>
<div class="modal-body">
	<div id="print-area" style="width: 100%;">
		<div style="border: 1px solid #F4F4F4; padding: 20px;">
			<table class="table table-hover">
				<tr>
					<td style="border: none; width: 150px;">
						<label class="control-label">Rombel</label>
					</td>
					<td style="border: none; width: 1px;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['nama_rombel'];?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Mapel</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['nama_mapel'];?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Tahun Ajaran</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['tahun_ajaran'];?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Semester</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$semester;?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Tanggal</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=tanggal_indonesia($getData['tanggal']);?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Jam</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=substr($getData['jam'], 0, 5);?> WIB</td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Pertemuan Ke</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['pertemuan_ke'];?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Uraian Materi</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['uraian_materi'];?></td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Total Hadir</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['jumlah_hadir'];?> Siswa</td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Total Sakit</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['jumlah_sakit'];?> Siswa</td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Total Izin</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['jumlah_izin'];?> Siswa</td>
				</tr>
				<tr>
					<td style="border: none;">
						<label class="control-label">Total Alpa</label>
					</td>
					<td style="border: none;"><label class="control-label">:</label></td>
					<td style="border: none;"><?=$getData['jumlah_alpa'];?> Siswa</td>
				</tr>
			</table>
			<hr/>
			<h3 style="margin-top: 0;">Daftar Siswa</h3>
			<div class="scrolling">
				<table class="table table-bordered table-hover data_custom_2">
					<thead>
						<tr>
							<th style="width: 1px;">No.</th>
							<th>NIS</th>
							<th>NISN</th>
							<th>Nama Lengkap</th>
							<th>Kehadiran</th>
							<th>Keterangan</th>
							<th>Sikap</th>
							<th>Catatan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$kehadiran = array("", "Hadir", "Sakit", "Izin", "Alpa");
						$sikap = array("", "Sangat Baik", "Baik", "Cukup", "Kurang");
						
						$x = 1;
						$siswa = mysql_query("SELECT siswa.*, absensi_detail.kehadiran, absensi_detail.keterangan, absensi_detail.sikap, absensi_detail.catatan FROM siswa LEFT JOIN absensi_detail ON siswa.id = absensi_detail.id_siswa AND absensi_detail.id_absensi = '$id' WHERE siswa.id_rombel = '$id_rombel'");
						while($getSiswa = mysql_fetch_array($siswa))
						{
							echo "
							<tr>
								<td align='center'>$x</td>
								<td>$getSiswa[nis]</td>
								<td>$getSiswa[nisn]</td>
								<td>$getSiswa[nama_lengkap]</td>
								<td>".$kehadiran[$getSiswa['kehadiran']]."</td>
								<td>$getSiswa[keterangan]</td>
								<td>".$sikap[$getSiswa['sikap']]."</td>
								<td>$getSiswa[catatan]</td>
							</tr>";
							$x++;
						}
						?>				
					</tbody>		
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<div class="dt-buttons btn-group">
		<a class="btn btn-default buttons-print" href="#" onclick="cetak()"><span>Print</span></a>
		<a class="btn btn-default buttons-excel buttons-html5" href="module/absensi/absensi_export.php?id=<?=$id;?>&format=excel" target="_blank"><span>Excel</span></a>
		<a class="btn btn-default buttons-pdf buttons-html5" href="module/absensi/absensi_export.php?id=<?=$id;?>&format=pdf" target="_blank"><span>PDF</span></a>
	</div>
</div>