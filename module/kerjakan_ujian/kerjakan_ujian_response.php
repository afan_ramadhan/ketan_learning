<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_waktu.php";

$data_siswa = mysql_query("SELECT * FROM siswa WHERE id = '$_POST[id_siswa]'");
$ambil_data_siswa = mysql_fetch_array($data_siswa);

if($_POST['mod']=="pilihJawaban")
{
	$cekKantong = mysql_num_rows(mysql_query("SELECT * FROM kantong_jawaban WHERE id_ujian = '$_POST[id_ujian]' AND id_soal = '$_POST[id_soal]' AND id_siswa = '$_POST[id_siswa]'"));
	if($cekKantong == 0)
	{
		mysql_query("INSERT INTO kantong_jawaban(id_ujian, id_soal, id_siswa, jawaban_siswa, tanggal_ditambah, jam_ditambah, ditambah_oleh) values('$_POST[id_ujian]', '$_POST[id_soal]', '$_POST[id_siswa]', '$_POST[jawaban_siswa]', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
	}
	else
	{
		mysql_query("UPDATE kantong_jawaban SET jawaban_siswa = '$_POST[jawaban_siswa]', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id_ujian = '$_POST[id_ujian]' AND id_soal = '$_POST[id_soal]' AND id_siswa = '$_POST[id_siswa]'");
	}
}
else if($_POST['mod'] == "simpanNilai")
{
	$jumlahSoal = mysql_num_rows(mysql_query("SELECT * FROM soal WHERE id_paket_soal = '$_POST[id_paket_soal]'"));
	
	$queryNilaiTemp = "
	SELECT SUM(benar) AS jumlah_benar, SUM(salah) AS jumlah_salah
	FROM
		(SELECT
			CASE WHEN kantong_jawaban.jawaban_siswa = soal.jawaban THEN 1 ELSE 0 END AS benar,
			CASE WHEN kantong_jawaban.jawaban_siswa <> soal.jawaban THEN 1 ELSE 0 END AS salah
		FROM soal
		LEFT JOIN kantong_jawaban
			ON soal.id = kantong_jawaban.id_soal
			AND kantong_jawaban.id_ujian = '$_POST[id_ujian]'
			AND kantong_jawaban.id_siswa = '$_POST[id_siswa]')
	AS nilai_temp";
	
	$kantongJawaban = mysql_fetch_array(mysql_query($queryNilaiTemp));
	
	$benar = $kantongJawaban['jumlah_benar'];
	$salah = $kantongJawaban['jumlah_salah'];
	$kosong = $jumlahSoal - $benar - $salah;
	$persentase = ($benar / $jumlahSoal ) * 100;
	
	$cekNilai = mysql_num_rows(mysql_query("SELECT * FROM nilai WHERE id_ujian = '$_POST[id_ujian]' AND id_paket_soal = '$_POST[id_paket_soal]' AND id_siswa = '$_POST[id_siswa]'"));
	if($cekNilai == 0)
	{
		mysql_query("INSERT INTO nilai(id_ujian, id_kelas, id_mapel, id_rombel, id_paket_soal, id_siswa, benar, salah, kosong, persentase, tanggal_ditambah, jam_ditambah, ditambah_oleh) values('$_POST[id_ujian]', '$_POST[id_kelas]', '$_POST[id_mapel]', '$_POST[id_rombel]', '$_POST[id_paket_soal]', '$_POST[id_siswa]', '$benar', '$salah', '$kosong', '$persentase', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
		
		$aktivitas = "Tambah Nilai";
		$keterangan = mysql_real_escape_string("Menambahkan '$ambil_data_siswa[nama_lengkap]' Pada Tabel 'nilai'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	}
	else
	{
		mysql_query("UPDATE nilai SET benar = '$benar', salah = '$salah', kosong = '$kosong', persentase = '$persentase', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id_ujian = '$_POST[id_ujian]' AND id_kelas = '$_POST[id_kelas]' AND id_mapel = '$_POST[id_mapel]' AND id_rombel = '$_POST[id_rombel]' AND id_paket_soal = '$_POST[id_paket_soal]' AND id_siswa = '$_POST[id_siswa]'");
		
		$aktivitas = "Perbarui Nilai";
		$keterangan = mysql_real_escape_string("Memperbarui '$ambil_data_siswa[nama_lengkap]' Pada Tabel 'nilai'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	}
}
else if($_POST['mod']=="simpanSuspect")
{
	mysql_query("INSERT INTO suspect(id_ujian, id_siswa, tanggal_ditambah, jam_ditambah, ditambah_oleh) values('$_POST[id_ujian]', '$_POST[id_siswa]', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
	
	$aktivitas = "Tambah Suspect";
	$keterangan = mysql_real_escape_string("Menambahkan '$ambil_data_siswa[nama_lengkap]' Pada Tabel 'suspect'");
	$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
}
?>