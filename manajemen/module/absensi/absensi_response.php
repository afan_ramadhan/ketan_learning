<?php
session_start();
include "../../config/database.php";

if($_POST['mod']=="ambilMapel")
{
	$nama_menu = "absensi";
	$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
	$getHakAkses = mysql_fetch_array($hakAkses);

	$s = $getHakAkses['s'];
	
	$id_absensi = $_POST['id_absensi'];
	$id_rombel = $_POST['id_rombel'];
	
	$data = mysql_query("SELECT * FROM absensi WHERE id = '$id_absensi'");
	$getData = mysql_fetch_array($data);
	
	$rombel = mysql_query("SELECT * FROM rombel WHERE id = '$id_rombel'");
	$getRombel = mysql_fetch_array($rombel);

	if($s == 1)
	{
		$mapel = mysql_query("SELECT * FROM mapel WHERE id_kelas = '$getRombel[id_kelas]' ORDER BY nama_mapel");
	}
	else
	{
		$mapel = mysql_query("SELECT mapel.* FROM mapel JOIN mapel_user ON mapel.id = mapel_user.id_mapel AND mapel_user.id_user = '$_SESSION[id]' WHERE mapel_user.id_kelas = '$getRombel[id_kelas]' ORDER BY nama_mapel");
	}
					
	while($getMapel = mysql_fetch_array($mapel))
	{
		$selected = ($getData['id_mapel'] == $getMapel['id'] ? "selected" : "");
		echo "
		<option value='$getMapel[id]' $selected>$getMapel[nama_mapel]</option>
		";
	}
}
else if($_POST['mod']=="ambilSiswa")
{
	$id_absensi = $_POST['id_absensi'];
	$id_rombel = $_POST['id_rombel'];
	
	$x = 1;
	$siswa = mysql_query("SELECT siswa.*, absensi_detail.kehadiran, absensi_detail.keterangan, absensi_detail.sikap, absensi_detail.catatan FROM siswa LEFT JOIN absensi_detail ON siswa.id = absensi_detail.id_siswa AND absensi_detail.id_absensi = '$id_absensi' WHERE siswa.id_rombel = '$id_rombel'");
	while($getSiswa = mysql_fetch_array($siswa))
	{
		echo "
		<tr>
			<td align='center'>$x</td>
			<td>$getSiswa[nis]</td>
			<td>$getSiswa[nisn]</td>
			<td>$getSiswa[nama_lengkap]</td>
			<td>
				<input type='hidden' name='id_siswa[]' value='$getSiswa[id]'>
				<select class='form-control' id='kehadiran' name='kehadiran[]' required>
					<option value='1' ".($getSiswa['kehadiran'] == 1 ? "selected" : "").">Hadir</option>
					<option value='2' ".($getSiswa['kehadiran'] == 2 ? "selected" : "").">Sakit</option>
					<option value='3' ".($getSiswa['kehadiran'] == 3 ? "selected" : "").">Izin</option>
					<option value='4' ".($getSiswa['kehadiran'] == 4 ? "selected" : "").">Alpa</option>
				</select>
			</td>
			<td>
				<input type='text' class='form-control' id='keterangan' name='keterangan[]' maxlength='200' value='$getSiswa[keterangan]' required/>
			</td>
			<td>
				<select class='form-control' id='sikap' name='sikap[]' required>
					<option value='1' ".($getSiswa['sikap'] == 1 ? "selected" : "").">Sangat Baik</option>
					<option value='2' ".($getSiswa['sikap'] == 2 ? "selected" : "").">Baik</option>
					<option value='3' ".($getSiswa['sikap'] == 3 ? "selected" : "").">Cukup</option>
					<option value='4' ".($getSiswa['sikap'] == 4 ? "selected" : "").">Kurang</option>
				</select>
			</td>
			<td>
				<input type='text' class='form-control' id='catatan' name='catatan[]' maxlength='200' value='$getSiswa[catatan]' required/>
			</td>
		</tr>";
		$x++;
	}
}
?>