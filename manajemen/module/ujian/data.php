<?php
session_start();
include "../../config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$nama_menu = "ujian";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$sTable = "ujian";
$sTableJoin1 = "kelas";
$sTableJoin2 = "mapel";
$sTableJoin3 = "rombel";
$sTableJoin4 = "paket_soal";
$sTableJoin5 = "user";

if($getHakAkses['s'] == 0)
{
	$aColumns = array('id', 'tanggal_ujian', 'jam_mulai', 'jam_selesai', 'jenis_ujian', 'tahun_ajaran', 'semester', 'nama_kelas', 'nama_mapel', 'nama_rombel', 'nama_paket_soal', 'suspect_aktif', 'acak_soal');
}
else
{
	$aColumns = array('id', 'tanggal_ujian', 'jam_mulai', 'jam_selesai', 'jenis_ujian', 'tahun_ajaran', 'semester', 'nama_kelas', 'nama_mapel', 'nama_rombel', 'nama_paket_soal', 'suspect_aktif', 'acak_soal', 'nama_lengkap');
}

$sIndexColumn = "$sTable.id"; 

$s = ($getHakAkses['s'] == 0 ? "AND $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
$ss = ($getHakAkses['s'] == 0 ? "WHERE $sTable.ditambah_oleh = '$_SESSION[username]'" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(strpos("tugas harian", $sKeyword) !== false)
	{
		$sValue1 = "1";
	}
	else if(strpos("ulangan harian", $sKeyword) !== false)
	{
		$sValue1 = "2";
	}
	else if(strpos("ujian tengah semester (uts)", $sKeyword) !== false)
	{
		$sValue1 = "3";
	}
	else if(strpos("ujian akhir semester (uas)", $sKeyword) !== false)
	{
		$sValue1 = "4";
	}
	else if(strpos("try out usbn", $sKeyword) !== false)
	{
		$sValue1 = "5";
	}
	else if(strpos("ujian sekolah berstandar nasional (usbn)", $sKeyword) !== false)
	{
		$sValue1 = "6";
	}
	else
	{
		$sValue1 = "@";
	}

	if(strpos("ganjil", $sKeyword) !== false)
	{
		$sValue2 = "1";
	}
	else if(strpos("genap", $sKeyword) !== false)
	{
		$sValue2 = "2";
	}
	else
	{
		$sValue2 = "@";
	}
	
	$sWhere = "WHERE
	$sTable.tanggal_ujian LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jam_mulai LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jam_selesai LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.jenis_ujian LIKE '%" . $sValue1 . "%' $s OR
	$sTable.tahun_ajaran LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.semester LIKE '%" . $sValue2 . "%' $s OR
	$sTableJoin1.nama_kelas LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin2.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin3.nama_rombel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin4.nama_paket_soal LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.suspect_aktif LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.acak_soal LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	
	if($getHakAkses['s'] != 0)
	{
		$sWhere .= "OR $sTableJoin5.nama_lengkap LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
	}
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 4)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("tugas harian", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("ulangan harian", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else if(strpos("ujian tengah semester (uts)", $bKeyword) !== false)
			{
				$bValue = "3";
			}
			else if(strpos("ujian akhir semester (uas)", $bKeyword) !== false)
			{
				$bValue = "4";
			}
			else if(strpos("try out usbn", $bKeyword) !== false)
			{
				$bValue = "5";
			}
			else if(strpos("ujian sekolah berstandar nasional (usbn)", $bKeyword) !== false)
			{
				$bValue = "6";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else if($i == 6)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("ganjil", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("genap", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTable.tanggal_ujian, $sTable.jam_mulai, $sTable.jam_selesai, $sTable.jenis_ujian, $sTable.tahun_ajaran, $sTable.semester, $sTableJoin1.nama_kelas, $sTableJoin2.nama_mapel, $sTableJoin3.nama_rombel, $sTableJoin4.nama_paket_soal, $sTable.suspect_aktif, $sTable.acak_soal, $sTableJoin5.nama_lengkap
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_kelas = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.id_rombel = $sTableJoin3.id
	LEFT JOIN $sTableJoin4 ON $sTable.id_paket_soal = $sTableJoin4.id
	LEFT JOIN $sTableJoin5 ON $sTable.ditambah_oleh = $sTableJoin5.username
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_kelas = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTable.id_mapel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTable.id_rombel = $sTableJoin3.id
	LEFT JOIN $sTableJoin4 ON $sTable.id_paket_soal = $sTableJoin4.id
	LEFT JOIN $sTableJoin5 ON $sTable.ditambah_oleh = $sTableJoin5.username";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>