<?php
session_start();
include "manajemen/libraries/fungsi_waktu.php";
include "manajemen/libraries/fungsi_user_agent.php";
include "manajemen/config/database.php";

if(empty($_SESSION['id']) and empty($_SESSION['username']) and empty($_SESSION['level']))
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else if($_SESSION['level'] != "siswa")
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else if(!isset($_POST['id_ujian']))
{
	echo "
	<script>
		window.location.href='index.php';
	</script>";
}
else
{
	$blokir = mysql_query("SELECT blokir FROM siswa WHERE username = '$_SESSION[username]'");
	$getBlokir = mysql_fetch_array($blokir);
	if($getBlokir['blokir'] == "Y")
	{
		session_destroy();
		
		echo "
		<script>
			window.location.href='login.php';
		</script>";
	}
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$data_siswa = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$_SESSION[id]'");
$ambil_data_siswa = mysql_fetch_array($data_siswa);

$rombelUser = mysql_query("SELECT id_rombel FROM siswa WHERE id = '$_SESSION[id]'");
$ambilRombellUser = mysql_fetch_array($rombelUser);

$data_ujian = mysql_fetch_array(mysql_query("SELECT ujian.*, mapel.nama_mapel FROM ujian LEFT JOIN mapel ON ujian.id_mapel = mapel.id WHERE ujian.id = '$_POST[id_ujian]'"))
?>

<!DOCTYPE html>

<html>
	
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | Ujian</title>
			
		<link rel="icon" type="image/png" href="manajemen/images/ketanware_2.png">
		<link href="manajemen/assets/plugins/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/bootstrap/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/calendar/css/pickmeup.css" rel="stylesheet" type="text/css"/>
		<link href="manajemen/assets/plugins/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/icofont/css/icofont.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/datatables/datatables.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/alertifyjs/css/alertify.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/alertifyjs/css/themes/default.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/ionicons/css/ionicons.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/adminlte/css/AdminLTE.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/adminlte/css/skins/_all-skins.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/ramadhan_afan/css/ramadhan_afan.css" type="text/css" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" type="text/css" rel="stylesheet">
		
		<style>
			[type=radio]
			{ 
				position: absolute;
				opacity: 0;
				width: 0;
				height: 0;
			}

			[type=radio] + img {
				cursor: pointer;
			}
			
			.box-body p img {
				max-width: 100%;
			}
		</style>

	</head>

	<body class="hold-transition <?=$ambil_data_siswa['tema'];?> sidebar-mini">
		
		<script src="manajemen/assets/plugins/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/moment.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/adminlte/js/adminlte.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/calendar/js/jquery.pickmeup.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/calendar/js/demo.js" type="text/javascript"></script>	
		<script src="manajemen/assets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/alertifyjs/alertify.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/preloaders/jquery.preloaders.js" type="text/javascript"></script>
		<script src="manajemen/assets/ramadhan_afan/js/ramadhan_afan.js" type="text/javascript"></script>		
			
		<script>
			//Animasi Loading
			$(function(){
				$('.loading').click(function(){
					$.preloader.start();
					setTimeout(function(){$.preloader.stop();}, 500);
				});
			});
			
			function mulaiAnimasi()
			{
				$.preloader.start();
			}
			
			function stopAnimasi()
			{
				$.preloader.stop();
			}
			
			$(document).ready(function(){
				<?php
				if($mobile == false)
				{
					echo "
					$('.sidebar-toggle').click(function(){
						$('#navigasi-soal').fadeToggle('fast');
					});
					";
				}
				?>
			});
			
			<?php
			$durasi_ujian = "+5 minutes";
			$jam_selesai = $data_ujian['jam_selesai'];
		
			$waktu_ujian = date('Y-m-d H:i:s', strtotime($durasi_ujian, strtotime($jam_selesai)));
			
			$date = DateTime::createFromFormat("Y-m-d H:i:s", $waktu_ujian);
			
			$waktu_fix = $date->format("F") . " " . $date->format("d") . ", " . $date->format("Y") . " " . $date->format("H:i:s");
			?>
			
			var waktu_fix = "<?=$waktu_fix;?>";
			
			var countDownDate = new Date(waktu_fix).getTime();
			var x = setInterval(function(){
				var now = new Date().getTime();
				var distance = countDownDate - now;
				var days = Math.floor(distance / (1000 * 60 * 60 * 24));
				var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				var seconds = Math.floor((distance % (1000 * 60)) / 1000);

				$(".waktu_ujian").text("Sisa Waktu " + hours + ":" + minutes + ":" + seconds);

				if(distance < 0)
				{
					clearInterval(x);
					$(".waktu_ujian").text("Waktu Habis!");
					
					simpanNilai();
				}
			}, 1000);
			
			var ukuran_font = 20;
			var ukuran_pilihan_ganda = 25;
			var jarak_pilihan_ganda = 15;
			
			function perbesarSoal()
			{
				ukuran_font += 2;
				ukuran_pilihan_ganda += 2;
				jarak_pilihan_ganda += 2;
				
				$(".soal").css('font-size', ukuran_font);
				$(".pilihan-ganda-detail").css('width', ukuran_pilihan_ganda);
				$(".pilihan-ganda-detail").css('margin-right', jarak_pilihan_ganda);
			}
			
			function perkecilSoal()
			{
				ukuran_font -= 2;
				ukuran_pilihan_ganda -= 2;
				jarak_pilihan_ganda -= 2;
				
				$(".soal").css('font-size', ukuran_font);
				$(".pilihan-ganda-detail").css('width', ukuran_pilihan_ganda);
				$(".pilihan-ganda-detail").css('margin-right', jarak_pilihan_ganda);
			}
			
			function resetUkuranSoal()
			{
				ukuran_font = 20;
				ukuran_pilihan_ganda = 25;
				jarak_pilihan_ganda = 15;
			
				$(".soal").css('font-size', ukuran_font);
				$(".pilihan-ganda-detail").css('width', ukuran_pilihan_ganda);
				$(".pilihan-ganda-detail").css('margin-right', jarak_pilihan_ganda);
			}
			
			function pindahSoal(nomor_soal)
			{
				$(".navigasi_soal").removeClass("navigasi-aktif");
				$("#navigasi_no_" + nomor_soal).addClass("navigasi-aktif");
					
				$(".soal").hide();
				$("#soal_no_" + nomor_soal).fadeIn();
			}
			
			function pindahSebelumnya(nomor_soal)
			{
				nomor_soal -= 1;
				$(".navigasi_soal").removeClass("navigasi-aktif");
				$("#navigasi_no_" + nomor_soal).addClass("navigasi-aktif");
					
				$(".soal").hide();
				$("#soal_no_" + nomor_soal).fadeIn();
			}
			
			function pindahBerikutnya(nomor_soal)
			{
				nomor_soal += 1;
					
				$(".navigasi_soal").removeClass("navigasi-aktif");
				$("#navigasi_no_" + nomor_soal).addClass("navigasi-aktif");
					
				$(".soal").hide();
				$("#soal_no_" + nomor_soal).fadeIn();
			}
			
			function pilihJawaban(nomor_soal, id_ujian, id_soal, id_siswa, jawaban_siswa)
			{
				var mod = "pilihJawaban";
				$.ajax({
					type	: "POST",
					url		: "module/kerjakan_ujian/kerjakan_ujian_response.php",
					data	: "mod=" + mod +
							  "&id_ujian=" + id_ujian +
							  "&id_soal=" + id_soal +
							  "&id_siswa=" + id_siswa +
							  "&jawaban_siswa=" + jawaban_siswa,
					success: function()
					{
						$("#navigasi_no_" + nomor_soal).removeClass("btn-default");
						$("#navigasi_no_" + nomor_soal).addClass("btn-success");
						
						$("#jawaban_no_" + nomor_soal + "_1").attr('src', 'manajemen/images/A.png');
						$("#jawaban_no_" + nomor_soal + "_2").attr('src', 'manajemen/images/B.png');
						$("#jawaban_no_" + nomor_soal + "_3").attr('src', 'manajemen/images/C.png');
						$("#jawaban_no_" + nomor_soal + "_4").attr('src', 'manajemen/images/D.png');
						$("#jawaban_no_" + nomor_soal + "_5").attr('src', 'manajemen/images/E.png');
						
						$("#jawaban_no_" + nomor_soal + "_" + jawaban_siswa).attr('src', 'manajemen/images/Checklist.png');
					},
					error: function()
					{
						alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Jawaban Tidak Tersimpan, Periksa Koneksi Internet Anda!');
					}
				})
			}
			
			function simpanNilai()
			{
				mulaiAnimasi();
				var mod = "simpanNilai";
				var id_ujian = "<?=$data_ujian['id'];?>";
				var id_kelas = "<?=$data_ujian['id_kelas'];?>";
				var id_mapel = "<?=$data_ujian['id_mapel'];?>";
				var id_rombel = "<?=$data_ujian['id_rombel'];?>";
				var id_paket_soal = "<?=$data_ujian['id_paket_soal'];?>";
				var id_siswa = "<?=$_SESSION['id'];?>";
				$.ajax({
					type	: "POST",
					url		: "module/kerjakan_ujian/kerjakan_ujian_response.php",
					data	: "mod=" + mod +
							  "&id_ujian=" + id_ujian +
							  "&id_kelas=" + id_kelas +
							  "&id_mapel=" + id_mapel +
							  "&id_rombel=" + id_rombel +
							  "&id_paket_soal=" + id_paket_soal +
							  "&id_siswa=" + id_siswa,
					success: function()
					{
						stopAnimasi();
						alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Disimpan!').set({onshow: null, onclose: function(){window.location.href='index.php?mod=ujian'}});
					},
					error: function()
					{
						stopAnimasi();
						alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nilai Tidak Tersimpan, Periksa Koneksi Internet Anda!');
					}
				})
			}
			
			var lama_toleransi = 3;

			window.addEventListener("blur", notFocus);
			window.addEventListener("focus", onFocus);

			function onFocus()
			{
				if(getCookie('iSuspect') != undefined && getCookie('iSuspect') != null)
				{
					var start_out_focus = new Date(getCookie('iSuspect'))
					var end_out_focus = new Date();
					var gapTime = (end_out_focus.getTime() - start_out_focus.getTime()) / 1000;

					if(gapTime > lama_toleransi)
					{
						<?php
						if($data_ujian['suspect_aktif'] == "Y")
						{
						?>
						
							mulaiAnimasi();
							var mod = "simpanSuspect";
							var id_ujian = "<?=$data_ujian['id'];?>";
							var id_siswa = "<?=$_SESSION['id'];?>";
							$.ajax({
								type	: "POST",
								url		: "module/kerjakan_ujian/kerjakan_ujian_response.php",
								data	: "mod=" + mod +
										  "&id_ujian=" + id_ujian +
										  "&id_siswa=" + id_siswa,
								success: function()
								{
									stopAnimasi();
									window.location.href="index.php?mod=ujian&suspect=true";
								},
								error: function()
								{
									stopAnimasi();
									window.location.href="index.php?mod=ujian&suspect=true";
								}
							})
							
						<?php
						}
						?>
					}
					
					eraseCookie('iSuspect');
				}
			}

			function notFocus(){
				if(getCookie('iSuspect') == undefined || getCookie('iSuspect') == null)
				{
					var startDate = new Date();
					setCookie('iSuspect', startDate, 1);
				}
			}
		
			function setCookie(name, value, days)
			{
				var expires = "";
				if(days)
				{
					var date = new Date();
					date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
					expires = "; expires=" + date.toUTCString();
				}
			
				document.cookie = name + "=" + (value || "")  + expires + "; path=/";
			}
		
			function getCookie(name)
			{
				var nameEQ = name + "=";
				var ca = document.cookie.split(';');
				for(var i = 0; i < ca.length; i++)
				{
					var c = ca[i];
					while(c.charAt(0) == ' ') c = c.substring(1, c.length);
					if(c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
				}
				
				return null;
			}
	  
			function eraseCookie(name)
			{   
				document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			}
		</script>
		
		<?php
		$cek_soal = mysql_num_rows(mysql_query("SELECT * FROM soal WHERE id_paket_soal = '$data_ujian[id_paket_soal]'"));
		$cek_suspect = mysql_num_rows(mysql_query("SELECT * FROM suspect WHERE id_ujian = '$data_ujian[id]' AND id_siswa = '$_SESSION[id]'"));
		if($cek_soal == 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Belum Diinput!').set({onshow: null, onclose: function(){window.location.href='index.php?mod=ujian'}});
			</script>";
		}
		else if($cek_suspect > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Status Kamu Pada Ujian Ini Sedang Suspect, Silahkan Hubungi Guru Yang Bersangkutan!').set({onshow: null, onclose: function(){window.location.href='index.php?mod=ujian'}});
			</script>";
		}
		?>
		
		<div class="wrapper">
		
			<?php
			include "header_ujian.php";
			include "menu_ujian.php";
			include "content_ujian.php";
			include "footer.php";
			include "sidebar.php";
			?>
			
		</div>

	</body>
	
</html>