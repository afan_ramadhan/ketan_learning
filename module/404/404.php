<script>
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | 404 Page Not Found";
	}
	GantiTitle();
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>404 Page Not Found</h1>
		<ol class="breadcrumb">
			<li><a href="index.php"><i class="fa fa-exclamation-triangle" style="margin-right: 10px;"></i>Dashboard</a></li>
			<li class="active">404 Page Not Found</li>
		</ol>
	</section>
	<section class="content">
		<div class="error-page">
			<h2 class="headline text-yellow">404</h2>

			<div class="error-content" style="padding-top: 15px;">
				<h3 align="center"><i class="fa fa-warning text-yellow" style="margin-right: 10px;"></i>Oops! Page not found.</h3>
				<p align="center">
					We could not find the page you were looking for. Meanwhile, you may <a href="../../index.php">return to dashboard</a>.
				</p>
			</div>
		</div>
	</section>
</div>