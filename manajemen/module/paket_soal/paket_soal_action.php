<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "paket_soal";

if($_POST['mod']=="simpanData")
{
	$nama_paket_soal = mysql_real_escape_string($_POST['nama_paket_soal']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_paket_soal']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Paket Soal Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (nama_paket_soal, keterangan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_paket_soal', '$keterangan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Paket Soal";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_paket_soal' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_paket_soal = mysql_real_escape_string($_POST['nama_paket_soal']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_paket_soal']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Paket Soal Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_paket_soal = '$nama_paket_soal', keterangan = '$keterangan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Paket Soal";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_paket_soal' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_paket_soal FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_paket_soal'];
	
	$cekPaketSoal_1 = mysql_num_rows(mysql_query("SELECT id_paket_soal FROM soal WHERE id_paket_soal = '$id'"));
	$cekPaketSoal_2 = mysql_num_rows(mysql_query("SELECT id_paket_soal FROM ujian WHERE id_paket_soal = '$id'"));
	if($cekPaketSoal_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Sedang Digunakan Soal!');
		</script>";
	}
	else if($cekPaketSoal_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Sedang Digunakan Ujian!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Paket Soal";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekPaketSoal_1 = mysql_num_rows(mysql_query("SELECT id_paket_soal FROM soal WHERE id_paket_soal IN ($data_terpilih)"));
	$cekPaketSoal_2 = mysql_num_rows(mysql_query("SELECT id_paket_soal FROM ujian WHERE id_paket_soal IN ($data_terpilih)"));
	if($cekPaketSoal_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Sedang Digunakan Soal!');
		</script>";
	}
	else if($cekPaketSoal_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Sedang Digunakan Ujian!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Paket Soal Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Paket Soal Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Paket Soal Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}
?>