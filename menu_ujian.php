<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<?php
				if($ambil_data_siswa['gambar']=="")
				{
					echo "<img class='img-circle' src='manajemen/images/user_kosong.jpg'/>";
				}
				else
				{
					echo "<img class='img-circle' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
				}
				?>
			</div>
			<div class="pull-left info">
				<p><?=$ambil_data_siswa['nama_lengkap'];?></p>
				<small><?=$ambil_data_siswa['nama_rombel'];?></small>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header" style="text-align: center;"><?=strtoupper($lihat_konfigurasi['nama_aplikasi']);?> NAVIGASI SOAL</li>
		</ul>
		<div id="navigasi-soal">
			<?php
			$orderBy = ($data_ujian['acak_soal'] == "Y" ? "ORDER BY RAND()" : "ORDER BY soal.id");
			$nomor_navigasi = 1;
			$data_soal = mysql_query("SELECT soal.*, kantong_jawaban.jawaban_siswa FROM soal LEFT JOIN kantong_jawaban ON soal.id = kantong_jawaban.id_soal AND kantong_jawaban.id_ujian = '$data_ujian[id]' AND kantong_jawaban.id_siswa = '$_SESSION[id]' WHERE soal.id_paket_soal = '$data_ujian[id_paket_soal]' $orderBy");
			$jumlah_navigasi = mysql_num_rows($data_soal);
			while($ambil_navigasi = mysql_fetch_array($data_soal))
			{
				$navigasi_pertama = ($nomor_navigasi == 1 ? "navigasi-aktif" : "");
				$navigasi_terpilih = ($ambil_navigasi['jawaban_siswa'] == "" ? "btn-default" : "btn-success");
				echo "<a id='navigasi_no_".$nomor_navigasi."' class='btn $navigasi_terpilih loading navigasi_soal $navigasi_pertama' onclick='pindahSoal($nomor_navigasi)'>$nomor_navigasi</a>";
				$nomor_navigasi++;
			}
			?>
		</div>
	</section>
</aside>
