<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM jadwal_rombel WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['id_rombel'] = 0;
}

$disabled = (!empty($_GET['id']) ? "disabled" : "");
$id_rombel = (!empty($_GET['id']) ? $_GET['id'] : $getData['id_rombel']);
?>

<script>
	$(document).ready(function(){
		var mod = "ambilMapelUser";
		var id_jadwal_rombel = <?=$id;?>;
		var id_rombel = $( "#id_rombel option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/jadwal_rombel/jadwal_rombel_response.php",
			data	: "mod=" + mod +
					  "&id_jadwal_rombel=" + id_jadwal_rombel +
					  "&id_rombel=" + id_rombel,
			success: function(html)
			{
				$("#id_mapel_user").html(html);
			}
		})
	})
	
	function ambilMapelUser()
	{
		var mod = "ambilMapelUser";
		var id_jadwal_rombel = <?=$id;?>;
		var id_rombel = $( "#id_rombel option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/jadwal_rombel/jadwal_rombel_response.php",
			data	: "mod=" + mod +
					  "&id_jadwal_rombel=" + id_jadwal_rombel +
					  "&id_rombel=" + id_rombel,
			success: function(html)
			{
				$("#id_mapel_user").html(html);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Jadwal Rombel";}else{echo "Tambah Jadwal Rombel";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_rombel" onchange="ambilMapelUser()" required <?=$disabled;?>>
					<?php
					$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
					while($getRombel = mysql_fetch_array($rombel))
					{
						$selected = ($id_rombel == $getRombel['id'] ? "selected" : "");
					?>
						<option value="<?=$getRombel['id'];?>" <?=$selected;?>><?=$getRombel['nama_rombel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Hari</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="hari" required>
					<option value="1" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 1 ? "selected" : "");} ?>>Senin</option>
					<option value="2" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 2 ? "selected" : "");} ?>>Selasa</option>
					<option value="3" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 3 ? "selected" : "");} ?>>Rabu</option>
					<option value="4" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 4 ? "selected" : "");} ?>>Kamis</option>
					<option value="5" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 5 ? "selected" : "");} ?>>Jum'at</option>
					<option value="6" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 6 ? "selected" : "");} ?>>Sabtu</option>
					<option value="0" <?php if($_POST['mod']=="editData"){echo ($getData['hari'] == 0 ? "selected" : "");} ?>>Minggu</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_mapel_user" required>
					<?php
					$mapelUser = mysql_query("SELECT mapel_user.*, mapel.nama_mapel, user.nama_lengkap FROM mapel_user JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id ORDER BY mapel.nama_mapel, user.nama_lengkap");
					
					while($getMapelUser = mysql_fetch_array($mapelUser))
					{
						$selected = ($getData['id_mapel_user'] == $getMapelUser['id'] ? "selected" : "");
					?>
						<option value="<?=$getMapelUser['id'];?>" <?=$selected;?>><?=$getMapelUser['nama_mapel'];?> - <?=$getMapelUser['nama_lengkap'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>