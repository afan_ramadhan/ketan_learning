<div class="content-wrapper">
	<section class="content-header">
		<h1><?=$data_ujian['nama_mapel'];?></h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i><?=$data_ujian['nama_mapel'];?></li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<div id="waktu-ujian">
							<a class="btn btn-info btn-flat waktu_ujian"></a>
						</div>
					</div>
					<div class="box-body">
						<a class="btn btn-default" onclick="perbesarSoal()">
							<i class="fa fa-search-plus" aria-hidden="true"></i>
						</a>
						<a class="btn btn-default" onclick="perkecilSoal()">
							<i class="fa fa-search-minus" aria-hidden="true"></i>
						</a>
						<a class="btn btn-default" onclick="resetUkuranSoal()">
							<i class="fa fa-refresh" aria-hidden="true"></i>
						</a>
						<?php
						$nomor_soal = 1;
						mysql_data_seek($data_soal, 0);
						$jumlah_soal = mysql_num_rows($data_soal);
						while($ambil_soal = mysql_fetch_array($data_soal))
						{
							$soal_pertama = ($nomor_soal == 1 ? "" : "style='display: none;'");
							$soal_sebelumnya = ($nomor_soal == 1 ? "style='display: none;'" : "");
							$soal_berikutnya = ($nomor_soal == $jumlah_soal ? "style='display: none;'" : "");
							$selesai = ($nomor_soal == $jumlah_soal ? "" : "style='display: none;'");
							$pilihan_1_terpilih = ($ambil_soal['jawaban_siswa'] == 1 ? "Checklist.png" : "A.png");
							$pilihan_2_terpilih = ($ambil_soal['jawaban_siswa'] == 2 ? "Checklist.png" : "B.png");
							$pilihan_3_terpilih = ($ambil_soal['jawaban_siswa'] == 3 ? "Checklist.png" : "C.png");
							$pilihan_4_terpilih = ($ambil_soal['jawaban_siswa'] == 4 ? "Checklist.png" : "D.png");
							$pilihan_5_terpilih = ($ambil_soal['jawaban_siswa'] == 5 ? "Checklist.png" : "E.png");
							
							echo "
							<div id='soal_no_".$nomor_soal."' class='soal' $soal_pertama>
								<div class='nomor-soal'>Soal No. $nomor_soal <small class='text-muted'>(Dari $jumlah_soal Soal)</small></div>
								<div class='pertanyaan'>
									$ambil_soal[pertanyaan]";
									if($ambil_soal['file'] != "")
									{
										echo "
										<audio controls style='width: 100%'>
											<source src='manajemen/uploads/$ambil_soal[file]' type='audio/mpeg'>
										</audio>";
									}
								echo "
								</div>
								<div class='pilihan-ganda'>
									<table>
										".($ambil_soal['pilihan_1'] != "" ? "<tr>
											<td>
												<label>
													<input type='radio' value='1' onclick='pilihJawaban($nomor_soal, $data_ujian[id], $ambil_soal[id], $_SESSION[id], 1)'/>
													<img id='jawaban_no_".$nomor_soal."_1'  class='pilihan-ganda-detail' src='manajemen/images/$pilihan_1_terpilih'>
												</label>
											</td>
											<td>$ambil_soal[pilihan_1]</td>
										</tr>" : "")."
										".($ambil_soal['pilihan_2'] != "" ? "<tr>
											<td>
												<label>
													<input type='radio' value='2' onclick='pilihJawaban($nomor_soal, $data_ujian[id], $ambil_soal[id], $_SESSION[id], 2)'/>
													<img id='jawaban_no_".$nomor_soal."_2' class='pilihan-ganda-detail' src='manajemen/images/$pilihan_2_terpilih'>
												</label>
											</td>
											<td>$ambil_soal[pilihan_2]</td>
										</tr>" : "")."
										".($ambil_soal['pilihan_3'] != "" ? "<tr>
											<td>
												<label>
													<input type='radio' value='3' onclick='pilihJawaban($nomor_soal, $data_ujian[id], $ambil_soal[id], $_SESSION[id], 3)'/>
													<img id='jawaban_no_".$nomor_soal."_3'  class='pilihan-ganda-detail' src='manajemen/images/$pilihan_3_terpilih'>
												</label>
											</td>
											<td>$ambil_soal[pilihan_3]</td>
										</tr>" : "")."
										".($ambil_soal['pilihan_4'] != "" ? "<tr>
											<td>
												<label>
													<input type='radio' value='4' onclick='pilihJawaban($nomor_soal, $data_ujian[id], $ambil_soal[id], $_SESSION[id], 4)'/>
													<img id='jawaban_no_".$nomor_soal."_4'  class='pilihan-ganda-detail' src='manajemen/images/$pilihan_4_terpilih'>
												</label>
											</td>
											<td>$ambil_soal[pilihan_4]</td>
										</tr>" : "")."
										".($ambil_soal['pilihan_5'] != "" ? "<tr>
											<td>
												<label>
													<input type='radio' value='5' onclick='pilihJawaban($nomor_soal, $data_ujian[id], $ambil_soal[id], $_SESSION[id], 5)'/>
													<img id='jawaban_no_".$nomor_soal."_5'  class='pilihan-ganda-detail' src='manajemen/images/$pilihan_5_terpilih'>
												</label>
											</td>
											<td>$ambil_soal[pilihan_5]</td>
										</tr>" : "")."
									</table>
								</div>
								<div class=''>
									<a class='btn btn-default loading pull-left' onclick='pindahSebelumnya($nomor_soal)' $soal_sebelumnya>
										<i class='fa fa-chevron-left' aria-hidden='true' style='margin-right: 10px;'></i>
										Soal Sebelumnya
									</a>
									<a class='btn btn-default loading pull-right' onclick='pindahBerikutnya($nomor_soal)' $soal_berikutnya>
										Soal Berikutnya
										<i class='fa fa-chevron-right' aria-hidden='true' style='margin-left: 10px;'></i>
									</a>
									<a class='btn btn-default loading pull-right' onclick='if(confirm(\"Akhiri Ujian?\"))simpanNilai()' $selesai>
										Selesai
										<i class='fa fa-check' aria-hidden='true' style='margin-left: 10px;'></i>
									</a>
								</div>
							</div>";
							$nomor_soal++;
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>