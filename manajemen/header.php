<?php
$data_adminweb = mysql_query("SELECT user.*, level.nama_level FROM user LEFT JOIN level ON user.id_level = level.id WHERE user.id = '$_SESSION[id]'");
$ambil_data_adminweb = mysql_fetch_array($data_adminweb);
?>

<script>
	//Logout
	function logout(){
		var mod = "logout";
		$.ajax({
			type	: "POST",
			url		: "verifikasi.php",
			data	: "mod=" + mod,
			success: function(html)
			{
				$("#notifikasi").html(html);
			}
		})
	}
</script>

<header class="main-header">
    <a href="index.php" class="logo">
		<span class="logo-mini"><b><?=substr($lihat_konfigurasi['nama_aplikasi'], 0, 1);?></b></span>
		<span class="logo-lg"><b><?=$lihat_konfigurasi['nama_aplikasi'];?></b> <small><?=$lihat_konfigurasi['versi'];?></small></span>
    </a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<?php
						if($ambil_data_adminweb['gambar']=="")
						{
							echo "<img class='user-image' src='images/user_kosong.jpg'/>";
						}
						else
						{
							echo "<img class='user-image' src='images/user/$ambil_data_adminweb[gambar]'/>";
						}
						?>
						<span class="hidden-xs"><?=$ambil_data_adminweb['nama_lengkap'];?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<?php
							if($ambil_data_adminweb['gambar']=="")
							{
								echo "<img class='img-circle' src='images/user_kosong.jpg'/>";
							}
							else
							{
								echo "<img class='img-circle' src='images/user/$ambil_data_adminweb[gambar]'/>";
							}
							?>
							<p>
								<?=$ambil_data_adminweb['nama_lengkap'];?>
								<small><?=$ambil_data_adminweb['username'];?></small>
							</p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="index.php?mod=profil" class="btn btn-default btn-flat">Profil</a>
							</div>
							<div class="pull-right">
								<a href="#" class="btn btn-default btn-flat" onclick="logout()">Log out</a>
							</div>
						</li>
					</ul>
				</li>
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-question-circle-o"></i></a>
				</li>
			</ul>
		</div>
    </nav>
</header>

<div id="notifikasi"></div>