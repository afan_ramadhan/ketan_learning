<?php
session_start();
include "manajemen/libraries/fungsi_waktu.php";
include "manajemen/libraries/fungsi_user_agent.php";
include "manajemen/config/database.php";

if(empty($_SESSION['id']) and empty($_SESSION['username']) and empty($_SESSION['level']))
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else if($_SESSION['level'] != "siswa")
{
	echo "
	<script>
		window.location.href='login.php';
	</script>";
}
else if(!isset($_POST['jenis_materi']))
{
	echo "
	<script>
		window.location.href='index.php';
	</script>";
}
else if(!isset($_POST['id_materi']))
{
	echo "
	<script>
		window.location.href='index.php';
	</script>";
}
else
{
	$blokir = mysql_query("SELECT blokir FROM siswa WHERE username = '$_SESSION[username]'");
	$getBlokir = mysql_fetch_array($blokir);
	if($getBlokir['blokir'] == "Y")
	{
		session_destroy();
		
		echo "
		<script>
			window.location.href='login.php';
		</script>";
	}
}

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$data_siswa = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$_SESSION[id]'");
$ambil_data_siswa = mysql_fetch_array($data_siswa);

$jenis_materi = $_POST['jenis_materi'];
$id_materi = $_POST['id_materi'];

$data_materi = mysql_fetch_array(mysql_query("SELECT * FROM $jenis_materi WHERE id = '$id_materi'"));
?>

<!DOCTYPE html>

<html>
	
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<title><?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | Belajar</title>
			
		<link rel="icon" type="image/png" href="manajemen/images/ketanware_2.png">
		<link href="manajemen/assets/plugins/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/bootstrap/css/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/calendar/css/pickmeup.css" rel="stylesheet" type="text/css"/>
		<link href="manajemen/assets/plugins/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/icofont/css/icofont.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/datatables/datatables.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/alertifyjs/css/alertify.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/alertifyjs/css/themes/default.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/plugins/ionicons/css/ionicons.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/adminlte/css/AdminLTE.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/adminlte/css/skins/_all-skins.min.css" type="text/css" rel="stylesheet"/>
		<link href="manajemen/assets/ramadhan_afan/css/ramadhan_afan.css" type="text/css" rel="stylesheet"/>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" type="text/css" rel="stylesheet">

	</head>

	<body class="hold-transition <?=$ambil_data_siswa['tema'];?> layout-top-nav">
		
		<script src="manajemen/assets/plugins/jquery/jquery-1.12.3.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/moment.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/adminlte/js/adminlte.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/calendar/js/jquery.pickmeup.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/calendar/js/demo.js" type="text/javascript"></script>	
		<script src="manajemen/assets/plugins/datatables/datatables.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/highcharts/highcharts.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/alertifyjs/alertify.min.js" type="text/javascript"></script>
		<script src="manajemen/assets/plugins/preloaders/jquery.preloaders.js" type="text/javascript"></script>
		<script src="manajemen/assets/ramadhan_afan/js/ramadhan_afan.js" type="text/javascript"></script>

		<script>
			//Animasi Loading
			$(function(){
				$('.loading').click(function(){
					$.preloader.start();
					setTimeout(function(){$.preloader.stop();}, 500);
				});
			});
			
			function mulaiAnimasi()
			{
				$.preloader.start();
			}
			
			function stopAnimasi()
			{
				$.preloader.stop();
			}
			
			$(function(){
				mulaiBelajar();
			});
			
			function mulaiBelajar()
			{
				simpanOtomatis = setInterval(function(){
					simpanStatusBelajar();
				}, 60000);
			}
	
			function simpanStatusBelajar()
			{
				var mod = "simpanStatusBelajar"
				var jenis_materi = "<?=$jenis_materi;?>";
				var id_materi = "<?=$id_materi;?>";
				var id_siswa = "<?=$_SESSION['id'];?>";
				$.ajax({
					type	: "POST",
					url		: "module/belajar/belajar_response.php",
					data	: "mod=" + mod +
							  "&jenis_materi=" + jenis_materi +
							  "&id_materi=" + id_materi +
							  "&id_siswa=" + id_siswa,
					success: function()
					{
						
					},
					error: function()
					{
						alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Status Belajar Tidak Tersimpan, Periksa Koneksi Internet Anda!');
					}
				})
			}
	
			function simpanStatusDownload()
			{
				var mod = "simpanStatusDownload"
				var jenis_materi = "<?=$jenis_materi;?>";
				var id_materi = "<?=$id_materi;?>";
				var id_siswa = "<?=$_SESSION['id'];?>";
				$.ajax({
					type	: "POST",
					url		: "module/belajar/belajar_response.php",
					data	: "mod=" + mod +
							  "&jenis_materi=" + jenis_materi +
							  "&id_materi=" + id_materi +
							  "&id_siswa=" + id_siswa,
					success: function()
					{
						
					},
					error: function()
					{
						alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Status Download Tidak Tersimpan, Periksa Koneksi Internet Anda!');
					}
				})
			}
			
			function stopBelajar()
			{
				clearInterval(simpanOtomatis);
			}
		</script>
		
		<div class="wrapper">
		
			<?php
			include "header_belajar.php";
			include "content_belajar.php";
			include "footer.php";
			include "sidebar.php";
			?>
			
		</div>

	</body>
	
</html>