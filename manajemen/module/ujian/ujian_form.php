<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$nama_menu = "ujian";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = ($getHakAkses['s'] == 0 ? "WHERE ditambah_oleh = '$_SESSION[username]'" : "");

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM ujian WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['suspect_aktif'] = "Y";
	$getData['acak_soal'] = "Y";
}
?>

<script>
	$(function(){
		$('#tanggal_ujian_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#jam_mulai_').datetimepicker({
			format: 'HH:mm'
		});
		$('#jam_selesai_').datetimepicker({
			format: 'HH:mm'
		});
	})
	
	$(document).ready(function(){
		var mod = "ambilMapel";
		var id_ujian = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/ujian/ujian_response.php",
			data	: "mod=" + mod +
					  "&id_ujian=" + id_ujian +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
				ambilRombel();
			}
		})
	})
	
	function ambilMapel()
	{
		var mod = "ambilMapel";
		var id_ujian = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/ujian/ujian_response.php",
			data	: "mod=" + mod +
					  "&id_ujian=" + id_ujian +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
				ambilRombel();
			}
		})
	}
	
	function ambilRombel()
	{
		var mod = "ambilRombel";
		var id_ujian = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/ujian/ujian_response.php",
			data	: "mod=" + mod +
					  "&id_ujian=" + id_ujian +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_rombel").html(html);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Ujian";}else{echo "Tambah Ujian";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Tanggal Ujian</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="tanggal_ujian_">
					<input type="text" class="form-control" id="tanggal_ujian" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal_ujian'];}else{echo $tanggal_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-calendar"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jam Mulai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="jam_mulai_">
					<input type="text" class="form-control" id="jam_mulai" value="<?php if($_POST['mod']=="editData"){echo $getData['jam_mulai'];}else{echo $jam_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-clock-o"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jam Selesai</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<div class="input-group date" id="jam_selesai_">
					<input type="text" class="form-control" id="jam_selesai" value="<?php if($_POST['mod']=="editData"){echo $getData['jam_selesai'];}else{echo $jam_sekarang;} ?>" required/>
					<span class="input-group-addon">
						<span class="fa fa-clock-o"></span>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Jenis Ujian</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="jenis_ujian" required>
					<option value="1" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 1 ? "selected" : "");} ?>>Tugas Harian</option>
					<option value="2" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 2 ? "selected" : "");} ?>>Ulangan Harian</option>
					<option value="3" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 3 ? "selected" : "");} ?>>Ujian Tengah Semester (UTS)</option>
					<option value="4" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 4 ? "selected" : "");} ?>>Ujian Akhir Semester (UAS)</option>
					<option value="5" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 5 ? "selected" : "");} ?>>Try Out USBN</option>
					<option value="6" <?php if($_POST['mod']=="editData"){echo ($getData['jenis_ujian'] == 6 ? "selected" : "");} ?>>Ujian Sekolah Berstandar Nasional (USBN)</option>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Ajaran</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_ajaran'];}else{echo $lihat_konfigurasi['tahun_ajaran'];} ?>" readonly/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Semester</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" value="<?php if($_POST['mod']=="editData"){echo ($getData['semester'] == 1 ? "Ganjil" : "Genap");}else{echo ($lihat_konfigurasi['semester'] == 1 ? "Ganjil" : "Genap");} ?>" readonly/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kelas" onchange="ambilMapel()" required>
					<?php
					if($getHakAkses['s'] == 1)
					{
						$kelas = mysql_query("SELECT * FROM kelas ORDER BY nama_kelas");
					}
					else
					{
						$kelas = mysql_query("SELECT kelas.* FROM kelas JOIN mapel_user ON kelas.id = mapel_user.id_kelas AND mapel_user.id_user = '$_SESSION[id]' GROUP BY mapel_user.id_kelas ORDER BY nama_kelas");
					}
					
					while($getKelas = mysql_fetch_array($kelas))
					{
						$selected = ($getData['id_kelas'] == $getKelas['id'] ? "selected" : "");
					?>
						<option value="<?=$getKelas['id'];?>" <?=$selected;?>><?=$getKelas['nama_kelas'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_mapel" onchange="ambilRombel()" required>
					<?php
					if($getHakAkses['s'] == 1)
					{
						$mapel = mysql_query("SELECT * FROM mapel ORDER BY nama_mapel");
					}
					else
					{
						$mapel = mysql_query("SELECT mapel.* FROM mapel JOIN mapel_user ON mapel.id = mapel_user.id_mapel AND mapel_user.id_user = '$_SESSION[id]' ORDER BY nama_mapel");
					}
					
					while($getMapel = mysql_fetch_array($mapel))
					{
						$selected = ($getData['id_mapel'] == $getMapel['id'] ? "selected" : "");
					?>
						<option value="<?=$getMapel['id'];?>" <?=$selected;?>><?=$getMapel['nama_mapel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Rombel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_rombel" required>
					<?php
					$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
					while($getRombel = mysql_fetch_array($rombel))
					{
						$selected = ($getData['id_rombel'] == $getRombel['id'] ? "selected" : "");
					?>
						<option value="<?=$getRombel['id'];?>" <?=$selected;?>><?=$getRombel['nama_rombel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Paket Soal</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_paket_soal" required>
					<?php
					$paketSoal = mysql_query("SELECT * FROM paket_soal $s ORDER BY nama_paket_soal");
					while($getPaketSoal = mysql_fetch_array($paketSoal))
					{
						$selected = ($getData['id_paket_soal'] == $getPaketSoal['id'] ? "selected" : "");
					?>
						<option value="<?=$getPaketSoal['id'];?>" <?=$selected;?>><?=$getPaketSoal['nama_paket_soal'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Suspect Aktif</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="radio" name="suspect_aktif" value="Y" style="margin-right: 10px;" <?php if($getData['suspect_aktif'] == "Y"){echo "checked";} ?>> Ya
				<input type="radio" name="suspect_aktif" value="N" style="margin-left: 20px; margin-right: 10px;" <?php if($getData['suspect_aktif'] == "N" or $id == 0){echo "checked";} ?>> Tidak
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Acak Soal</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="radio" name="acak_soal" value="Y" style="margin-right: 10px;" <?php if($getData['acak_soal'] == "Y"){echo "checked";} ?>> Ya
				<input type="radio" name="acak_soal" value="N" style="margin-left: 20px; margin-right: 10px;" <?php if($getData['acak_soal'] == "N"){echo "checked";} ?>> Tidak
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>