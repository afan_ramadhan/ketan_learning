<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px; vertical-align: middle !important;}" : "");
$oderBy = ($mobile == true ? 0 : 4);
?>

<style>
	<?=$mobileSize;?>
	
	.customHeader {
		font-weight: bold;
		padding-right: 10px;
	}
</style>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ <?=$oderBy;?>, "desc" ]],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/absensi/data.php",
			"aoColumns": [
				<?php
				if($mobile == false)
				{
				?>
					null,
					null,
					{
						"mData": "2",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Ganjil";
							}
							else if(data == 2)
							{
								return "Genap";
							}
						}
					},
					null,
					null,
					null,
					null,
					{
						"mData": "7",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Hadir";
							}
							else if(data == 2)
							{
								return "Sakit";
							}
							else if(data == 3)
							{
								return "Izin";
							}
							else if(data == 4)
							{
								return "Alpa";
							}
						}
					},
					null,
					{
						"mData": "9",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Sangat Baik";
							}
							else if(data == 2)
							{
								return "Baik";
							}
							else if(data == 3)
							{
								return "Cukup";
							}
							else if(data == 4)
							{
								return "Kurang";
							}
						}
					},
					null,
				<?php
				}
				else
				{
				?>
					{
						"mData": "0",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<table>" + data + "</table>";
							
							return aksi;
						}
					}
				<?php
				}
				?>
			]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<?php
			if($mobile == false)
			{
			?>
				<th>Rombel</th>
				<th>Tahun Ajaran</th>
				<th>Semester</th>
				<th>Mapel</th>
				<th>Tanggal</th>
				<th>Jam</th>
				<th>Pertemuan Ke</th>
				<th>Kehadiran</th>
				<th>Keterangan</th>
				<th>Sikap</th>
				<th>Catatan</th>
			<?php
			}
			else
			{
			?>
				<th>Absensi</th>
			<?php
			}
			?>
			
		</tr>
	</thead>

	<tbody></tbody>
	
	<?php
	if($mobile == false)
	{
	?>
		<tfoot>
			<tr>
				<th class="filter">Rombel</th>
				<th class="filter">Tahun Ajaran</th>
				<th class="filter">Semester</th>
				<th class="filter">Mapel</th>
				<th class="filter">Tanggal</th>
				<th class="filter">Jam</th>
				<th class="filter">Pertemuan Ke</th>
				<th class="filter">Kehadiran</th>
				<th class="filter">Keterangan</th>
				<th class="filter">Sikap</th>
				<th class="filter">Catatan</th>
			</tr>
		</tfoot>
	<?php
	}
	?>
	
</table>