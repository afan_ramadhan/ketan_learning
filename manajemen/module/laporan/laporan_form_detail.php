<?php
session_start();
include "../../config/database.php";

$nama_menu = "laporan";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($_POST['mod']=="lihatDetail")
{
	$id_ujian = $_POST['id_ujian'];
	
	$data = mysql_query("SELECT ujian.*, mapel.kkm FROM ujian LEFT JOIN mapel ON ujian.id_mapel = mapel.id WHERE ujian.id = '$id_ujian'");
	$getData = mysql_fetch_array($data);
}
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom_1').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 1, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Detail Laporan</h4>
</div>
<div class="modal-body">

	<div class="detail scrolling">
		<table class="table table-bordered table-hover data_custom_1">

			<thead>
				<tr>
					<th style="width: 1px;">No.</th>
					<th>NIS</th>
					<th>NISN</th>
					<th>Nama Lengkap</th>
					<th>Username</th>
					<th>Benar</th>
					<th>Salah</th>
					<th>Kosong</th>
					<th>Nilai</th>
					<th>KKM</th>
					<th>#</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$nilai = mysql_query("SELECT siswa.*, nilai.id AS id_nilai, nilai.id_ujian, nilai.benar, nilai.salah, nilai.kosong, nilai.persentase FROM siswa LEFT JOIN nilai ON siswa.id = nilai.id_siswa WHERE nilai.id_ujian = '$id_ujian' ORDER BY siswa.nis");
				$tertinggiBenar = mysql_fetch_array(mysql_query("SELECT IFNULL(MAX(benar), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$tertinggiSalah = mysql_fetch_array(mysql_query("SELECT IFNULL(MAX(salah), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$tertinggiKosong = mysql_fetch_array(mysql_query("SELECT IFNULL(MAX(kosong), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$tertinggiNilai = mysql_fetch_array(mysql_query("SELECT IFNULL(MAX(persentase), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$terendahBenar = mysql_fetch_array(mysql_query("SELECT IFNULL(MIN(benar), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$terendahSalah = mysql_fetch_array(mysql_query("SELECT IFNULL(MIN(salah), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$terendahKosong = mysql_fetch_array(mysql_query("SELECT IFNULL(MIN(kosong), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$terendahNilai = mysql_fetch_array(mysql_query("SELECT IFNULL(MIN(persentase), 0) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$rataRataBenar = mysql_fetch_array(mysql_query("SELECT SUM(benar) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$rataRataSalah = mysql_fetch_array(mysql_query("SELECT SUM(salah) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$rataRataKosong = mysql_fetch_array(mysql_query("SELECT SUM(kosong) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$rataRataNilai = mysql_fetch_array(mysql_query("SELECT SUM(persentase) FROM nilai WHERE id_ujian = '$id_ujian'"));
				$jumlahSiswa = mysql_num_rows(mysql_query("SELECT * FROM nilai WHERE id_ujian = '$id_ujian'"));
				while($getNilai = mysql_fetch_array($nilai))
				{
					$persentase = (round($getNilai['persentase']) >= $getData['kkm'] ? "<p class='text-success'>".round($getNilai['persentase'])."</p>" : "<p class='text-danger'>".round($getNilai['persentase'])."</p>");
					echo "
					<tr>
						<td align='center'>$x</td>
						<td>$getNilai[nis]</td>
						<td>$getNilai[nisn]</td>
						<td>$getNilai[nama_lengkap]</td>
						<td>$getNilai[username]</td>
						<td class='nilai_$getNilai[id]' align='center'>".($getNilai['benar'] != "" ? $getNilai['benar'] : "-")."</td>
						<td class='nilai_$getNilai[id]' align='center'>".($getNilai['salah'] != "" ? $getNilai['salah'] : "-")."</td>
						<td class='nilai_$getNilai[id]' align='center'>".($getNilai['kosong'] != "" ? $getNilai['kosong'] : "-")."</td>
						<td class='nilai_$getNilai[id]' align='center'>".($getNilai['persentase'] != "" ? $persentase : "-")."</td>
						<td align='center'>$getData[kkm]</td>
						<td align='center'>";
							if(isset($getNilai['id_nilai']))
							{
								echo "
								<button id='lihatAnalisis_$getNilai[id]' type='button' class='btn btn-info btn-sm' onclick='lihatAnalisis($getNilai[id_ujian], $getNilai[id])'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>Lihat Analisis</button>
								<button id='hapus_$getNilai[id]' type='button' class='btn btn-danger btn-sm' onclick='hapusNilai($getNilai[id_nilai], $getNilai[id])' style='$d'><i class='fa fa-trash' aria-hidden='true' style='margin-right: 10px;'></i>Hapus</button>";
							}
						echo "
						</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
			<!--tfoot>
				<tr>
					<th colspan="5">Tertinggi</th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $tertinggiBenar[0]);?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $tertinggiSalah[0]);?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $tertinggiKosong[0]);?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : round($tertinggiNilai[0]));?></center></th>
					<th colspan="2" rowspan="3"></th>
				</tr>
				<tr>
					<th colspan="5">Terendah</th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $terendahBenar[0]);?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $terendahSalah[0]);?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : $terendahKosong[0]);?></center></th>
					<th style="border-right: 1px solid #f4f4f4;"><center><?=($jumlahSiswa == 0 ? "-" : round($terendahNilai[0]));?></center></th>
				</tr>
				<tr>
					<th colspan="5">Rata - Rata</th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : round($rataRataBenar[0] / $jumlahSiswa));?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : round($rataRataSalah[0] / $jumlahSiswa));?></center></th>
					<th><center><?=($jumlahSiswa == 0 ? "-" : round($rataRataKosong[0] / $jumlahSiswa));?></center></th>
					<th style="border-right: 1px solid #f4f4f4;"><center><?=($jumlahSiswa == 0 ? "-" : round($rataRataNilai[0] / $jumlahSiswa));?></center></th>
				</tr>
			</tfoot-->
			
		</table>
	</div>
	
</div>
<div class="modal-footer">
</div>