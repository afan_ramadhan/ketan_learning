<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "absensi_detail";
$sTableJoin1 = "absensi";
$sTableJoin2 = "rombel";
$sTableJoin3 = "mapel";

if($mobile == false)
{
	$aColumns = array('nama_rombel', 'tahun_ajaran', 'semester', 'nama_mapel', 'tanggal', 'jam', 'pertemuan_ke', 'kehadiran', 'keterangan', 'sikap', 'catatan'); 
}
else
{
	$aColumns = array('detail_absensi'); 
}

$sIndexColumn = "$sTable.id";

$s = (isset($_SESSION['username']) ? "AND $sTable.id_siswa = '$_SESSION[id]'" : "");
$ss = (isset($_SESSION['username']) ? "WHERE $sTable.id_siswa = '$_SESSION[id]'" : "");
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(strpos("ganjil", $sKeyword) !== false)
	{
		$sValue1 = "1";
	}
	else if(strpos("genap", $sKeyword) !== false)
	{
		$sValue1 = "2";
	}
	else
	{
		$sValue1 = "@";
	}
	
	if(strpos("hadir", $sKeyword) !== false)
	{
		$sValue2 = "1";
	}
	else if(strpos("sakit", $sKeyword) !== false)
	{
		$sValue2 = "2";
	}
	else if(strpos("izin", $sKeyword) !== false)
	{
		$sValue2 = "3";
	}
	else if(strpos("alpa", $sKeyword) !== false)
	{
		$sValue2 = "4";
	}
	else
	{
		$sValue2 = "@";
	}
	
	if(strpos("baik", $sKeyword) !== false)
	{
		$sValue3 = "2";
	}
	else if(strpos("sangat baik", $sKeyword) !== false)
	{
		$sValue3 = "1";
	}
	else if(strpos("cukup", $sKeyword) !== false)
	{
		$sValue3 = "3";
	}
	else if(strpos("kurang", $sKeyword) !== false)
	{
		$sValue3 = "4";
	}
	else
	{
		$sValue3 = "@";
	}
	
	$sWhere = "WHERE
	$sTableJoin2.nama_rombel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin1.tahun_ajaran LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin1.semester LIKE '%" . $sValue1 . "%' $s OR
	$sTableJoin3.nama_mapel LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin1.tanggal LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin1.jam LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTableJoin1.pertemuan_ke LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.kehadiran LIKE '%" . $sValue2 . "%' $s OR
	$sTable.keterangan LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.sikap LIKE '%" . $sValue3 . "%' $s OR
	$sTable.catatan LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s";
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 2)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("ganjil", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("genap", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else if($i == 7)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("hadir", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("sakit", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else if(strpos("izin", $bKeyword) !== false)
			{
				$bValue = "3";
			}
			else if(strpos("alpa", $bKeyword) !== false)
			{
				$bValue = "4";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else if($i == 9)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("baik", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else if(strpos("sangat baik", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("cukup", $bKeyword) !== false)
			{
				$bValue = "3";
			}
			else if(strpos("kurang", $bKeyword) !== false)
			{
				$bValue = "4";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTableJoin2.nama_rombel, $sTableJoin1.tahun_ajaran, $sTableJoin1.semester, $sTableJoin3.nama_mapel, $sTableJoin1.tanggal, $sTableJoin1.jam, $sTableJoin1.pertemuan_ke, $sTable.kehadiran, $sTable.keterangan, $sTable.sikap, $sTable.catatan,
	CONCAT(
		'<tr>
			<td class=customHeader>Tanggal</td>
			<td class=customHeader>:</td>
			<td>', tanggal, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Jam</td>
			<td class=customHeader>:</td>
			<td>', jam, ' WIB </td>
		</tr>',
		'<tr>
			<td class=customHeader>Rombel</td>
			<td class=customHeader>:</td>
			<td>', nama_rombel, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Tahun Ajaran</td>
			<td class=customHeader>:</td>
			<td>', tahun_ajaran, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Semester</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN semester = 1 THEN 'Ganjil' ELSE 'Genap'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Mapel</td>
			<td class=customHeader>:</td>
			<td>', nama_mapel, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Pertemuan Ke</td>
			<td class=customHeader>:</td>
			<td>', pertemuan_ke, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Kehadiran</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN kehadiran = 1 THEN 'Hadir'
					WHEN kehadiran = 2 THEN 'Sakit'
					WHEN kehadiran = 3 THEN 'Izin'
					WHEN kehadiran = 4 THEN 'Alpa'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Keterangan</td>
			<td class=customHeader>:</td>
			<td>', $sTable.keterangan, '</td>
		</tr>',
		'<tr>
			<td class=customHeader>Sikap</td>
			<td class=customHeader>:</td>
			<td>',
				CASE
					WHEN sikap = 1 THEN 'Sangat Baik'
					WHEN sikap = 2 THEN 'Baik'
					WHEN sikap = 3 THEN 'Cukup'
					WHEN sikap = 4 THEN 'Kurang'
				END,
			'</td>
		</tr>',
		'<tr>
			<td class=customHeader>Catatan</td>
			<td class=customHeader>:</td>
			<td>', catatan, '</td>
		</tr>'
	) AS detail_absensi
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_absensi = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTableJoin1.id_rombel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTableJoin1.id_mapel = $sTableJoin3.id
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable
	LEFT JOIN $sTableJoin1 ON $sTable.id_absensi = $sTableJoin1.id
	LEFT JOIN $sTableJoin2 ON $sTableJoin1.id_rombel = $sTableJoin2.id
	LEFT JOIN $sTableJoin3 ON $sTableJoin1.id_mapel = $sTableJoin3.id";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>