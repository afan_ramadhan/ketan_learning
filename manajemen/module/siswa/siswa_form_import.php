<?php
include "../../config/database.php";
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Import Siswa
	</h4>
</div>
<div class="modal-body">

	<a class="btn btn-info" href="files/Format_Import_Siswa.xlsx"><i class="fa fa-file-excel-o" aria-hidden="true" style="margin-right: 10px;"></i>Download Format Import</a>
	
	<br/>
	<br/>
	
	<table class="table table-condensed">
		<thead>
			<tr>
				<th>Nama Rombel</th>
				<th>ID Rombel</th>
			</tr>
		</thead>
		<tbody>
			
			<?php
			$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
			while($getRombel = mysql_fetch_array($rombel))
			{
			?>
				
				<tr>
					<td><?=$getRombel['nama_rombel'];?></td>
					<td><b><u><?=$getRombel['id'];?></u></b></td>
				</tr>
				
			<?php
			}
			?>
			
		</tbody>
	</table>
	
	<hr/>
	
	<table class="table table-hover">
		
		<tr>
			<td style="border: none;">
				<label class="control-label">File</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="file" class="form-control" id="file" name="file"/>
				<p class="help-block">File Harus Berformat Excel.</p>
			</td>
		</tr>
		
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="simpanDataImport" onclick="simpanDataImport()"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Upload</button>
</div>