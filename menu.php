<?php
$mod = (isset($_GET['mod']) ? $_GET['mod']: "");

$rombelUser = mysql_query("SELECT id_rombel FROM siswa WHERE id = '$_SESSION[id]'");
$ambilRombellUser = mysql_fetch_array($rombelUser);
?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<?php
				if($ambil_data_siswa['gambar']=="")
				{
					echo "<img class='img-circle' src='manajemen/images/user_kosong.jpg'/>";
				}
				else
				{
					echo "<img class='img-circle' src='manajemen/images/siswa/$ambil_data_siswa[gambar]'/>";
				}
				?>
			</div>
			<div class="pull-left info">
				<p><?=$ambil_data_siswa['nama_lengkap'];?></p>
				<small><?=$ambil_data_siswa['nama_rombel'];?></small>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header" style="text-align: center;"><?=strtoupper($lihat_konfigurasi['nama_aplikasi']);?> MENU</li>
			<li <?php if(!isset($_GET['mod'])){echo "class='active'";} ?>><a class="loading" href="index.php"><i class="fa fa-dashboard" style="margin-right: 10px;"></i><span>Dashboard</span></a></li>
			<li <?php if($mod == "jadwal_rombel"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=jadwal_rombel"><i class="fa fa-calendar" style="margin-right: 10px;"></i><span>Jadwal Rombel</span></a></li>
			<li <?php if($mod == "absensi"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=absensi"><i class="fa fa-calendar-check-o" style="margin-right: 10px;"></i><span>Absensi</span></a></li>
			<li <?php if($mod == "ebook"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=ebook"><i class="fa fa-book" style="margin-right: 10px;"></i><span>Ebook</span></a></li>
			<li <?php if($mod == "video"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=video"><i class="fa fa-file-video-o" style="margin-right: 10px;"></i><span>Video</span></a></li>
			<li <?php if($mod == "ujian"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=ujian"><i class="fa fa-flag-checkered" style="margin-right: 10px;"></i><span>Ujian</span></a></li>
			<li <?php if($mod == "laporan"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=laporan"><i class="fa fa fa-file-text" style="margin-right: 10px;"></i><span>Laporan</span></a></li>
			<li <?php if($mod == "progres_belajar"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=progres_belajar"><i class="fa fa-tasks" style="margin-right: 10px;"></i><span>Progres Belajar</span></a></li>
			<li <?php if($mod == "saran"){echo "class='active'";} ?>><a class="loading" href="index.php?mod=saran"><i class="fa fa-commenting-o" style="margin-right: 10px;"></i><span>Saran</span></a></li>
		</ul>
	</section>
</aside>
