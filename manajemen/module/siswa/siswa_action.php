<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "siswa";

if($_POST['mod']=="simpanData")
{
	$id_rombel = $_POST['id_rombel'];
	$nis = mysql_real_escape_string($_POST['nis']);
	$nisn = mysql_real_escape_string($_POST['nisn']);
	$nama_lengkap = mysql_real_escape_string($_POST['nama_lengkap']);
	$username = mysql_real_escape_string($_POST['username']);
	$password = md5(mysql_real_escape_string($_POST['password']));
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$tempat_lahir = mysql_real_escape_string($_POST['tempat_lahir']);
	$tanggal_lahir = mysql_real_escape_string($_POST['tanggal_lahir']);
	$agama = $_POST['agama'];
	$alamat = mysql_real_escape_string($_POST['alamat']);
	$email = mysql_real_escape_string($_POST['email']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$nama_ayah = mysql_real_escape_string($_POST['nama_ayah']);
	$nama_ibu = mysql_real_escape_string($_POST['nama_ibu']);
	
	if(empty($_POST['nama_lengkap']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Lengkap Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['username']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['password']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Password Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$cekUsername = mysql_num_rows(mysql_query("SELECT username FROM $nama_tabel WHERE username = '$username'"));
		if($cekUsername > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Sudah Digunakan!');
			</script>";
		}
		else
		{
			$simpanData = mysql_query("INSERT INTO $nama_tabel (id_rombel, nis, nisn, nama_lengkap, username, password, jenis_kelamin, tempat_lahir, tanggal_lahir, agama, alamat, email, nomor_telepon, nama_ayah, nama_ibu, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_rombel', '$nis', '$nisn', '$nama_lengkap', '$username', '$password', '$jenis_kelamin', '$tempat_lahir', '$tanggal_lahir', '$agama', '$alamat', '$email', '$nomor_telepon', '$nama_ayah', '$nama_ibu', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
			if($simpanData)
			{
				$aktivitas = "Tambah Siswa";
				$keterangan = mysql_real_escape_string("Menambahkan '$nama_lengkap' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Ditambahkan!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
				</script>
				";
			}
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_rombel = $_POST['id_rombel'];
	$nis = mysql_real_escape_string($_POST['nis']);
	$nisn = mysql_real_escape_string($_POST['nisn']);
	$nama_lengkap = mysql_real_escape_string($_POST['nama_lengkap']);
	$username = mysql_real_escape_string($_POST['username']);
	$password = md5(mysql_real_escape_string($_POST['password']));
	$jenis_kelamin = $_POST['jenis_kelamin'];
	$tempat_lahir = mysql_real_escape_string($_POST['tempat_lahir']);
	$tanggal_lahir = mysql_real_escape_string($_POST['tanggal_lahir']);
	$agama = $_POST['agama'];
	$alamat = mysql_real_escape_string($_POST['alamat']);
	$email = mysql_real_escape_string($_POST['email']);
	$nomor_telepon = mysql_real_escape_string($_POST['nomor_telepon']);
	$nama_ayah = mysql_real_escape_string($_POST['nama_ayah']);
	$nama_ibu = mysql_real_escape_string($_POST['nama_ibu']);
	$blokir = $_POST['blokir'];
	
	if(empty($_POST['nama_lengkap']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Lengkap Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['username']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$siswa = mysql_query("SELECT username FROM $nama_tabel WHERE id = '$id'");
		$ambilSiswa = mysql_fetch_array($siswa);
		$cekSiswa = mysql_num_rows(mysql_query("SELECT username FROM $nama_tabel WHERE username = '$username'"));
		if($ambilSiswa['username'] != $username and $cekSiswa > 0)
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Username Sudah Digunakan!');
			</script>";
		}
		else
		{
			$perbaruiSaran = mysql_query("UPDATE saran SET ditambah_oleh = '$username' WHERE ditambah_oleh = '$ambilUser[username]'");
			
			$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_rombel = '$id_rombel', nis = '$nis', nisn = '$nisn', nama_lengkap = '$nama_lengkap', username = '$username', jenis_kelamin = '$jenis_kelamin', tempat_lahir = '$tempat_lahir', tanggal_lahir = '$tanggal_lahir', agama = '$agama', alamat = '$alamat', email = '$email', nomor_telepon = '$nomor_telepon', nama_ayah = '$nama_ayah', nama_ibu = '$nama_ibu', blokir = '$blokir', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
			if(!empty($_POST['password']))
			{
				mysql_query("UPDATE $nama_tabel SET password = '$password' WHERE id = '$id'");
			}
			
			if($perbaruiData)
			{
				$aktivitas = "Perbarui Siswa";
				$keterangan = mysql_real_escape_string("Memperbarui '$nama_lengkap' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
				</script>
				";
			}
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_lengkap FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_lengkap'];
	
	$ambilSiswa = mysql_fetch_array(mysql_query("SELECT * FROM $nama_tabel WHERE id = '$id'"));
		
	if($ambilSiswa['gambar'] != "")
	{
		unlink ("../../images/siswa/$ambilSiswa[gambar]");
	}
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
	$hapusDataKantong = mysql_query("DELETE FROM kantong_jawaban WHERE id_siswa = '$id'");
	$hapusDataNilai = mysql_query("DELETE FROM nilai WHERE id_siswa = '$id'");
	$hapusDataAbsensi = mysql_query("DELETE FROM absensi_detail WHERE id_siswa = '$id'");
	
	if($hapusData)
	{
		$aktivitas = "Hapus Siswa";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$dataSiswa = mysql_query("SELECT * FROM $nama_tabel WHERE id IN ($data_terpilih)");
	while($ambilSiswa = mysql_fetch_array($dataSiswa))
	{
		if($ambilSiswa['gambar'] != "")
		{
			unlink ("../../images/siswa/$ambilSiswa[gambar]");	
		}
	}
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
	$hapusDataKantong = mysql_query("DELETE FROM kantong_jawaban WHERE id_siswa IN ($data_terpilih)");
	$hapusDataNilai = mysql_query("DELETE FROM nilai WHERE id_siswa IN ($data_terpilih)");
	$hapusDataAbsensi = mysql_query("DELETE FROM absensi_detail WHERE id_siswa IN ($data_terpilih)");
	
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="aktifkanDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$aktifkanDataTerpilih = mysql_query("UPDATE $nama_tabel SET blokir = 'N', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($aktifkanDataTerpilih)
	{
		$aktivitas = "Aktifkan Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Mengaktifkan Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="blokirDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$blokirDataTerpilih = mysql_query("UPDATE $nama_tabel SET blokir = 'Y', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($blokirDataTerpilih)
	{
		$aktivitas = "Blokir Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Memblokir Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="pindahRombelDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	$id_rombel = $_POST['id_rombel'];
	
	$pindahRombelDataTerpilih = mysql_query("UPDATE $nama_tabel SET id_rombel = '$id_rombel', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($pindahRombelDataTerpilih)
	{
		$aktivitas = "Perbarui Siswa Terpilih";
		$keterangan = mysql_real_escape_string("Memperbarui Siswa Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Siswa Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
		</script>
		";
	}
}

if($_POST['mod']=="simpanDataImport")
{
	if(!empty($_FILES['file']['name']))
	{
		$target_dir = "../../uploads/import/";
		$name_file = basename($_FILES['file']['name']) ;
		$target_file = $target_dir . $name_file;
		move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

		require_once "../../libraries/PHPExcel.php";
		require_once "../../libraries/PHPExcel/IOFactory.php";

		$file = $target_dir . $name_file;

		$objPHPExcel = PHPExcel_IOFactory::load($file);
		
		$success = 0;
		$error = 0;
		$keterangan_error = "";
		
		foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
		{
			$totalrow = $worksheet->getHighestRow();
			
			for($row = 2; $row <= $totalrow; $row++)
			{
				$id_rombel = mysql_escape_string($worksheet->getCellByColumnAndRow(0, $row)->getValue());
				$nis = mysql_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
				$nisn = mysql_escape_string($worksheet->getCellByColumnAndRow(2, $row)->getValue());
				$nama_lengkap = mysql_escape_string($worksheet->getCellByColumnAndRow(3, $row)->getValue());
				$username = mysql_escape_string($worksheet->getCellByColumnAndRow(4, $row)->getValue());
				$jenis_kelamin = mysql_escape_string($worksheet->getCellByColumnAndRow(5, $row)->getValue());
				$tempat_lahir = mysql_escape_string($worksheet->getCellByColumnAndRow(6, $row)->getValue());
				$tanggal_lahir = mysql_escape_string($worksheet->getCellByColumnAndRow(7, $row)->getValue());
				$agama = mysql_escape_string($worksheet->getCellByColumnAndRow(8, $row)->getValue());
				$alamat = mysql_escape_string($worksheet->getCellByColumnAndRow(9, $row)->getValue());
				$email = mysql_escape_string($worksheet->getCellByColumnAndRow(10, $row)->getValue());
				$nomor_telepon = mysql_escape_string($worksheet->getCellByColumnAndRow(11, $row)->getValue());
				$nama_ayah = mysql_escape_string($worksheet->getCellByColumnAndRow(12, $row)->getValue());
				$nama_ibu = mysql_escape_string($worksheet->getCellByColumnAndRow(13, $row)->getValue());
				
				if($nama_lengkap == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Nama Lengkap Tidak Boleh Kosong.";
				}
				else if($username == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Username Tidak Boleh Kosong.";
				}
				else
				{
					$cekUsername = mysql_num_rows(mysql_query("SELECT username FROM $nama_tabel WHERE username = '$username'"));
					if($cekUsername > 0)
					{
						$error++;
						$keterangan_error .= "<br/>Pada Baris $row, Username Sudah Digunakan.";
					}
					else
					{
						$simpanDataImport = mysql_query("INSERT INTO $nama_tabel (id_rombel, nis, nisn, nama_lengkap, username, password, jenis_kelamin, tempat_lahir, tanggal_lahir, agama, alamat, email, nomor_telepon, nama_ayah, nama_ibu, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_rombel', '$nis', '$nisn', '$nama_lengkap', '$username', '827ccb0eea8a706c4c34a16891f84e7b', '$jenis_kelamin', '$tempat_lahir', '$tanggal_lahir', '$agama', '$alamat', '$email', '$nomor_telepon', '$nama_ayah', '$nama_ibu', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
						
						if($simpanDataImport)
						{
							$aktivitas = "Tambah Siswa";
							$keterangan = mysql_real_escape_string("Menambahkan '$nama_lengkap' Pada Tabel '$nama_tabel'");
							$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
							
							$success++;
						}
						else
						{
							$error++;
							$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
						}
					}
				}	
			}
        }
		
		unlink($target_file);
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Siswa Ditambahkan, $error Error! $keterangan_error');
			$('#form').modal('hide');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}
?>