<?php
include "libraries/fungsi_waktu.php";

$nama_menu = "absensi";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.s, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Absensi";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/absensi/absensi_database.php");
		})
		
		//Tambah Data
		function tambahData()
		{
			var mod = "tambahData";
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Simpan Data
		function simpanData()
		{
			mulaiAnimasi();
			var data = $("#formAbsensi").serialize();
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_action.php",
				data	: data,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/absensi/absensi_database.php");
				}
			})
		}
		
		//Edit Data
		function editData(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData()
		{
			mulaiAnimasi();
			var data = $("#formAbsensi").serialize();
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_action.php",
				data	: data,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/absensi/absensi_database.php");
				}
			})
		}
		
		//Hapus Data
		function hapusData(id)
		{
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusData";
				var id = id;
				$.ajax({
					type	: "POST",
					url		: "module/absensi/absensi_action.php",
					data	: "mod=" + mod +
							  "&id=" + id,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/absensi/absensi_database.php");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/absensi/absensi_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/absensi/absensi_database.php");
					}
				})
			}
		}
		
		//Lihat Detail
		function lihatDetail(id)
		{
			var mod = "lihatDetail";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_form_detail.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent_").html(html);
					$("#form_").modal();
				}
			})
		}
		
		//Cetak Detail
		function cetak()
		{
			var printContents = document.getElementById('print-area').innerHTML;
			var originalContents = document.body.innerHTML;
			document.body.innerHTML = printContents;
			window.print();
			document.body.innerHTML = originalContents;
		}
		
		$(function(){
			$('#tanggal_awal_').datetimepicker({
				format: 'YYYY-MM-DD',
			});
			$('#tanggal_akhir_').datetimepicker({
				format: 'YYYY-MM-DD',
			});
		})
		
		//Filter Absensi
		function filterAbsensi()
		{
			mulaiAnimasi();
			var mod = "filterAbsensi";
			var tanggal_awal = $("#tanggal_awal").val();
			var tanggal_akhir = $("#tanggal_akhir").val();
			var rombel = $("#rombel").val();
			var kehadiran = $("#kehadiran").val();
			$.ajax({
				type	: "POST",
				url		: "module/absensi/absensi_database_filter.php",
				data	: "mod=" + mod +
						  "&tanggal_awal=" + tanggal_awal +
						  "&tanggal_akhir=" + tanggal_akhir +
						  "&rombel=" + rombel +
						  "&kehadiran=" + kehadiran,
				success: function(html)
				{
					stopAnimasi();
					$("#tampilData_").html(html);
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Absensi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Absensi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Absensi</h3>
						</div>
						<div class="box-body">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#master_absensi">Master Absensi</a></li>
								<li><a data-toggle="tab" href="#filter_absensi">Filter Absensi</a></li>
							</ul>
							<div class="tab-content">
								<div id="master_absensi" class="tab-pane fade in active">
									<h3>Master Absensi</h3>
									<button type="button" class="btn btn-primary btn-md" onclick="tambahData()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah Absensi</button>
									<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
										<input type="checkbox" id="pilihSemua" style="margin-right: 10px;">Pilih Semua
									</button>
									<button type="button" id="hapusDataTerpilih" class="btn btn-danger" onclick="hapusDataTerpilih()" style="<?=$d;?>">
										<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus Absensi Terpilih</span>
									</button>
									<div class="modal fade" id="form" role="dialog">
										<div class="modal-dialog modal-lg">
											<div id="formContent" class="modal-content"></div>
											<div id="notifikasi" class="modal-content"></div>
										</div>
									</div>			
									<div class="modal fade" id="form_" role="dialog">
										<div class="modal-dialog modal-lg">
											<div id="formContent_" class="modal-content"></div>
											<div id="notifikasi_" class="modal-content"></div>
										</div>
									</div>
									<div class="modal fade" id="print" role="dialog">
										<div class="modal-dialog modal-lg" style="width: 210mm;">
											<div id="printContent" class="modal-content"></div>
										</div>
									</div>	
									<br/>
									<br/>  
									<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
								</div>
								<div id="filter_absensi" class="tab-pane fade">
									<h3>Filter Absensi</h3>
									<table>
										<tr>
											<td><label>Tanggal Awal</label></td>
											<td style="width: 200px; padding: 5px;">
												<div class="input-group date" id="tanggal_awal_">
													<input type="text" class="form-control" id="tanggal_awal" value="<?=$tanggal_sekarang;?>" required/>
													<span class="input-group-addon">
														<span class="fa fa-calendar"></span>
													</span>
												</div>
											</td>
										</tr>
										<tr>
											<td><label>Tanggal Akhir</label></td>
											<td style="width: 200px; padding: 5px;">
												<div class="input-group date" id="tanggal_akhir_">
													<input type="text" class="form-control" id="tanggal_akhir" value="<?=$tanggal_sekarang;?>" required/>
													<span class="input-group-addon">
														<span class="fa fa-calendar"></span>
													</span>
												</div>
											</td>
										</tr>
										<tr>
											<td><label>Rombel</label></td>
											<td style="width: 200px; padding: 5px;">
												<select class="form-control" id="rombel" required>
													<option value="">Semua</option>
													<?php
													$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
													while($getRombel = mysql_fetch_array($rombel))
													{
														echo "<option value='$getRombel[id]'>$getRombel[nama_rombel]</option>";
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td><label>Kehadiran</label></td>
											<td style="width: 200px; padding: 5px;">
												<select class="form-control" id="kehadiran" required>
													<option value="">Semua</option>
													<option value="1">Hadir</option>
													<option value="2">Sakit</option>
													<option value="3">Izin</option>
													<option value="4">Alpa</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right" colspan="2" style="padding: 5px;">
												<button type="button" id="filterAbsensi" class="btn btn-default" onclick="filterAbsensi()">
													<i class="fa fa-filter" aria-hidden="true" style="margin-right: 10px;"></i>Filter</span>
												</button>
											</td>
										</tr>
									</table>
									<br/>
									<div id="tampilData_" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Absensi</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li class="active">Absensi</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>