<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM mapel_user WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['id_user'] = 0;
	$getData['id_kelas'] = 0;
}

$disabled = (!empty($_GET['id']) ? "disabled" : "");
$id_user = (!empty($_GET['id']) ? $_GET['id'] : $getData['id_user']);
?>

<script>
	$(document).ready(function(){
		var mod = "ambilMapel";
		var id_mapel_user = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/mapel_user/mapel_user_response.php",
			data	: "mod=" + mod +
					  "&id_mapel_user=" + id_mapel_user +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
			}
		})
	})
	
	function ambilMapel()
	{
		var mod = "ambilMapel";
		var id_mapel_user = <?=$id;?>;
		var id_kelas = $( "#id_kelas option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/mapel_user/mapel_user_response.php",
			data	: "mod=" + mod +
					  "&id_mapel_user=" + id_mapel_user +
					  "&id_kelas=" + id_kelas,
			success: function(html)
			{
				$("#id_mapel").html(html);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Mapel User";}else{echo "Tambah Mapel User";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">User</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_user" required <?=$disabled;?>>
					<?php
					$user = mysql_query("SELECT * FROM user ORDER BY nama_lengkap");
					while($getUser = mysql_fetch_array($user))
					{
						$selected = ($id_user == $getUser['id'] ? "selected" : "");
					?>
						<option value="<?=$getUser['id'];?>" <?=$selected;?>><?=$getUser['nama_lengkap'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kelas" onchange="ambilMapel()" required>
					<?php
					$kelas = mysql_query("SELECT * FROM kelas ORDER BY nama_kelas");
					while($getKelas = mysql_fetch_array($kelas))
					{
						$selected = ($getData['id_kelas'] == $getKelas['id'] ? "selected" : "");
					?>
						<option value="<?=$getKelas['id'];?>" <?=$selected;?>><?=$getKelas['nama_kelas'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_mapel" required>
					<?php
					$mapel = mysql_query("SELECT * FROM mapel ORDER BY nama_mapel");
					
					while($getMapel = mysql_fetch_array($mapel))
					{
						$selected = ($getData['id_mapel'] == $getMapel['id'] ? "selected" : "");
					?>
						<option value="<?=$getMapel['id'];?>" <?=$selected;?>><?=$getMapel['nama_mapel'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>