<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px; vertical-align: middle !important;}" : "");
?>

<style>
	<?=$mobileSize;?>
	
	.customHeader {
		font-weight: bold;
		padding-right: 10px;
	}
</style>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var tanggal_sekarang = new Date();
		var hari = ("0" + (tanggal_sekarang.getDate())).slice(-2)
		var bulan = ("0" + (tanggal_sekarang.getMonth() + 1)).slice(-2)
		var tahun = tanggal_sekarang.getFullYear();
		tanggal_sekarang = tahun + '-' + bulan + '-' + hari;
		
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 0, "asc" ]],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/ujian/data.php",
			"aoColumns": [
				<?php
				if($mobile == false)
				{
				?>
					null,
					null,
					null,
					{
						"mData": "3",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Tugas Harian";
							}
							else if(data == 2)
							{
								return "Ulangan Harian";
							}
							else if(data == 3)
							{
								return "Ujian Tengah Semester (UTS)";
							}
							else if(data == 4)
							{
								return "Ujian Akhir Semester (UAS)";
							}
							else if(data == 5)
							{
								return "Try Out USBN";
							}
							else if(data == 6)
							{
								return "Ujian Sekolah Berstandar Nasional (USBN)";
							}
						}
					},
					null,
					{
						"mData": "5",
						"mRender": function(data)
						{	
							if(data == 1)
							{
								return "Ganjil";
							}
							else if(data == 2)
							{
								return "Genap";
							}
						}
					},
					null,
					{
						"mData": "7",
						"mRender": function(data, type, full)
						{	
							var suspect_aktif = (data == "N" ? "<i class='fa fa-circle text-danger'></i>" : "<i class='fa fa-circle text-success'></i>");
							var aksi = "";
							aksi += "<center>" + suspect_aktif + "</center>";
							
							return aksi;
						}
					},
					{
						"mData": "8",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							
							if(full[0] == tanggal_sekarang)
							{
								var jam_mulai = full[1][0] + full[1][1];
								var menit_mulai = full[1][3] + full[1][4];
								var jam_selesai = full[2][0] + full[2][1];
								var menit_selesai = full[2][3] + full[2][4];
								
								var start_a = parseInt(jam_mulai);
								var start_b = parseInt(menit_mulai);
								var start = start_a * 60 + start_b;
								var end_a = parseInt(jam_selesai);
								var end_b = parseInt(menit_selesai);
								var end = end_a * 60 + end_b;
								var now = new Date().getHours() * 60 + new Date().getMinutes();

								if(now > start && now <= end)
								{
									aksi += "<form action='ujian.php' role='form' method='POST'><input type='hidden' name='id_ujian' value='" + full[8] + "'/><button type='submit' class='btn btn-info btn-sm'><i class='fa fa-play' aria-hidden='true' style='margin-right: 10px;'></i>Kerjakan</button></form>";
								}
								
								return aksi;
							}
							else
							{
								return aksi;
							}
						}
					}
				<?php
				}
				else
				{
				?>
					null,
					null,
					null,
					{
						"mData": "3",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<table>" + data + "</table>";
							
							return aksi;
						}
					},
					{
						"mData": "4",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							
							if(full[0] == tanggal_sekarang)
							{
								var jam_mulai = full[1][0] + full[1][1];
								var menit_mulai = full[1][3] + full[1][4];
								var jam_selesai = full[2][0] + full[2][1];
								var menit_selesai = full[2][3] + full[2][4];
								
								var start_a = parseInt(jam_mulai);
								var start_b = parseInt(menit_mulai);
								var start = start_a * 60 + start_b;
								var end_a = parseInt(jam_selesai);
								var end_b = parseInt(menit_selesai);
								var end = end_a * 60 + end_b;
								var now = new Date().getHours() * 60 + new Date().getMinutes();

								if(now > start && now <= end)
								{
									aksi += "<form action='ujian.php' role='form' method='POST'><input type='hidden' name='id_ujian' value='" + full[4] + "'/><button type='submit' class='btn btn-info btn-sm'><i class='fa fa-play' aria-hidden='true' style='margin-right: 10px;'></i>Kerjakan</button></form>";
								}
								
								return aksi;
							}
							else
							{
								return aksi;
							}
						}
					}
				<?php
				}
				?>
			]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<?php
			if($mobile == false)
			{
			?>
				<th>Tanggal Ujian</th>
				<th>Jam Mulai</th>
				<th>Jam Selesai</th>
				<th>Jenis Ujian</th>
				<th>Tahun Ajaran</th>
				<th>Semester</th>
				<th>Mapel</th>
				<th>Suspect Aktif</th>
			<?php
			}
			else
			{
			?>
				<th>Tanggal</th>
				<th>Jam Mulai</th>
				<th>Jam Selesai</th>
				<th>Ujian</th>
			<?php
			}
			?>
			
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<?php
	if($mobile == false)
	{
	?>
		<tfoot>
			<tr>
				<th class="filter">Tanggal Ujian</th>
				<th class="filter">Jam Mulai</th>
				<th class="filter">Jam Selesai</th>
				<th class="filter">Jenis Ujian</th>
				<th class="filter">Tahun Ajaran</th>
				<th class="filter">Semester</th>
				<th class="filter">Mapel</th>
				<th class="filter">Suspect Aktif</th>
				<th>#</th>
			</tr>
		</tfoot>
	<?php
	}
	?>
	
</table>