<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM konfigurasi WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		Edit Konfigurasi
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Sekolah / Bimbel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_sekolah" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_sekolah'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Alamat</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="alamat" maxlength="250" value="<?php if($_POST['mod']=="editData"){echo $getData['alamat'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nomor Telepon</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nomor_telepon" maxlength="15" value="<?php if($_POST['mod']=="editData"){echo $getData['nomor_telepon'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Email</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="email" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['email'];} ?>" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Tahun Ajaran</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="tahun_ajaran" maxlength="4" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_ajaran'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Semester</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="semester" required>
					<option value="1" <?php if($getData['semester'] == "1"){echo "selected";} ?>>Ganjil</option>
					<option value="2" <?php if($getData['semester'] == "2"){echo "selected";} ?>>Genap</option>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-success" id="perbaruiData" onclick="perbaruiData(<?=$getData['id'];?>)"><i class="fa fa-save" aria-hidden="true" style="margin-right: 10px;"></i>Perbarui</button>
</div>