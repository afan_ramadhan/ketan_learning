<?php
session_start();
include "../../config/database.php";

$nama_menu = "absensi";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = ($getHakAkses['s'] == 0 ? "AND absensi.ditambah_oleh = '$_SESSION[username]'" : "");
$tanggal_awal = $_POST['tanggal_awal'];
$tanggal_akhir = $_POST['tanggal_akhir'];
$rombel = $_POST['rombel'];
$kehadiran = $_POST['kehadiran'];

if($tanggal_awal == "" AND $tanggal_akhir == "")
{
	$whereTanggal = "";
}
else if($tanggal_awal != "" AND $tanggal_akhir == "")
{
	$whereTanggal = "AND absensi.tanggal = '$tanggal_awal'";
}
else if($tanggal_awal == "" AND $tanggal_akhir != "")
{
	$whereTanggal = "AND absensi.tanggal = '$tanggal_akhir'";
}
else if($tanggal_awal < $tanggal_akhir)
{
	$whereTanggal = "AND absensi.tanggal BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
}
else if($tanggal_awal > $tanggal_akhir)
{
	$whereTanggal = "AND absensi.tanggal = '$tanggal_awal'";
}
else if($tanggal_awal == $tanggal_akhir)
{
	$whereTanggal = "AND absensi.tanggal = '$tanggal_awal'";
}
else
{
	$whereTanggal = "";
}

$whereRombel = ($rombel != "" ? "AND absensi.id_rombel = '$rombel'" : "");
$whereKehadiran = ($kehadiran != "" ? "AND absensi_detail.kehadiran = '$kehadiran'" : "");
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		$('.data_custom tfoot .filter').each(function(){
			var title = $(this).text();
			$(this).html( '<input type="text"/>' );
		});
			
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"order": [[ 1, "desc" ]],
			"dom": 'lBfrtip',
			"buttons": [
				'colvis',
				{
					"extend": 'print',
					"exportOptions": {
						"columns": ':visible'
					}
				},
				{
					"extend": 'excel',
					"exportOptions": {
						"columns": ':visible'
					}
				},
				{
					"extend": 'pdf',
					"exportOptions": {
						"columns": ':visible'
					}
				}
			]
		});
			
		table.columns().every(function(){
			var that = this;
		 
			$('input', this.footer()).on('keyup change',function(){
				if (that.search() !== this.value){
					that
					.search(this.value)
					.draw();
				}
			});
		});
	});
</script>

<table class="table table-bordered table-hover data_custom">

	<thead>
		<tr>
			<th>Tanggal</th>
			<th>Jam</th>
			<th>Tahun Ajaran</th>
			<th>Semester</th>
			<th>Rombel</th>
			<th>Mapel</th>
			<th>Pertemuan Ke</th>
			<th>NIS</th>
			<th>NISN</th>
			<th>Nama Siswa</th>
			<th>Kehadiran</th>
			<th>Keterangan</th>
			<th>Sikap</th>
			<th>Catatan</th>
			
			<?php
			if($getHakAkses['s'] == 1)
			{
				echo "<th class='filter'>Guru</th>";
			}
			?>
			
		</tr>
	</thead>

	<tbody>
	
		<?php
		if($_POST['mod']=="filterAbsensi")
		{
			$data = mysql_query("
			SELECT absensi.tanggal, absensi.jam, absensi.tahun_ajaran,
			CASE
				WHEN absensi.semester = 1 THEN 'Ganjil'
				ELSE 'Genap'
			END AS semester_absensi,
			rombel.nama_rombel, mapel.nama_mapel, absensi.pertemuan_ke,
			siswa.nis, siswa.nisn, siswa.nama_lengkap,
			CASE
				WHEN absensi_detail.kehadiran = 1 THEN 'Hadir'
				WHEN absensi_detail.kehadiran = 2 THEN 'Sakit'
				WHEN absensi_detail.kehadiran = 3 THEN 'Izin'
				ELSE 'Alpa'
			END AS kehadiran_siswa,
			absensi_detail.keterangan,
			CASE
				WHEN absensi_detail.sikap = 1 THEN 'Sangat Baik'
				WHEN absensi_detail.sikap = 2 THEN 'Baik'
				WHEN absensi_detail.sikap = 3 THEN 'Cukup'
				ELSE 'Kurang'
			END AS sikap_siswa,
			absensi_detail.catatan, user.nama_lengkap AS guru
			FROM absensi_detail
			JOIN absensi ON absensi_detail.id_absensi = absensi.id
			LEFT JOIN rombel ON absensi.id_rombel = rombel.id
			LEFT JOIN mapel ON absensi.id_mapel = mapel.id
			LEFT JOIN siswa ON absensi_detail.id_siswa = siswa.id
			LEFT JOIN user ON absensi.ditambah_oleh = user.username
			WHERE 1 = 1
			$s
			$whereTanggal
			$whereRombel
			$whereKehadiran
			ORDER BY absensi.tanggal DESC, absensi.jam ASC, siswa.nama_lengkap ASC");
			while($getData = mysql_fetch_array($data))
			{
				echo "
				<tr>
					<td>$getData[tanggal]</td>
					<td>$getData[jam]</td>
					<td>$getData[tahun_ajaran]</td>
					<td>$getData[semester_absensi]</td>
					<td>$getData[nama_rombel]</td>
					<td>$getData[nama_mapel]</td>
					<td>$getData[pertemuan_ke]</td>
					<td>$getData[nis]</td>
					<td>$getData[nisn]</td>
					<td>$getData[nama_lengkap]</td>
					<td>$getData[kehadiran_siswa]</td>
					<td>$getData[keterangan]</td>
					<td>$getData[sikap_siswa]</td>
					<td>$getData[catatan]</td>
					".($getHakAkses['s'] == 1 ? "<td>$getData[guru]</td>" : "")."
				</tr>
				";
			}
		}
		?>
		
	</tbody>
	
	<tfoot>
		<tr>
			<th class="filter">Tanggal</th>
			<th class="filter">Jam</th>
			<th class="filter">Tahun Ajaran</th>
			<th class="filter">Semester</th>
			<th class="filter">Rombel</th>
			<th class="filter">Mapel</th>
			<th class="filter">Pertemuan Ke</th>
			<th class="filter">NIS</th>
			<th class="filter">NISN</th>
			<th class="filter">Nama Siswa</th>
			<th class="filter">Kehadiran</th>
			<th class="filter">Keterangan</th>
			<th class="filter">Sikap</th>
			<th class="filter">Catatan</th>
			
			<?php
			if($getHakAkses['s'] == 1)
			{
				echo "<th class='filter'>Guru</th>";
			}
			?>
			
		</tr>
	</tfoot>
	
</table>