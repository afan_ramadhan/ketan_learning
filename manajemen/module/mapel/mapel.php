<?php
$nama_menu = "mapel";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

$id = (isset($_GET['id']) ? "$_GET[id]" : "");
$id_kelas = (isset($_GET['id']) ? $_GET['id'] : "$('#id_kelas').val()");
$breadcrumb = (isset($_GET['id']) ? "<li><a href='index.php?mod=kelas'>Kelas</a></li>" : "");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Mapel";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
		})
		
		//Tambah Data
		function tambahData()
		{
			var mod = "tambahData";
			$.ajax({
				type	: "POST",
				url		: "module/mapel/mapel_form.php?id=<?=$id;?>",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Simpan Data
		function simpanData()
		{
			mulaiAnimasi();
			var mod = "simpanData";
			var id_kelas = <?=$id_kelas;?>;
			var kode_mapel = $("#kode_mapel").val();
			var nama_mapel = $("#nama_mapel").val();
			var kkm = $("#kkm").val();
			var kelompok_mapel = $("#kelompok_mapel").val();
			$.ajax({
				type	: "POST",
				url		: "module/mapel/mapel_action.php",
				data	: "mod=" + mod +
						 "&id_kelas=" + id_kelas +
						  "&kode_mapel=" + kode_mapel +
						  "&nama_mapel=" + nama_mapel +
						  "&kkm=" + kkm +
						  "&kelompok_mapel=" + kelompok_mapel,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
				}
			})
		}
		
		//Edit Data
		function editData(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/mapel/mapel_form.php?id=<?=$id;?>",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData(id)
		{
			mulaiAnimasi();
			var mod = "perbaruiData";
			var id = id;
			var id_kelas = <?=$id_kelas;?>;
			var kode_mapel = $("#kode_mapel").val();
			var nama_mapel = $("#nama_mapel").val();
			var kkm = $("#kkm").val();
			var kelompok_mapel = $("#kelompok_mapel").val();
			$.ajax({
				type	: "POST",
				url		: "module/mapel/mapel_action.php",
				data	: "mod=" + mod +
						  "&id=" + id +
						  "&id_kelas=" + id_kelas +
						  "&kode_mapel=" + kode_mapel +
						  "&nama_mapel=" + nama_mapel +
						  "&kkm=" + kkm +
						  "&kelompok_mapel=" + kelompok_mapel,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
				}
			})
		}
		
		//Hapus Data
		function hapusData(id)
		{
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusData";
				var id = id;
				$.ajax({
					type	: "POST",
					url		: "module/mapel/mapel_action.php",
					data	: "mod=" + mod +
							  "&id=" + id,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/mapel/mapel_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
					}
				})
			}
		}
		
		//Tambah Data Import
		function tambahDataImport()
		{
			var mod = "tambahDataImport";
			$.ajax({
				type	: "POST",
				url		: "module/mapel/mapel_form_import.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Simpan Data Import
		function simpanDataImport()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanDataImport");
			data.append("file", $('#file')[0].files[0]);
			$.ajax({
				type		: "POST",
				url			: "module/mapel/mapel_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/mapel/mapel_database.php?id=<?=$id;?>");
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Mapel</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<?=$breadcrumb;?>
				<li class="active">Mapel</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Mapel</h3>
						</div>
						<div class="box-body">
							<button type="button" class="btn btn-primary btn-md" onclick="tambahData()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah Mapel</button>
							<button type="button" class="btn btn-primary btn-md" onclick="tambahDataImport()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Import Mapel</button>
							<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
								<input type="checkbox" id="pilihSemua" style="margin-right: 10px;">Pilih Semua
							</button>
							<button type="button" id="hapusDataTerpilih" class="btn btn-danger" onclick="hapusDataTerpilih()" style="<?=$d;?>">
								<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus Mapel Terpilih
							</button>
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog">
									<div id="formContent" class="modal-content"></div>
									<div id="notifikasi" class="modal-content"></div>
								</div>
							</div>			
							<br/>
							<br/>  
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Mapel</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<?=$breadcrumb;?>
				<li class="active">Mapel</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>