<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "mapel";

if($_POST['mod']=="simpanData")
{
	$id_kelas = $_POST['id_kelas'];
	$kode_mapel = mysql_real_escape_string($_POST['kode_mapel']);
	$nama_mapel = mysql_real_escape_string($_POST['nama_mapel']);
	$kkm = mysql_real_escape_string($_POST['kkm']);
	$kelompok_mapel = $_POST['kelompok_mapel'];
	
	if(empty($_POST['kode_mapel']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kode Mapel Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['nama_mapel']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Mapel Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['kkm']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'KKM Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_kelas, kode_mapel, nama_mapel, kkm, kelompok_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$kode_mapel', '$nama_mapel', '$kkm', '$kelompok_mapel', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Mapel";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_mapel' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_kelas = $_POST['id_kelas'];
	$kode_mapel = mysql_real_escape_string($_POST['kode_mapel']);
	$nama_mapel = mysql_real_escape_string($_POST['nama_mapel']);
	$kkm = mysql_real_escape_string($_POST['kkm']);
	$kelompok_mapel = $_POST['kelompok_mapel'];
	
	if(empty($_POST['kode_mapel']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kode Mapel Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['nama_mapel']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Mapel Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['kkm']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'KKM Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_kelas = '$id_kelas', kode_mapel = '$kode_mapel', nama_mapel = '$nama_mapel', kkm = '$kkm', kelompok_mapel = '$kelompok_mapel', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Mapel";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_mapel' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_mapel FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_mapel'];
	
	$cekMapel_1 = mysql_num_rows(mysql_query("SELECT id_mapel FROM mapel_user WHERE id_mapel = '$id'"));
	$cekMapel_2 = mysql_num_rows(mysql_query("SELECT id_mapel FROM nilai WHERE id_mapel = '$id'"));
	if($cekMapel_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Sedang Digunakan Mapel User!');
		</script>";
	}
	else if($cekMapel_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Mapel";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekMapel_1 = mysql_num_rows(mysql_query("SELECT id_mapel FROM mapel_user WHERE id_mapel IN ($data_terpilih)"));
	$cekMapel_2 = mysql_num_rows(mysql_query("SELECT id_mapel FROM nilai WHERE id_mapel IN ($data_terpilih)"));
	if($cekMapel_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Sedang Digunakan Mapel User!');
		</script>";
	}
	else if($cekMapel_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Sedang Digunakan Nilai!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Mapel Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Mapel Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Mapel Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="simpanDataImport")
{
	function kelompokMapel($abjad)
	{
		if($abjad == "Kelompok A")
		{
			return 1;
		}
		else if($abjad == "Kelompok B")
		{
			return 2;
		}
		else if($abjad == "Kelompok C")
		{
			return 3;
		}
	}
	
	if(!empty($_FILES['file']['name']))
	{
		$target_dir = "../../uploads/import/";
		$name_file = basename($_FILES['file']['name']) ;
		$target_file = $target_dir . $name_file;
		move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

		require_once "../../libraries/PHPExcel.php";
		require_once "../../libraries/PHPExcel/IOFactory.php";

		$file = $target_dir . $name_file;

		$objPHPExcel = PHPExcel_IOFactory::load($file);
		
		$success = 0;
		$error = 0;
		$keterangan_error = "";
		
		foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
		{
			$totalrow = $worksheet->getHighestRow();
			
			for($row = 2; $row <= $totalrow; $row++)
			{
				$id_kelas = mysql_escape_string($worksheet->getCellByColumnAndRow(0, $row)->getValue());
				$kode_mapel = mysql_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
				$nama_mapel = mysql_escape_string($worksheet->getCellByColumnAndRow(2, $row)->getValue());
				$kkm = mysql_escape_string($worksheet->getCellByColumnAndRow(3, $row)->getValue());
				$kelompok_mapel = mysql_escape_string($worksheet->getCellByColumnAndRow(4, $row)->getValue());
				
				if($id_kelas == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, ID Kelas Tidak Boleh Kosong.";
				}
				else if($kode_mapel == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Kode Mapel Tidak Boleh Kosong.";
				}
				else if($nama_mapel == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Nama Mapel Tidak Boleh Kosong.";
				}
				else if($kkm == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, KKM Tidak Boleh Kosong.";
				}
				else
				{
					$simpanDataImport = mysql_query("INSERT INTO $nama_tabel (id_kelas, kode_mapel, nama_mapel, kkm, kelompok_mapel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$kode_mapel', '$nama_mapel', '$kkm', '".kelompokMapel($kelompok_mapel)."', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
						
					if($simpanDataImport)
					{
						$aktivitas = "Tambah Mapel";
						$keterangan = mysql_real_escape_string("Menambahkan '$nama_mapel' Pada Tabel '$nama_tabel'");
						$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
							
						$success++;
					}
					else
					{
						$error++;
						$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
					}
				}	
			}
        }
		
		unlink($target_file);
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Mapel Ditambahkan, $error Error! $keterangan_error');
			$('#form').modal('hide');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}
?>