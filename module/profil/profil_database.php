<?php
session_start();
include "../../manajemen/config/database.php";
?>

<table class="table table-stripped table-hover data">

	<tbody>

		<?php
		$data = mysql_query("SELECT siswa.*, rombel.nama_rombel FROM siswa LEFT JOIN rombel ON siswa.id_rombel = rombel.id WHERE siswa.id = '$_SESSION[id]'");
		$getData = mysql_fetch_array($data);
		
		if($getData['gambar']=="")
		{
			echo "
			<tr>
				<th colspan='3'>
					<center><img class='img-thumbnail' src='manajemen/images/user_kosong.jpg' style='width: 250px; margin: 20px;'/></center>
				</th>
			</tr>";
		}
		else
		{
			echo "
			<tr>
				<th colspan='3'>
					<center><img class='img-thumbnail' src='manajemen/images/siswa/$getData[gambar]' style='width: 250px; margin: 20px;'/></center>
				</th>
			</tr>";
		}
		
		$jenis_kelamin = ($getData['jenis_kelamin'] == "L" ? "Laki - Laki" : "Perempuan");
		$tema = ucwords(str_replace("-", " ", $getData['tema']));
		
		echo "
		<tr>
			<th style='width: 1px;'>NIS</th>
			<th style='width: 1px;'>:</th>
			<td>$getData[nis]</td>
		</tr>
		<tr>
			<th style='width: 1px;'>NISN</th>
			<th style='width: 1px;'>:</th>
			<td>$getData[nisn]</td>
		</tr>
		<tr>
			<th style='width: 1px;'>Nama Lengkap</th>
			<th style='width: 1px;'>:</th>
			<td>$getData[nama_lengkap]</td>
		</tr>
		<tr>
			<th>Username</th>
			<th>:</th>
			<td>$getData[username]</td>
		</tr>
		<tr>
			<th>Jenis Kelamin</th>
			<th>:</th>
			<td>$jenis_kelamin</td>
		</tr>
		<tr>
			<th>Tempat Lahir</th>
			<th>:</th>
			<td>$getData[tempat_lahir]</td>
		</tr>
		<tr>
			<th>Tanggal Lahir</th>
			<th>:</th>
			<td>$getData[tanggal_lahir]</td>
		</tr>
		<tr>
			<th>Agama</th>
			<th>:</th>
			<td>$getData[agama]</td>
		</tr>
		<tr>
			<th>Alamat</th>
			<th>:</th>
			<td>$getData[alamat]</td>
		</tr>
		<tr>
			<th>Email</th>
			<th>:</th>
			<td>$getData[email]</td>
		</tr>
		<tr>
			<th>Nomor Telepon</th>
			<th>:</th>
			<td>$getData[nomor_telepon]</td>
		</tr>
		<tr>
			<th>Nama Ayah</th>
			<th>:</th>
			<td>$getData[nama_ayah]</td>
		</tr>
		<tr>
			<th>Nama Ibu</th>
			<th>:</th>
			<td>$getData[nama_ibu]</td>
		</tr>
		<tr>
			<th>Rombel</th>
			<th>:</th>
			<td>$getData[nama_rombel]</td>
		</tr>
		<tr>
			<th>Tema</th>
			<th>:</th>
			<td>$tema</td>
		</tr>";
		?>
		
	</tbody>
	
</table>