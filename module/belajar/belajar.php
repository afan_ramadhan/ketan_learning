<div class="content-wrapper">
	<section class="content-header">
		<h1><?=($jenis_materi == "ebook" ? $data_materi['nama_ebook'] : $data_materi['nama_video']);?></h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i><?=($jenis_materi == "ebook" ? $data_materi['nama_ebook'] : $data_materi['nama_video']);?></li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Belajar</h3>
					</div>
					<div class="box-body">
						<span class="help-block">Apabila Ebook Atau Video Tidak Muncul, Silahkan Refresh Halaman.</span>
						<center>
							<?php
							if($jenis_materi == "ebook")
							{
								$jenisPdf = strtolower(substr($data_materi['file'], -3));
								$reader = ($jenisPdf == "pdf" ? "manajemen/ebook/ViewerJS/#../$data_materi[file]" : "https://view.officeapps.live.com/op/embed.aspx?src=http://$lihat_konfigurasi[url]/ebook/$data_materi[file]");
							?>
								
								<iframe src="<?=$reader;?>" style="width: 100%; height: 500px; border: 1px solid #F4F4F4;"></iframe>
								<br/>
								<a class="btn btn-info" href="manajemen/ebook/<?=$data_materi['file'];?>" onclick="simpanStatusDownload()" download><i class="fa fa-download" style="margin-right: 10px;"></i>Unduh Materi</a>
							<?php
							}
							else
							{
								$link = str_replace("https://youtu.be/", "https://www.youtube.com/embed/", $data_materi['url']);
								$link = str_replace("https://www.youtube.com/watch?v=", "https://www.youtube.com/embed/", $link);
								$linkDownload = str_replace("https://youtu.be/", "https://www.ssyoutube.com/watch?v=", $data_materi['url']);
							?>
								<div style="width: 100%; background-color: #D1D1D1; border: 1px solid #F4F4F4; padding: 10px; margin-bottom: 5px;">
									<iframe src="<?=$link;?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="width: 560px; max-width: 100%; height: 315px;"></iframe>
								</div>
								<a class="btn btn-info" href="<?=$linkDownload;?>" onclick="simpanStatusDownload()" target="_blank"><i class="fa fa-download" style="margin-right: 10px;"></i>Unduh Materi</a>
							<?php	
							}
							?>
						</center>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>