<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$ambil_konfigurasi = mysql_query("SELECT * FROM konfigurasi WHERE id = '1'");
$lihat_konfigurasi = mysql_fetch_array($ambil_konfigurasi);

$nama_menu = "absensi";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$s = ($getHakAkses['s'] == 0 ? "WHERE ditambah_oleh = '$_SESSION[username]'" : "");

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	$mod = "perbaruiData";
	
	$data = mysql_query("SELECT * FROM absensi WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
	
	$id_rombel = $getData['id_rombel'];
}
else
{
	$id = 0;
	$mod = "simpanData";
	$id_rombel = 0;
}
?>

<script>
	$(function(){
		$('#tanggal_').datetimepicker({
			format: 'YYYY-MM-DD',
		});
		$('#jam_').datetimepicker({
			format: 'HH:mm'
		});
	})
	
	$(document).ready(function(){
		var mod = "ambilMapel";
		var id_absensi = <?=$id;?>;
		var id_rombel = $( "#id_rombel option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/absensi/absensi_response.php",
			data	: "mod=" + mod +
					  "&id_absensi=" + id_absensi +
					  "&id_rombel=" + id_rombel,
			success: function(html)
			{
				$("#id_mapel").html(html);
				ambilSiswa();
			}
		})
	})
	
	function ambilMapel()
	{
		var mod = "ambilMapel";
		var id_absensi = <?=$id;?>;
		var id_rombel = $( "#id_rombel option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/absensi/absensi_response.php",
			data	: "mod=" + mod +
					  "&id_absensi=" + id_absensi +
					  "&id_rombel=" + id_rombel,
			success: function(html)
			{
				$("#id_mapel").html(html);
				ambilSiswa();
			}
		})
	}
	
	function ambilSiswa()
	{
		var mod = "ambilSiswa";
		var id_absensi = <?=$id;?>;
		var id_rombel = $( "#id_rombel option:selected" ).val();
		$.ajax({
			type	: "POST",
			url		: "module/absensi/absensi_response.php",
			data	: "mod=" + mod +
					  "&id_absensi=" + id_absensi +
					  "&id_rombel=" + id_rombel,
			success: function(html)
			{
				$("#dataSiswa").html(html);
			}
		})
	}
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit absensi";}else{echo "Tambah absensi";} ?>
	</h4>
</div>
<div class="modal-body">
	<form id="formAbsensi">
		<input type="hidden" name="mod" value="<?=$mod;?>">
		<input type="hidden" name="id" value="<?=$id;?>">
		<table class="table table-hover">
			<tr>
				<td style="border: none;">
					<label class="control-label">Rombel</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="id_rombel" name="id_rombel" onchange="ambilMapel()" required>
						<?php
						if($getHakAkses['s'] == 1)
						{
							$rombel = mysql_query("SELECT * FROM rombel ORDER BY nama_rombel");
						}
						else
						{
							$rombel = mysql_query("SELECT DISTINCT rombel.id, rombel.nama_rombel FROM rombel JOIN jadwal_rombel ON rombel.id = jadwal_rombel.id_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id WHERE mapel_user.id_user = '$_SESSION[id]' ORDER BY nama_rombel");
						}
						
						while($getRombel = mysql_fetch_array($rombel))
						{
							$selected = ($getData['id_rombel'] == $getRombel['id'] ? "selected" : "");
						?>
							<option value="<?=$getRombel['id'];?>" <?=$selected;?>><?=$getRombel['nama_rombel'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Mapel</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<select class="form-control" id="id_mapel" name="id_mapel" required>
						<?php
						if($getHakAkses['s'] == 1)
						{
							$mapel = mysql_query("SELECT * FROM mapel ORDER BY nama_mapel");
						}
						else
						{
							$mapel = mysql_query("SELECT mapel.* FROM mapel JOIN mapel_user ON mapel.id = mapel_user.id_mapel AND mapel_user.id_user = '$_SESSION[id]' ORDER BY nama_mapel");
						}
						
						while($getMapel = mysql_fetch_array($mapel))
						{
							$selected = ($getData['id_mapel'] == $getMapel['id'] ? "selected" : "");
						?>
							<option value="<?=$getMapel['id'];?>" <?=$selected;?>><?=$getMapel['nama_mapel'];?></option>
						<?php
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Tahun Ajaran</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="text" class="form-control" value="<?php if($_POST['mod']=="editData"){echo $getData['tahun_ajaran'];}else{echo $lihat_konfigurasi['tahun_ajaran'];} ?>" readonly/>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Semester</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="text" class="form-control" value="<?php if($_POST['mod']=="editData"){echo ($getData['semester'] == 1 ? "Ganjil" : "Genap");}else{echo ($lihat_konfigurasi['semester'] == 1 ? "Ganjil" : "Genap");} ?>" readonly/>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Tanggal</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<div class="input-group date" id="tanggal_">
						<input type="text" class="form-control" id="tanggal" name="tanggal" value="<?php if($_POST['mod']=="editData"){echo $getData['tanggal'];}else{echo $tanggal_sekarang;} ?>" required/>
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Jam</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<div class="input-group date" id="jam_">
						<input type="text" class="form-control" id="jam" name="jam" value="<?php if($_POST['mod']=="editData"){echo $getData['jam'];}else{echo $jam_sekarang;} ?>" required/>
						<span class="input-group-addon">
							<span class="fa fa-clock-o"></span>
						</span>
					</div>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Pertemuan Ke</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="text" class="form-control" id="pertemuan_ke" name="pertemuan_ke" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['pertemuan_ke'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
				</td>
			</tr>
			<tr>
				<td style="border: none;">
					<label class="control-label">Uraian Materi</label>
				</td>
				<td style="border: none;"><label class="control-label">:</label></td>
				<td style="border: none;">
					<input type="text" class="form-control" id="uraian_materi" name="uraian_materi" maxlength="200" value="<?php if($_POST['mod']=="editData"){echo $getData['uraian_materi'];} ?>" required/>
				</td>
			</tr>
		</table>
		<h3 style="margin-top: 0; margin-bottom: 10px;">Daftar Siswa</h3>
		<div class="scrolling">
			<table class="table table-bordered table-hover">
				<thead>
					<tr>
						<th style="width: 1px;">No.</th>
						<th>NIS</th>
						<th>NISN</th>
						<th>Nama Lengkap</th>
						<th style="min-width: 100px;">Kehadiran</th>
						<th style="min-width: 200px;">Keterangan</th>
						<th style="min-width: 150px;">Sikap</th>
						<th style="min-width: 200px;">Catatan</th>
					</tr>
				</thead>
				<tbody id="dataSiswa">
					<?php
					$x = 1;
					$siswa = mysql_query("SELECT siswa.*, absensi_detail.kehadiran, absensi_detail.keterangan, absensi_detail.sikap, absensi_detail.catatan FROM siswa LEFT JOIN absensi_detail ON siswa.id = absensi_detail.id_siswa AND absensi_detail.id_absensi = '$id' WHERE siswa.id_rombel = '$id_rombel'");
					while($getSiswa = mysql_fetch_array($siswa))
					{
						echo "
						<tr>
							<td align='center'>$x</td>
							<td>$getSiswa[nis]</td>
							<td>$getSiswa[nisn]</td>
							<td>$getSiswa[nama_lengkap]</td>
							<td>
								<input type='hidden' name='id_siswa[]' value='$getSiswa[id]'>
								<select class='form-control' id='kehadiran' name='kehadiran[]' required>
									<option value='1' ".($getSiswa['kehadiran'] == 1 ? "selected" : "").">Hadir</option>
									<option value='2' ".($getSiswa['kehadiran'] == 2 ? "selected" : "").">Sakit</option>
									<option value='3' ".($getSiswa['kehadiran'] == 3 ? "selected" : "").">Izin</option>
									<option value='4' ".($getSiswa['kehadiran'] == 4 ? "selected" : "").">Alpa</option>
								</select>
							</td>
							<td>
								<input type='text' class='form-control' id='keterangan' name='keterangan[]' maxlength='200' value='$getSiswa[keterangan]' required/>
							</td>
							<td>
								<select class='form-control' id='sikap' name='sikap[]' required>
									<option value='1' ".($getSiswa['sikap'] == 1 ? "selected" : "").">Sangat Baik</option>
									<option value='2' ".($getSiswa['sikap'] == 2 ? "selected" : "").">Baik</option>
									<option value='3' ".($getSiswa['sikap'] == 3 ? "selected" : "").">Cukup</option>
									<option value='4' ".($getSiswa['sikap'] == 4 ? "selected" : "").">Kurang</option>
								</select>
							</td>
							<td>
								<input type='text' class='form-control' id='catatan' name='catatan[]' maxlength='200' value='$getSiswa[catatan]' required/>
							</td>
						</tr>";
						$x++;
					}
					?>				
				</tbody>		
			</table>
		</div>
	</form>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>