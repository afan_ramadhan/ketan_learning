<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "kelas";

if($_POST['mod']=="simpanData")
{
	$nama_kelas = mysql_real_escape_string($_POST['nama_kelas']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_kelas']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kelas Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (nama_kelas, keterangan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_kelas', '$keterangan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Kelas";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_kelas' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_kelas = mysql_real_escape_string($_POST['nama_kelas']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	
	if(empty($_POST['nama_kelas']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Kelas Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_kelas = '$nama_kelas', keterangan = '$keterangan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Kelas";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_kelas' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_kelas FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_kelas'];
	
	$cekKelas_1 = mysql_num_rows(mysql_query("SELECT id_kelas FROM mapel WHERE id_kelas = '$id'"));
	$cekKelas_2 = mysql_num_rows(mysql_query("SELECT id_kelas FROM rombel WHERE id_kelas = '$id'"));
	if($cekKelas_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Sedang Digunakan Mapel!');
		</script>";
	}
	else if($cekKelas_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Sedang Digunakan Rombel!');
		</script>";
	}
	else
	{
		$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
		if($hapusData)
		{
			$aktivitas = "Hapus Kelas";
			$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$cekKelas_1 = mysql_num_rows(mysql_query("SELECT id_kelas FROM mapel WHERE id_kelas IN ($data_terpilih)"));
	$cekKelas_2 = mysql_num_rows(mysql_query("SELECT id_kelas FROM rombel WHERE id_kelas IN ($data_terpilih)"));
	if($cekKelas_1 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Sedang Digunakan Mapel!');
		</script>";
	}
	else if($cekKelas_2 > 0)
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Sedang Digunakan Rombel!');
		</script>";
	}
	else
	{
		$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
		if($hapusDataTerpilih)
		{
			$aktivitas = "Hapus Kelas Terpilih";
			$keterangan = mysql_real_escape_string("Menghapus Kelas Terpilih Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Kelas Dihapus!');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
			</script>
			";
		}
	}
}
?>