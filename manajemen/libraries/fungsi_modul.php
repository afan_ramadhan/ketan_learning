<?php
if(isset($_GET['mod']))
{
	switch($_GET['mod'])
	{
		case '404':
			include("module/404/404.php");
			break;
		case 'level':
			include("module/level/level.php");
			break;
		case 'user':
			include("module/user/user.php");
			break;
		case 'profil':
			include("module/profil/profil.php");
			break;
		case 'konfigurasi':
			include("module/konfigurasi/konfigurasi.php");
			break;
		case 'riwayat':
			include("module/riwayat/riwayat.php");
			break;
		case 'kelas':
			include("module/kelas/kelas.php");
			break;
		case 'mapel':
			include("module/mapel/mapel.php");
			break;
		case 'rombel':
			include("module/rombel/rombel.php");
			break;
		case 'jadwal_rombel':
			include("module/jadwal_rombel/jadwal_rombel.php");
			break;
		case 'siswa':
			include("module/siswa/siswa.php");
			break;
		case 'absensi':
			include("module/absensi/absensi.php");
			break;
		case 'ebook':
			include("module/ebook/ebook.php");
			break;
		case 'video':
			include("module/video/video.php");
			break;
		case 'paket_soal':
			include("module/paket_soal/paket_soal.php");
			break;
		case 'soal':
			include("module/soal/soal.php");
			break;
		case 'ujian':
			include("module/ujian/ujian.php");
			break;
		case 'suspect':
			include("module/suspect/suspect.php");
			break;
		case 'laporan':
			include("module/laporan/laporan.php");
			break;
		case 'kegiatan':
			include("module/kegiatan/kegiatan.php");
			break;
		case 'pengumuman':
			include("module/pengumuman/pengumuman.php");
			break;
		case 'progres_belajar':
			include("module/progres_belajar/progres_belajar.php");
			break;
		case 'saran':
			include("module/saran/saran.php");
			break;
		case 'mapel_user':
			include("module/mapel_user/mapel_user.php");
			break;
		default:
			include ("module/dashboard/dashboard.php");
			break;
	}   
}
else
{
	include ("module/dashboard/dashboard.php");
}
?>	