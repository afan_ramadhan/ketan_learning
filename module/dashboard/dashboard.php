<?php
include "manajemen/libraries/fungsi_waktu.php";

$namaHari = array("1" => "Senin", "2" => "Selasa", "3" => "Rabu", "4" => "Kamis", "5" => "Jum'at", "6" => "Sabtu", "0" => "Minggu");
$hariIni = date("w");

$totalTugasHarian = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '1' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalTugasHarianDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '1' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));
$totalUlanganHarian = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '2' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalUlanganHarianDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '2' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));
$totalUjianTengahSemester = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '3' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalUjianTengahSemesterDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '3' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));
$totalUjianAkhirSemester = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '4' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalUjianAkhirSemesterDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '4' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));
$totalTryOutUSBN = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '5' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalTryOutUSBNDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '5' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));
$totalUjianSekolahBerstandarNasional = mysql_num_rows(mysql_query("SELECT * FROM ujian WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '6' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalUjianSekolahBerstandarNasionalDikerjakan = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_ujian = '6' AND tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]' AND nilai.id IS NOT NULL"));

$totalHadir = mysql_num_rows(mysql_query("SELECT absensi_detail.* FROM absensi_detail LEFT JOIN absensi ON absensi_detail.id_absensi = absensi.id WHERE absensi_detail.id_siswa = '$_SESSION[id]' AND absensi_detail.kehadiran = '1' AND absensi.tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalSakit = mysql_num_rows(mysql_query("SELECT absensi_detail.* FROM absensi_detail LEFT JOIN absensi ON absensi_detail.id_absensi = absensi.id WHERE absensi_detail.id_siswa = '$_SESSION[id]' AND absensi_detail.kehadiran = '2' AND absensi.tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalIzin = mysql_num_rows(mysql_query("SELECT absensi_detail.* FROM absensi_detail LEFT JOIN absensi ON absensi_detail.id_absensi = absensi.id WHERE absensi_detail.id_siswa = '$_SESSION[id]' AND absensi_detail.kehadiran = '3' AND absensi.tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));
$totalAlpa = mysql_num_rows(mysql_query("SELECT absensi_detail.* FROM absensi_detail LEFT JOIN absensi ON absensi_detail.id_absensi = absensi.id WHERE absensi_detail.id_siswa = '$_SESSION[id]' AND absensi_detail.kehadiran = '4' AND absensi.tahun_ajaran = '$lihat_konfigurasi[tahun_ajaran]'"));

$profilWaliKelas = mysql_fetch_array(mysql_query("SELECT user.* FROM rombel LEFT JOIN user ON rombel.id_user = user.id WHERE rombel.id = '$ambil_data_siswa[id_rombel]'"));

$jumlahEbook = mysql_num_rows(mysql_query("SELECT * FROM ebook WHERE id_kelas = '$ambil_data_siswa[id_kelas]' AND publikasi = 'Y'"));
$jumlahVideo = mysql_num_rows(mysql_query("SELECT * FROM video WHERE id_kelas = '$ambil_data_siswa[id_kelas]' AND publikasi = 'Y'"));
$jumlahUjian = mysql_num_rows(mysql_query("SELECT * FROM ujian LEFT JOIN nilai ON ujian.id = nilai.id_ujian AND nilai.id_siswa = '$_SESSION[id]' WHERE ujian.id_rombel = '$ambil_data_siswa[id_rombel]' AND ujian.tanggal_ujian >= '$tanggal_sekarang' AND nilai.id IS NULL"));
?>

<script language="JavaScript">
	//Ganti Title
	function GantiTitle()
	{
		document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Siswa | Dashboard";
	}
	GantiTitle();
	
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			//"order": [[ 1, "asc" ]],
			// "dom": 'lBfrtip',
        	// "buttons": [
				// 'colvis',
				// {
                	// "extend": 'print',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// },
				// {
                	// "extend": 'excel',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// },
				// {
                	// "extend": 'pdf',
                	// "exportOptions": {
                    	// "columns": ':visible'
                	// }
            	// }
       		// ]
		});
	});
		
	//Lihat Detail
	function lihatDetail(id)
	{
		var mod = "lihatDetail";
		var id = id;
		$.ajax({
			type	: "POST",
			url		: "module/dashboard/dashboard_form_detail.php",
			data	: "mod=" + mod +
					  "&id=" + id,
			success: function(html)
			{
				$("#formContent").html(html);
				$("#form").modal();
			}
		})
	}
</script>

<div class="content-wrapper">
	<section class="content-header">
		<h1>Dashboard</h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</li>
		</ol>
	</section>
	<section class="content container-fluid">
		<div class="row">
			<?php
			$dataPengumuman = mysql_query("SELECT pengumuman.*, user.nama_lengkap, user.jenis_kelamin FROM pengumuman JOIN user ON pengumuman.ditambah_oleh = user.username WHERE (pengumuman.penerima = 1 OR (pengumuman.penerima = 3 AND (pengumuman.id_rombel = '$ambil_data_siswa[id_rombel]' OR id_rombel = 0))) AND jenis_pengumuman = '0' AND (CURRENT_DATE() BETWEEN pengumuman.tanggal_mulai AND pengumuman.tanggal_selesai) ORDER BY pengumuman.id DESC");
			while($ambilDataPengumuman = mysql_fetch_array($dataPengumuman))
			{
				$tanggal_tugas = ($ambilDataPengumuman['tanggal_mulai'] == $ambilDataPengumuman['tanggal_selesai'] ? "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])."</small>" : "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])." s/d ".tanggal($ambilDataPengumuman['tanggal_selesai'])."</small>");
				$jenis_kelamin = ($ambilDataPengumuman['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
				
				$keterangan = str_replace("/uploads/", "manajemen/uploads/", $ambilDataPengumuman['keterangan']);
				// $keterangan = $ambilDataPengumuman['keterangan'];
				
				echo "
				<div class='col-md-12'>
					<div class='alert alert-warning alert-dismissible'>
						<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
						<h4><i class='icon fa fa-info'></i>$ambilDataPengumuman[nama_pengumuman] ($jenis_kelamin $ambilDataPengumuman[nama_lengkap])</h4>
						$tanggal_tugas
						".str_replace("<img ", "<img style='max-width: 100% !important;' ", $keterangan)."
					</div>
				</div>";
			}
			?>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Tugas Harian</h3>
					</div>
					<div class="box-body">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#daftar_tugas">Daftar Tugas</a></li>
							<li><a data-toggle="tab" href="#riwayat_tugas">Riwayat Tugas</a></li>
						</ul>
						<div class="tab-content">
							<div id="daftar_tugas" class="tab-pane fade in active" style="padding-top: 20px;">
							
								<?php
								$dataPengumuman = mysql_query("SELECT pengumuman.*, user.nama_lengkap, user.jenis_kelamin FROM pengumuman JOIN user ON pengumuman.ditambah_oleh = user.username WHERE (pengumuman.penerima = 3 AND pengumuman.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_pengumuman = '1') AND (CURRENT_DATE() BETWEEN pengumuman.tanggal_mulai AND pengumuman.tanggal_selesai) ORDER BY pengumuman.id DESC");
								while($ambilDataPengumuman = mysql_fetch_array($dataPengumuman))
								{
									$tanggal_tugas = ($ambilDataPengumuman['tanggal_mulai'] == $ambilDataPengumuman['tanggal_selesai'] ? "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])."</small>" : "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])." s/d ".tanggal($ambilDataPengumuman['tanggal_selesai'])."</small>");
									$jenis_materi = ($ambilDataPengumuman['jenis_materi'] == 1 ? "ebook" : "video");
									$form = "
										<br/>
										<form action='belajar.php' role='form' method='POST' style='margin-top: 15px;'>
											<input type='hidden' name='jenis_materi' value='$jenis_materi'/>
											<input type='hidden' name='id_materi' value='$ambilDataPengumuman[id_materi]'/>
											<button type='submit' class='btn btn-default btn-sm'>Lihat ".ucwords($jenis_materi)."</button>
										</form>";
									$tampilkanForm = ($ambilDataPengumuman['id_materi'] != 0 ? "$form" : "");
										
									$jenis_kelamin = ($ambilDataPengumuman['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
									echo "
									<div class='alert alert-info alert-dismissible'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
										<h4><i class='icon fa fa-thumb-tack'></i>$ambilDataPengumuman[nama_pengumuman] ($jenis_kelamin $ambilDataPengumuman[nama_lengkap])</h4>
										$tanggal_tugas
										".str_replace("/uploads/", "manajemen/uploads/", $ambilDataPengumuman['keterangan'])."
										$tampilkanForm
									</div>";
								}
								?>
								
							</div>
							<div id="riwayat_tugas" class="tab-pane fade" style="padding-top: 20px;">
							
								<?php
								$dataPengumuman = mysql_query("SELECT pengumuman.*, user.nama_lengkap, user.jenis_kelamin FROM pengumuman JOIN user ON pengumuman.ditambah_oleh = user.username WHERE (pengumuman.penerima = 3 AND pengumuman.id_rombel = '$ambil_data_siswa[id_rombel]' AND jenis_pengumuman = '1') AND pengumuman.tanggal_selesai < CURRENT_DATE() ORDER BY pengumuman.id DESC LIMIT 25");
								while($ambilDataPengumuman = mysql_fetch_array($dataPengumuman))
								{
									$tanggal_tugas = ($ambilDataPengumuman['tanggal_mulai'] == $ambilDataPengumuman['tanggal_selesai'] ? "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])."</small>" : "<small><i class='fa fa-calendar-check-o' style='margin-right: 10px;'></i>".tanggal($ambilDataPengumuman['tanggal_mulai'])." s/d ".tanggal($ambilDataPengumuman['tanggal_selesai'])."</small>");
									$jenis_materi = ($ambilDataPengumuman['jenis_materi'] == 1 ? "ebook" : "video");
									$form = "
										<br/>
										<form action='belajar.php' role='form' method='POST' style='margin-top: 15px;'>
											<input type='hidden' name='jenis_materi' value='$jenis_materi'/>
											<input type='hidden' name='id_materi' value='$ambilDataPengumuman[id_materi]'/>
											<button type='submit' class='btn btn-default btn-sm'>Lihat ".ucwords($jenis_materi)."</button>
										</form>";
									$tampilkanForm = ($ambilDataPengumuman['id_materi'] != 0 ? "$form" : "");
										
									$jenis_kelamin = ($ambilDataPengumuman['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
									echo "
									<div class='alert alert-success alert-dismissible'>
										<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>×</button>
										<h4><i class='icon fa fa-history'></i>$ambilDataPengumuman[nama_pengumuman] ($jenis_kelamin $ambilDataPengumuman[nama_lengkap])</h4>
										$tanggal_tugas
										".str_replace("/uploads/", "manajemen/uploads/", $ambilDataPengumuman['keterangan'])."
										$tampilkanForm
									</div>";
								}
								?>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-default">
					<div class="box-header with-border">
						<h3 class="box-title">Jadwal Rombel <?=$namaHari[$hariIni];?></h3>
					</div>
					<div class="box-body">
						<table class="table table-bordered table-hover data_custom">
							<thead>
								<tr>
									<th style="width: 1px;">No.</th>
									<th>Mapel</th>
									<th>Guru</th>
								</tr>
							</thead>

							<tbody>
					
								<?php
								$x = 1;
								$jadwalKelas = mysql_query("SELECT jadwal_rombel.*, mapel.nama_mapel, user.id AS id_user, user.nama_lengkap, user.jenis_kelamin FROM jadwal_rombel JOIN mapel_user ON jadwal_rombel.id_mapel_user = mapel_user.id JOIN mapel ON mapel_user.id_mapel = mapel.id JOIN user ON mapel_user.id_user = user.id WHERE jadwal_rombel.id_rombel = '$ambil_data_siswa[id_rombel]' AND jadwal_rombel.hari = '$hariIni' ORDER BY jadwal_rombel.id");
								while($getJadwalKelas = mysql_fetch_array($jadwalKelas))
								{
									$jenis_kelamin = ($getJadwalKelas['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");
									echo "
									<tr>
										<td align='center'>$x</td>
										<td>$getJadwalKelas[nama_mapel]</td>
										<td><button type='button' class='btn btn-info btn-sm' onclick='lihatDetail($getJadwalKelas[id_user])' style='margin-bottom: 10px;'><i class='fa fa-search' aria-hidden='true' style='margin-right: 10px;'></i>$jenis_kelamin $getJadwalKelas[nama_lengkap]</button></td>
									</tr>";
									$x++;
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Jadwal Kegiatan</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-lg-12 col-xs-12">
								<div id="jadwal_kegiatan" class="monthly"></div>
								<script>
									var jadwalKegiatan = {
										"monthly": [
																
											<?php
											$dataKegiatan = mysql_query("SELECT * FROM kegiatan");
											while($ambilDataKegiatan = mysql_fetch_array($dataKegiatan))
											{
												echo "
												{
													'name': '$ambilDataKegiatan[nama_kegiatan]',
													'startdate': '$ambilDataKegiatan[tanggal_mulai]',
													'enddate': '$ambilDataKegiatan[tanggal_selesai]',
													'starttime': '$ambilDataKegiatan[jam_mulai]',
													'endtime': '$ambilDataKegiatan[jam_selesai]',
													'color': '$ambilDataKegiatan[warna]',
													'url': ''
												},";
											}
											?>
										
										]
									};

									$(window).load(function(){
										$('#jadwal_kegiatan').monthly({
											mode: 'event',
											dataType: 'json',
											events: jadwalKegiatan
										});
									});
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Statistik Ujian Tahun Ajaran <?=$lihat_konfigurasi['tahun_ajaran'];?></h3>
					</div>
					<div class="box-body">
						<div id="statistik_tugas"></div>
						<script>
							Highcharts.chart('statistik_tugas', {
								chart: {
									type: 'bar',
									borderWidth: 1,
									borderColor: '#A9A9A9'
								},
								title: {
									text: null
								},
								subtitle: {
									text: null
								},
								xAxis: {
									categories: [
										'Tugas Harian',
										'Ulangan Harian',
										'Ujian Tengah Semester (UTS)',
										'Ujian Akhir Semester (UAS)',
										'Try Out USBN',
										'Ujian Sekolah Berstandar Nasional (USBN)'
									]
								},
								yAxis: {
									min: 0,
									allowDecimals: false,
									title: {
										text: 'Jumlah Ujian'
									}
								},
								tooltip: {
									valueSuffix: ' Ujian'
								},
								legend: {
									reversed: true
								},
								plotOptions: {
									series: {
										stacking: 'normal'
									}
								},
								credits: {
									enabled: false
								},
								series: [
									{
										name: 'Belum Dikerjakan',
										color: '#A8C5DC',
										data: [
											<?=$totalTugasHarian - $totalTugasHarianDikerjakan;?>,
											<?=$totalUlanganHarian - $totalUlanganHarianDikerjakan;?>,
											<?=$totalUjianTengahSemester - $totalUjianTengahSemesterDikerjakan;?>,
											<?=$totalUjianAkhirSemester - $totalUjianAkhirSemesterDikerjakan;?>,
											<?=$totalTryOutUSBN - $totalTryOutUSBNDikerjakan;?>,
											<?=$totalUjianSekolahBerstandarNasional - $totalUjianSekolahBerstandarNasionalDikerjakan;?>
										]
									},
									{
										name: 'Dikerjakan',
										color: '#4982AF',
										data: [
											<?=$totalTugasHarianDikerjakan;?>,
											<?=$totalUlanganHarianDikerjakan;?>,
											<?=$totalUjianTengahSemesterDikerjakan;?>,
											<?=$totalUjianAkhirSemesterDikerjakan;?>,
											<?=$totalTryOutUSBNDikerjakan;?>,
											<?=$totalUjianSekolahBerstandarNasionalDikerjakan;?>
										]
									}
								]
							});
						</script>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Grafik Absensi Tahun Ajaran <?=$lihat_konfigurasi['tahun_ajaran'];?></h3>
					</div>
					<div class="box-body">
						<div id="grafik_absensi"></div>
						<script>
							Highcharts.chart('grafik_absensi', {
								chart: {
									type: 'pie',
									borderWidth: 1,
									borderColor: '#A9A9A9'
								},
								title: {
									text: null
								},
								subtitle: {
									text: null
								},
								tooltip: {
									valueSuffix: ' Hari'
								},
								plotOptions: {
									pie: {
										allowPointSelect: true,
										cursor: 'pointer',
										dataLabels: {
											enabled: false
										},
										showInLegend: true
									}
								},
								legend: {
									reversed: true
								},
								credits: {
									enabled: false
								},
								series: [
									{
										name: 'Total',
										colorByPoint: true,
										data: [
											{
												name: 'Alpa',
												color: "#F7827B",
												y: <?=$totalAlpa;?>
											},
											{
												name: 'Izin',
												color: "#7CF7C0",
												y: <?=$totalIzin;?>
											},
											{
												name: 'Sakit',
												color: "#F7C07C",
												y: <?=$totalSakit;?>
											},
											{
												name: 'Hadir',
												color: "#B3F77C",
												y: <?=$totalHadir;?>,
												sliced: true,
												selected: true
											}
										]
									}
								]
							});
						</script>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="info-box bg-yellow" style="background-color: #F39C12 !important;">
					<span class="info-box-icon">
						<i class="fa fa-user-circle"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Profil Wali Kelas</span>
						<span class="info-box-number" style="margin-bottom: 18px; font-size: 12px;"><?=($profilWaliKelas['jenis_kelamin'] == "L" ? "Bapak" : "Ibu");?> <?=$profilWaliKelas['nama_lengkap'];?></span>
						<a class="pull-right" onclick="lihatDetail(<?=$profilWaliKelas['id'];?>)">Lihat Profil<i class="fa fa-id-card-o" style="margin-left: 10px;"></i></a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="info-box bg-blue" style="background-color: #00C0EF !important;">
					<span class="info-box-icon">
						<i class="fa fa-book"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Ebook</span>
						<span class="info-box-number" style="margin-bottom: 10px;"><?=$jumlahEbook;?> Tersedia</span>
						
						<a class="pull-right" href="?mod=ebook">Buka<i class="fa fa-folder" style="margin-left: 10px;"></i></a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="info-box bg-blue" style="background-color: #00C0EF !important;">
					<span class="info-box-icon">
						<i class="fa fa-file-video-o"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Video</span>
						<span class="info-box-number" style="margin-bottom: 10px;"><?=$jumlahVideo;?> Tersedia</span>
						
						<a class="pull-right" href="?mod=video">Buka<i class="fa fa-folder" style="margin-left: 10px;"></i></a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="info-box bg-blue" style="background-color: #00A65A !important;">
					<span class="info-box-icon">
						<i class="fa fa-flag-checkered"></i>
					</span>
					<div class="info-box-content">
						<span class="info-box-text">Ujian</span>
						<span class="info-box-number" style="margin-bottom: 10px;"><?=$jumlahUjian;?> Tersedia</span>
						
						<a class="pull-right" href="?mod=ujian">Kerjakan<i class="fa fa-play" style="margin-left: 10px;"></i></a>
					</div>
				</div>
			</div>
        </div>
		<div class="modal fade" id="form" role="dialog">
			<div class="modal-dialog">
				<div id="formContent" class="modal-content"></div>
			</div>
		</div>	
	</section>
</div>