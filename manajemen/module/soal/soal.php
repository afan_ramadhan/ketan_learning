<?php
$nama_menu = "soal";
$hakAkses = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.r, hak_akses.w, hak_akses.u, hak_akses.d FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu'");
$getHakAkses = mysql_fetch_array($hakAkses);

$r = ($getHakAkses['r'] == 1 ? "" : "display: none;");
$w = ($getHakAkses['w'] == 1 ? "" : "display: none;");
$u = ($getHakAkses['u'] == 1 ? "" : "display: none;");
$d = ($getHakAkses['d'] == 1 ? "" : "display: none;");

$nama_menu_ = "paket_soal";
$hakAkses_ = mysql_query("SELECT user.id AS id_user, level.id AS id_level, hak_akses.id_menu, menu.nama_menu, hak_akses.s FROM user LEFT JOIN level ON user.id_level = level.id RIGHT JOIN hak_akses ON level.id = hak_akses.id_level LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE user.id = '$_SESSION[id]' AND nama_menu = '$nama_menu_'");
$getHakAkses_ = mysql_fetch_array($hakAkses_);

$s_ = ($getHakAkses_['s'] == 0 ? "WHERE id <> '$_GET[id]' AND ditambah_oleh = '$_SESSION[username]'" : "WHERE id <> '$_GET[id]'");

if($getHakAkses['r'] == 1)
{
?>

	<script>
		//Ganti Title
		function GantiTitle()
		{
			document.title="<?=$lihat_konfigurasi['nama_aplikasi'];?> <?=$lihat_konfigurasi['versi'];?> Manajemen | Soal";
		}
		GantiTitle();
		
		//Tampil Database
		$(document).ready(function(){
			$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
		})
		
		//Tambah Data
		function tambahData()
		{
			var mod = "tambahData";
			$.ajax({
				type	: "POST",
				url		: "module/soal/soal_form.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Simpan Data
		function simpanData()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanData");
			data.append("id_paket_soal", "<?=$_GET['id']?>");
			data.append("bab", $("#bab").val());
			data.append("pertanyaan", tinymce.get("pertanyaan").getContent());
			data.append("file", $('#file')[0].files[0]);
			data.append("pilihan_1", $("#pilihan_1").val());
			data.append("pilihan_2", $("#pilihan_2").val());
			data.append("pilihan_3", $("#pilihan_3").val());
			data.append("pilihan_4", $("#pilihan_4").val());
			data.append("pilihan_5", $("#pilihan_5").val());
			data.append("jawaban", $("#jawaban").val());
			data.append("pembahasan", tinymce.get("pembahasan").getContent());
			$.ajax({
				type		: "POST",
				url			: "module/soal/soal_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
				}
			})
		}
		
		//Edit Data
		function editData(id)
		{
			var mod = "editData";
			var id = id;
			$.ajax({
				type	: "POST",
				url		: "module/soal/soal_form.php",
				data	: "mod=" + mod +
						  "&id=" + id,
				success: function(html)
				{
					$("#formContent").html(html);
					$("#form").modal();
				}
			})
		}
		
		//Perbarui Data
		function perbaruiData(id)
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "perbaruiData");
			data.append("id", id);
			data.append("id_paket_soal", "<?=$_GET['id']?>");
			data.append("bab", $("#bab").val());
			data.append("pertanyaan", tinymce.get("pertanyaan").getContent());
			data.append("file", $('#file')[0].files[0]);
			data.append("pilihan_1", $("#pilihan_1").val());
			data.append("pilihan_2", $("#pilihan_2").val());
			data.append("pilihan_3", $("#pilihan_3").val());
			data.append("pilihan_4", $("#pilihan_4").val());
			data.append("pilihan_5", $("#pilihan_5").val());
			data.append("jawaban", $("#jawaban").val());
			data.append("pembahasan", tinymce.get("pembahasan").getContent());
			$.ajax({
				type		: "POST",
				url			: "module/soal/soal_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
				}
			})
		}
		
		//Hapus Data
		function hapusData(id)
		{
			var konfirmasi = confirm("Hapus Data?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusData";
				var id = id;
				$.ajax({
					type	: "POST",
					url		: "module/soal/soal_action.php",
					data	: "mod=" + mod +
							  "&id=" + id,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
					}
				})
			}
		}
		
		//Hapus Data Terpilih
		function hapusDataTerpilih()
		{
			var konfirmasi = confirm("Hapus Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "hapusDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				$.ajax({
					type	: "POST",
					url		: "module/soal/soal_action.php",
					data	: "mod=" + mod +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
						$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
					}
				})
			}
		}
		
		//Salin Soal Data Terpilih
		function salinSoalDataTerpilih()
		{
			var konfirmasi = confirm("Salin Soal Data Terpilih?");
			if(konfirmasi)
			{
				mulaiAnimasi();
				var mod = "salinSoalDataTerpilih";
				var data_terpilih = new Array();
				$(".terpilih:checked").each(function(){
					data_terpilih.push($(this).attr("id"));
				});	
				var id_paket_soal = $("#paket_soal").val();
				$.ajax({
					type	: "POST",
					url		: "module/soal/soal_action.php",
					data	: "mod=" + mod +
							  "&id_paket_soal=" + id_paket_soal +
							  "&data_terpilih=" + data_terpilih,
					success: function(html)
					{
						stopAnimasi();
						$("#notifikasi").html(html);
					}
				})
			}
		}
		
		//Tambah Data Import
		function tambahDataImport()
		{
			var mod = "tambahDataImport";
			$.ajax({
				type	: "POST",
				url		: "module/soal/soal_form_import.php",
				data	: "mod=" + mod,
				success: function(html)
				{
					$("#formContent_").html(html);
					$("#form_").modal();
				}
			})
		}
		
		//Simpan Data Import
		function simpanDataImport()
		{
			mulaiAnimasi();
			var data = new FormData();
			data.append("mod", "simpanDataImport");
			data.append("id_paket_soal", "<?=$_GET['id']?>");
			data.append("file", $('#file')[0].files[0]);
			$.ajax({
				type		: "POST",
				url			: "module/soal/soal_action.php",
				data		: data,
				cache		: false,
				processData	: false,
				contentType	: false,
				success: function(html)
				{
					stopAnimasi();
					$("#notifikasi").html(html);
					$("#tampilData").load("module/soal/soal_database.php?id=<?=$_GET['id'];?>");
				}
			})
		}
	</script>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Soal</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li><a href="index.php?mod=paket_soal">Paket Soal</a></li>
				<li class="active">Soal</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">Data Soal</h3>
						</div>
						<div class="box-body">
							<button type="button" class="btn btn-primary btn-md" onclick="tambahData()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Tambah Soal</button>
							<button type="button" class="btn btn-primary btn-md" onclick="tambahDataImport()" style="margin-right: 10px; <?=$w;?>"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 10px;"></i>Import Soal</button>
							<button type="button" class="btn btn-default" style="margin-right: 10px; <?=$d;?>">
								<input type="checkbox" id="pilihSemua" style="margin-right: 10px;">Pilih Semua
							</button>
							<button type="button" id="hapusDataTerpilih" class="btn btn-danger" onclick="hapusDataTerpilih()" style="<?=$d;?>">
								<i class="fa fa-trash" aria-hidden="true" style="margin-right: 10px;"></i>Hapus Soal Terpilih</span>
							</button>
							<br style="margin-bottom: 20px; <?=$w;?>"/>
							<select class="form-control" id="paket_soal" style="width: 200px; margin-right: 10px; display: inline; <?=$w;?>" required>
								<?php
								$paketSoal = mysql_query("SELECT * FROM paket_soal $s_ ORDER BY nama_paket_soal");
								while($getPaketSoal = mysql_fetch_array($paketSoal))
								{
									$selected = ($getData['id_paket_soal'] == $getPaketSoal['id'] ? "selected" : "");
								?>
									<option value="<?=$getPaketSoal['id'];?>" <?=$selected;?>><?=$getPaketSoal['nama_paket_soal'];?></option>
								<?php
								}
								?>
							</select>
							<button type="button" id="salinSoalDataTerpilih" class="btn btn-primary" onclick="salinSoalDataTerpilih()" style="<?=$u;?>">
								<i class="fa fa-clone" aria-hidden="true" style="margin-right: 10px;"></i>Salin Soal Terpilih</span>
							</button>
							<div class="modal fade" id="form" role="dialog">
								<div class="modal-dialog modal-lg">
									<div id="formContent" class="modal-content"></div>
									<div id="notifikasi" class="modal-content"></div>
								</div>
							</div>	
							<div class="modal fade" id="form_" role="dialog">
								<div class="modal-dialog">
									<div id="formContent_" class="modal-content"></div>
								</div>
							</div>			
							<br/>
							<br/>  
							<div id="tampilData" class="scrolling" onload="tampilData()" style="padding-bottom: 45px;"></div>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
else
{
?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1>Soal</h1>
			<ol class="breadcrumb">
				<li><a href="index.php"><i class="fa fa-chevron-right" style="margin-right: 10px;"></i>Dashboard</a></li>
				<li><a href="index.php?mod=paket_soal">Paket Soal</a></li>
				<li class="active">Soal</li>
			</ol>
		</section>
		<section class="content container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="box box-warning">
						<div class="box-header with-border">
							<h3 class="box-title">Halaman Tidak Dapat Di Akses</h3>
						</div>
						<div class="box-body">
							<center><img src="images/lock_icon.png" style="width: 50%"/></center>
						</div>
					</div>
				</div>
			</div>
		</section>	
	</div>
	
<?php
}
?>