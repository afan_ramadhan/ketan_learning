<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px;}" : "");
?>

<style>
	<?=$mobileSize;?>
</style>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 0, "desc" ]],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/saran/data.php",
			"aoColumns": [
				{
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "<input type='checkbox' id='" + data + "' class='terpilih' onclick='tampilTombolHapus(" + data + ")'/>";
						
						return aksi;
					}
				},
				null,
				{
					"mData": "2",
					"mRender": function(data)
					{	
						if(data == 1)
						{
							return "Kurang Memuaskan";
						}
						else if(data == 2)
						{
							return "Cukup Memuaskan";
						}
						else if(data == 3)
						{
							return "Memuaskan";
						}
						else if(data == 4)
						{
							return "Sangat Memuaskan";
						}
					}
				},
				{
					"bSortable": false,
					"mData": "0",
					"mRender": function(data, type, full)
					{	
						var aksi = "";
						aksi += "<div class='dropdown'><button class='btn btn-default btn-sm dropdown-toggle' type='button' data-toggle='dropdown'>Action.. <span class='caret'></span></button><ul class='dropdown-menu' style='padding: 10px;'>";
						aksi += "<li><button type='button' class='btn btn-warning btn-sm' onclick='editData(" + data + ")' style='margin-bottom: 10px;'><i class='fa fa-pencil' aria-hidden='true' style='margin-right: 10px;'></i>Edit</button></li>";
						aksi += "<li><button type='button' class='btn btn-danger btn-sm' onclick='hapusData(" + data + ")'><i class='fa fa-trash' aria-hidden='true' style='margin-right: 10px;'></i>Hapus</button></li>";
						aksi += "</ul></div>";
						
						return aksi;
					}
				}
			]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
	
	//Disable Tombol Hapus Terpilih
	$(document).ready(function(){
		$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
	});
	
	//Select All
	$(document).ready(function(){
		$('#pilihSemua').change(function(){
			if($('#pilihSemua').is(':checked'))
			{
				$('.terpilih').prop('checked', true);
				$('#hapusDataTerpilih').removeClass("disabled").prop('disabled', false);
			}
			else
			{
				$('.terpilih').prop('checked', false);
				$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
			}
		});
	});
	
	//Tampil Tombol Hapus Terpilih
	var banyaknya = 0;
	
	function tampilTombolHapus(id)
	{
		var terpilih = $("#"+id+":checked").val();
		
		if(terpilih == "on")
		{
			banyaknya += 1;
		}
		else
		{
			banyaknya -= 1;
		}
		
		if(banyaknya > 0)
		{
			$('#hapusDataTerpilih').removeClass("disabled").prop('disabled', false);
		}
		else
		{
			$('#hapusDataTerpilih').addClass("disabled").prop('disabled', true);
		}
	}
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<th style="width: 1px;">#</th>
			<th>Saran</th>
			<th>Penilaian Aplikasi</th>
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<?php
	if($mobile == false)
	{
	?>
		<tfoot>
			<tr>
				<th>#</th>
				<th class="filter">Saran</th>
				<th class="filter">Penilaian Aplikasi</th>
				<th>#</th>
			</tr>
		</tfoot>
	<?php
	}
	?>
	
</table>