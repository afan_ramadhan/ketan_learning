<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "ebook";

if($_POST['mod']=="simpanData")
{
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_ebook = mysql_real_escape_string($_POST['nama_ebook']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$publikasi = $_POST['publikasi'];
	
	if(!empty($_FILES['file']['name']))
	{
		if(empty($_POST['nama_ebook']))
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Ebook Tidak Boleh Kosong!');
			</script>
			";
		}
		else
		{
			$simpanData = mysql_query("INSERT INTO $nama_tabel (id_kelas, id_mapel, nama_ebook, keterangan, publikasi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$id_mapel', '$nama_ebook', '$keterangan', '$publikasi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
			if(!empty($_FILES['file']['name']))
			{
				function UploadFile($file)
				{
					$vdir_upload = "../../ebook/";
					$vfile_upload = $vdir_upload . $file;
					
					move_uploaded_file($_FILES["file"]["tmp_name"], $vfile_upload);
				}
					
				$lokasi_file = $_FILES['file']['tmp_name'];
				$nama_file = $_FILES['file']['name'];
				$acak = rand(1,99);
				$nama_file_unik = $acak . $nama_file;
				UploadFile($nama_file_unik);
				
				$ebook = mysql_fetch_array(mysql_query("SELECT MAX(id) id FROM ebook"));

				mysql_query("UPDATE ebook SET file = '$nama_file_unik' WHERE id = '$ebook[id]'");
			}
			
			if($simpanData)
			{
				$aktivitas = "Tambah Ebook";
				$keterangan = mysql_real_escape_string("Menambahkan '$nama_ebook' Pada Tabel '$nama_tabel'");
				$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Ditambahkan!');
					$('#form').modal('hide');
				</script>
				";
			}
			else
			{
				echo "
				<script>
					alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
				</script>
				";
			}
		}
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_ebook = mysql_real_escape_string($_POST['nama_ebook']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$publikasi = $_POST['publikasi'];
	
	if(empty($_POST['nama_ebook']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Ebook Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET id_kelas = '$id_kelas', id_mapel = '$id_mapel', nama_ebook = '$nama_ebook', keterangan = '$keterangan', publikasi = '$publikasi', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		if(!empty($_FILES['file']['name']))
		{
			$ambilEbook = mysql_fetch_array(mysql_query("SELECT * FROM ebook WHERE id = '$id'"));
		
			if($ambilEbook['file'] != "")
			{
				$cekFile = mysql_num_rows(mysql_query("SELECT * FROM ebook WHERE file = '$ambilEbook[file]'"));
				if($cekFile == 1)
				{
					unlink ("../../ebook/$ambilEbook[file]");		
				}
			}
		
			function UploadFile($file)
			{
				$vdir_upload = "../../ebook/";
				$vfile_upload = $vdir_upload . $file;
				
				move_uploaded_file($_FILES["file"]["tmp_name"], $vfile_upload);
			}
				
			$lokasi_file = $_FILES['file']['tmp_name'];
			$nama_file = $_FILES['file']['name'];
			$acak = rand(1,99);
			$nama_file_unik = $acak . $nama_file;
			UploadFile($nama_file_unik);
			
			mysql_query("UPDATE ebook SET file = '$nama_file_unik' WHERE id = '$id'");
		}
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Ebook";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_ebook' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="simpanSalinanData")
{
	$id_kelas = $_POST['id_kelas'];
	$id_mapel = $_POST['id_mapel'];
	$nama_ebook = $_POST['nama_ebook'];
	$keterangan = $_POST['keterangan'];
	$file = $_POST['file'];
	$publikasi = $_POST['publikasi'];
	
	if(empty($_POST['nama_ebook']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Ebook Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_kelas, id_mapel, nama_ebook, keterangan, file, publikasi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_kelas', '$id_mapel', '$nama_ebook', '$keterangan', '$file', '$publikasi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Ebook";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_ebook' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
					
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_ebook FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_ebook'];
	
	$ambilEbook = mysql_fetch_array(mysql_query("SELECT * FROM ebook WHERE id = '$id'"));
		
	if($ambilEbook['file'] != "")
	{
		$cekFile = mysql_num_rows(mysql_query("SELECT * FROM ebook WHERE file = '$ambilEbook[file]'"));
		if($cekFile == 1)
		{
			unlink ("../../ebook/$ambilEbook[file]");		
		}
	}
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
	$hapusPengumuman = mysql_query("DELETE FROM pengumuman WHERE jenis_pengumuman = '1' AND jenis_materi = '1' AND id_materi = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Ebook";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$dataEbook = mysql_query("SELECT * FROM ebook WHERE id IN ($data_terpilih)");
	while($ambilEbook = mysql_fetch_array($dataEbook))
	{
		if($ambilEbook['file'] != "")
		{
			$cekFile = mysql_num_rows(mysql_query("SELECT * FROM ebook WHERE file = '$ambilEbook[file]'"));
			if($cekFile == 1)
			{
				unlink ("../../ebook/$ambilEbook[file]");		
			}
			mysql_query("UPDATE ebook SET file = '' WHERE id = '$ambilEbook[id]'");
		}
	}
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
	$hapusPengumuman = mysql_query("DELETE FROM pengumuman WHERE jenis_pengumuman = '1' AND jenis_materi = '1' AND id_materi IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Ebook Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Ebook Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="publikasiDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$publikasiDataTerpilih = mysql_query("UPDATE $nama_tabel SET publikasi = 'Y', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($publikasiDataTerpilih)
	{
		$aktivitas = "Publikasi Ebook Terpilih";
		$keterangan = mysql_real_escape_string("Mempublikasikan Ebook Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="sembunyikanDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$sembunyikanDataTerpilih = mysql_query("UPDATE $nama_tabel SET publikasi = 'N', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id IN ($data_terpilih)");
	
	if($sembunyikanDataTerpilih)
	{
		$aktivitas = "Sembunyikan Ebook Terpilih";
		$keterangan = mysql_real_escape_string("Menyembunyikan Ebook Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
	
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Ebook Diperbarui!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui');
		</script>
		";
	}
}

if($_POST['mod']=="simpanPengumuman")
{
	$id_materi = $_POST['id_ebook'];
	$nama_pengumuman = $_POST['nama_pengumuman'];
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tanggal_mulai = $_POST['tanggal_mulai'];
	$tanggal_selesai = $_POST['tanggal_selesai'];
	$penerima = $_POST['penerima'];
	$id_rombel = $_POST['id_rombel'];
	
	if(empty($_POST['nama_pengumuman']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Pengumuman Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$cekPengumuman = mysql_num_rows(mysql_query("SELECT * FROM pengumuman WHERE tanggal_mulai = '$tanggal_mulai' AND penerima = '$penerima' AND id_rombel = '$id_rombel' AND jenis_pengumuman = '1' AND jenis_materi = '1' AND id_materi = '$id_materi'"));
		if($cekPengumuman > 0)
		{
			$hapusData = mysql_query("DELETE FROM pengumuman WHERE tanggal_mulai = '$tanggal_mulai' AND penerima = '$penerima' AND id_rombel = '$id_rombel' AND jenis_pengumuman = '1' AND jenis_materi = '1' AND id_materi = '$id_materi'");
		}
		
		$simpanData = mysql_query("INSERT INTO pengumuman (nama_pengumuman, keterangan, tanggal_mulai, tanggal_selesai, penerima, id_rombel, jenis_pengumuman, jenis_materi, id_materi, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_pengumuman', '$keterangan', '$tanggal_mulai', '$tanggal_selesai', '$penerima', '$id_rombel', '1', '1', '$id_materi', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Pengumuman";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_pengumuman' Pada Tabel 'pengumuman'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Ditambahkan!');
				$('#form__').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}
?>