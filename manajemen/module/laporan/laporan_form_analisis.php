<?php
session_start();
include "../../config/database.php";

if($_POST['mod']=="lihatAnalisis")
{
	$id_ujian = $_POST['id_ujian'];
	$id_siswa = $_POST['id_siswa'];
	
	$data = mysql_query("SELECT * FROM ujian WHERE id = '$id_ujian'");
	$getData = mysql_fetch_array($data);
	
	function abjad($jawaban)
	{
		if($jawaban == 1)
		{
			return "A";
		}
		else if($jawaban == 2)
		{
			return "B";
		}
		else if($jawaban == 3)
		{
			return "C";
		}
		else if($jawaban == 4)
		{
			return "D";
		}
		else if($jawaban == 5)
		{
			return "E";
		}
	}
	
	function keterangan($jawaban, $jawaban_siswa)
	{
		if($jawaban == $jawaban_siswa)
		{
			return "<p class='text-success'>Benar</p>";
		}
		else
		{
			return "<p class='text-danger'>Salah</p>";
		}
	}
}
?>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		var table = $('.data_custom_2').DataTable({
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			//"order": [[ 1, "asc" ]],
			"dom": 'lBfrtip',
        	"buttons": [
				'colvis',
				{
                	"extend": 'print',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'excel',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	},
				{
                	"extend": 'pdf',
                	"exportOptions": {
                    	"columns": ':visible'
                	}
            	}
       		]
		});
	});
</script>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">Analisis Laporan</h4>
</div>
<div class="modal-body">

	<div class="scrolling">
		<table class="table table-bordered table-hover data_custom_2">

			<thead>
				<tr>
					<th style="width: 1px;">No.</th>
					<th>Bab</th>
					<th style="width: 400px;">Pertanyaan</th>
					<th>Pilihan Ganda</th>
					<th style="width: 400px;">Pembahasan</th>
					<th>Jawaban Benar</th>
					<th>Jawaban Siswa</th>
					<th>Keterangan</th>
				</tr>
			</thead>

			<tbody>

				<?php
				$x = 1;
				$data = mysql_query("SELECT soal.*, kantong_jawaban.jawaban_siswa FROM soal LEFT JOIN kantong_jawaban ON soal.id = kantong_jawaban.id_soal AND kantong_jawaban.id_ujian = '$getData[id]' AND kantong_jawaban.id_siswa = '$id_siswa' WHERE soal.id_paket_soal = '$getData[id_paket_soal]'");
				while($getData = mysql_fetch_array($data))
				{
					echo "
					<tr>
						<td align='center'>$x</td>
						<td>$getData[bab]</td>
						<td style='white-space:normal;'>
							$getData[pertanyaan]";
							if($getData['file'] != "")
							{
								echo "
								<audio controls style='width: 100%'>
									<source src='manajemen/uploads/$getData[file]' type='audio/mpeg'>
								</audio>";
							}
							echo "
						</td>
						<td style='white-space:normal;'>
							<table>
								".($getData['pilihan_1'] != "" ? "<tr><td style='vertical-align: top; padding-right: 5px;'>A.</td><td>$getData[pilihan_1]</td></tr>" : "")."
								".($getData['pilihan_2'] != "" ? "<tr><td style='vertical-align: top; padding-right: 5px;'>B.</td><td>$getData[pilihan_2]</td></tr>" : "")."
								".($getData['pilihan_3'] != "" ? "<tr><td style='vertical-align: top; padding-right: 5px;'>C.</td><td>$getData[pilihan_3]</td></tr>" : "")."
								".($getData['pilihan_4'] != "" ? "<tr><td style='vertical-align: top; padding-right: 5px;'>D.</td><td>$getData[pilihan_4]</td></tr>" : "")."
								".($getData['pilihan_5'] != "" ? "<tr><td style='vertical-align: top; padding-right: 5px;'>E.</td><td>$getData[pilihan_5]</td></tr>" : "")."
							</table>
						</td>
						<td style='white-space:normal;'>
							$getData[pembahasan]
						</td>
						<td>".abjad($getData['jawaban'])."</td>
						<td>".abjad($getData['jawaban_siswa'])."</td>
						<td>".keterangan($getData['jawaban'], $getData['jawaban_siswa'])."</td>
					</tr>";
					$x++;
				}
				?>
				
			</tbody>
			
		</table>
	</div>
	
</div>
<div class="modal-footer">
</div>