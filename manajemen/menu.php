<?php
$mod = (isset($_GET['mod']) ? $_GET['mod']: "");

$levelUser = mysql_query("SELECT id_level FROM user WHERE id = '$_SESSION[id]'");
$ambilLevelUser = mysql_fetch_array($levelUser);

$menuAkses = mysql_query("SELECT menu.nama_menu, hak_akses.r FROM hak_akses LEFT JOIN menu ON hak_akses.id_menu = menu.id WHERE id_level = '$ambilLevelUser[id_level]'");
while($ambilMenuAkses = mysql_fetch_array($menuAkses))
{
	$view[$ambilMenuAkses['nama_menu']] = $ambilMenuAkses['r'];
}
?>

<aside class="main-sidebar">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<?php
				if($ambil_data_adminweb['gambar']=="")
				{
					echo "<img class='img-circle' src='images/user_kosong.jpg'/>";
				}
				else
				{
					echo "<img class='img-circle' src='images/user/$ambil_data_adminweb[gambar]'/>";
				}
				?>
			</div>
			<div class="pull-left info">
				<p><?=$ambil_data_adminweb['nama_lengkap'];?></p>
				<small><?=$ambil_data_adminweb['nama_level'];?></small>
			</div>
		</div>
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header" style="text-align: center;"><?=strtoupper($lihat_konfigurasi['nama_aplikasi']);?> MENU</li>
			<li <?php if(!isset($_GET['mod'])){echo "class='active'";} ?>><a class="loading" href="index.php"><i class="fa fa-dashboard" style="margin-right: 10px;"></i><span>Dashboard</span></a></li>
			<li class="treeview <?php if($mod == "kelas" or $mod == "mapel" or $mod == "rombel" or $mod == "jadwal_rombel" or $mod == "siswa"){echo "active menu-open";} ?>" style="<?php if($view['kelas'] == 0 and $view['mapel'] == 0 and $view['rombel'] == 0 and $view['jadwal_rombel'] == 0 and $view['siswa'] == 0){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-briefcase" style="margin-right: 10px;"></i><span>Master</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "kelas"){echo "class='active'";} ?> style="<?php if($view['kelas'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=kelas"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Kelas</span></a></li>
					<li <?php if($mod == "mapel"){echo "class='active'";} ?> style="<?php if($view['mapel'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=mapel"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Mapel</span></a></li>
					<li <?php if($mod == "rombel"){echo "class='active'";} ?> style="<?php if($view['rombel'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=rombel"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Rombel</span></a></li>
					<li <?php if($mod == "jadwal_rombel"){echo "class='active'";} ?> style="<?php if($view['jadwal_rombel'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=jadwal_rombel"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Jadwal Rombel</span></a></li>
					<li <?php if($mod == "siswa"){echo "class='active'";} ?> style="<?php if($view['siswa'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=siswa"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Siswa</span></a></li>
				</ul>
			</li>
			<li class="treeview <?php if($mod == "absensi" or $mod == "ebook" or $mod == "video" or $mod == "paket_soal" or $mod == "soal" or $mod == "ujian" or $mod == "suspect"){echo "active menu-open";} ?>" style="<?php if($view['absensi'] == 0 and $view['ebook'] == 0 and $view['video'] == 0 and $view['paket_soal'] == 0 and $view['ujian'] == 0 and $view['suspect'] == 0){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-graduation-cap" style="margin-right: 10px;"></i><span>Pembelajaran</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "absensi"){echo "class='active'";} ?> style="<?php if($view['absensi'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=absensi"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Absensi</span></a></li>
					<li <?php if($mod == "ebook"){echo "class='active'";} ?> style="<?php if($view['ebook'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=ebook"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Ebook</span></a></li>
					<li <?php if($mod == "video"){echo "class='active'";} ?> style="<?php if($view['video'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=video"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Video</span></a></li>
					<li <?php if($mod == "paket_soal" or $mod == "soal"){echo "class='active'";} ?> style="<?php if($view['paket_soal'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=paket_soal"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Paket Soal</span></a></li>
					<li <?php if($mod == "ujian"){echo "class='active'";} ?> style="<?php if($view['ujian'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=ujian"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Ujian</span></a></li>
					<li <?php if($mod == "suspect"){echo "class='active'";} ?> style="<?php if($view['suspect'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=suspect"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Suspect</span></a></li>
				</ul>
			</li>
			<li <?php if($mod == "laporan"){echo "class='active'";} ?> style="<?php if($view['laporan'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=laporan"><i class="fa fa-file-text" style="margin-right: 10px;"></i><span>Laporan</span></a></li>
			<li <?php if($mod == "kegiatan"){echo "class='active'";} ?> style="<?php if($view['kegiatan'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=kegiatan"><i class="fa fa-calendar" style="margin-right: 10px;"></i><span>Kegiatan</span></a></li>
			<li <?php if($mod == "pengumuman"){echo "class='active'";} ?> style="<?php if($view['pengumuman'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=pengumuman"><i class="fa fa-bullhorn" style="margin-right: 10px;"></i><span>Pengumuman</span></a></li>
			<li class="treeview <?php if($mod == "level" or $mod == "user" or $mod == "mapel_user"){echo "active menu-open";} ?>" style="<?php if($view['level'] == 0 and $view['user'] == 0 and $view['mapel_user'] == 0){echo "display: none;";} ?>">
				<a href="#">
					<i class="fa fa-lock" style="margin-right: 10px;"></i><span>Manajemen Akses</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">	
					<li <?php if($mod == "level"){echo "class='active'";} ?> style="<?php if($view['level'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=level"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Level</span></a></li>
					<li <?php if($mod == "user"){echo "class='active'";} ?> style="<?php if($view['user'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=user"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>User</span></a></li>
					<li <?php if($mod == "mapel_user"){echo "class='active'";} ?> style="<?php if($view['mapel_user'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=mapel_user"><i class="fa fa-circle-o" style="margin-right: 10px;"></i><span>Mapel User</span></a></li>
				</ul>
			</li>
			<li <?php if($mod == "saran"){echo "class='active'";} ?> style="<?php if($view['saran'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=saran"><i class="fa fa-commenting-o" style="margin-right: 10px;"></i><span>Saran</span></a></li>
			<li <?php if($mod == "riwayat"){echo "class='active'";} ?> style="<?php if($view['riwayat'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=riwayat"><i class="fa fa-clock-o" style="margin-right: 10px;"></i><span>Riwayat Aktivitas</span></a></li>
			<li <?php if($mod == "konfigurasi"){echo "class='active'";} ?> style="<?php if($view['konfigurasi'] == 0){echo "display: none;";} ?>"><a class="loading" href="index.php?mod=konfigurasi"><i class="fa fa-cogs" style="margin-right: 10px;"></i><span>Konfigurasi</span></a></li>
		</ul>
	</section>
</aside>
