<?php
include "../../config/database.php";

if($_POST['mod']=="editData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT * FROM mapel WHERE id = '$id'");
	$getData = mysql_fetch_array($data);
}
else
{
	$id = 0;
	$getData['id_kelas'] = 0;
}

$disabled = (!empty($_GET['id']) ? "disabled" : "");
$id_kelas = (!empty($_GET['id']) ? $_GET['id'] : $getData['id_kelas']);
?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h4 class="modal-title">
		<?php if($_POST['mod'] == "editData"){echo "Edit Mapel";}else{echo "Tambah Mapel";} ?>
	</h4>
</div>
<div class="modal-body">
	<table class="table table-hover">
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelas</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="id_kelas" required <?=$disabled;?>>
					<?php
					$kelas = mysql_query("SELECT * FROM kelas ORDER BY nama_kelas");
					while($getKelas = mysql_fetch_array($kelas))
					{
						$selected = ($id_kelas == $getKelas['id'] ? "selected" : "");
					?>
						<option value="<?=$getKelas['id'];?>" <?=$selected;?>><?=$getKelas['nama_kelas'];?></option>
					<?php
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kode Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kode_mapel" maxlength="20" value="<?php if($_POST['mod']=="editData"){echo $getData['kode_mapel'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Nama Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="nama_mapel" maxlength="100" value="<?php if($_POST['mod']=="editData"){echo $getData['nama_mapel'];} ?>"/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">KKM</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<input type="text" class="form-control" id="kkm" maxlength="2" value="<?php if($_POST['mod']=="editData"){echo $getData['kkm'];} ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required/>
			</td>
		</tr>
		<tr>
			<td style="border: none;">
				<label class="control-label">Kelompok Mapel</label>
			</td>
			<td style="border: none;"><label class="control-label">:</label></td>
			<td style="border: none;">
				<select class="form-control" id="kelompok_mapel" required>
					<option value="1" <?php if($_POST['mod']=="editData"){echo ($getData['kelompok_mapel'] == 1 ? "selected" : "");} ?>>Kelompok A</option>
					<option value="2" <?php if($_POST['mod']=="editData"){echo ($getData['kelompok_mapel'] == 2 ? "selected" : "");} ?>>Kelompok B</option>
					<option value="3" <?php if($_POST['mod']=="editData"){echo ($getData['kelompok_mapel'] == 3 ? "selected" : "");} ?>>Kelompok C</option>
				</select>
			</td>
		</tr>
	</table>
</div>
<div class="modal-footer">
	<?php
	if($_POST['mod']=="editData")
	{
		echo "<button type='button' class='btn btn-success' id='perbaruiData' onclick='perbaruiData($getData[id])'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Perbarui</button>";
	}
	else
	{
		echo "<button type='button' class='btn btn-success' id='simpanData' onclick='simpanData()'><i class='fa fa-save' aria-hidden='true' style='margin-right: 10px;'></i>Simpan</button>";
	}
	?>
</div>