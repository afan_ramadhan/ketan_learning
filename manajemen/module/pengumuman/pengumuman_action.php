<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "pengumuman";

if($_POST['mod']=="simpanData")
{
	$nama_pengumuman = mysql_real_escape_string($_POST['nama_pengumuman']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tanggal_mulai = mysql_real_escape_string($_POST['tanggal_mulai']);
	$tanggal_selesai = mysql_real_escape_string($_POST['tanggal_selesai']);
	$penerima = $_POST['penerima'];
	$id_rombel = 0;
	
	if($penerima == 3)
	{
		$id_rombel = $_POST['id_rombel'];
	}
	
	if(empty($_POST['nama_pengumuman']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Pengumuman Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (nama_pengumuman, keterangan, tanggal_mulai, tanggal_selesai, penerima, id_rombel, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$nama_pengumuman', '$keterangan', '$tanggal_mulai', '$tanggal_selesai', '$penerima', '$id_rombel', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Pengumuman";
			$keterangan = mysql_real_escape_string("Menambahkan '$nama_pengumuman' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$nama_pengumuman = mysql_real_escape_string($_POST['nama_pengumuman']);
	$keterangan = mysql_real_escape_string($_POST['keterangan']);
	$tanggal_mulai = mysql_real_escape_string($_POST['tanggal_mulai']);
	$tanggal_selesai = mysql_real_escape_string($_POST['tanggal_selesai']);
	$penerima = $_POST['penerima'];
	$id_rombel = 0;
	
	if($penerima == 3)
	{
		$id_rombel = $_POST['id_rombel'];
	}
	
	if(empty($_POST['nama_pengumuman']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Nama Pengumuman Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_mulai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Mulai Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['tanggal_selesai']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Tanggal Selesai Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET nama_pengumuman = '$nama_pengumuman', keterangan = '$keterangan', tanggal_mulai = '$tanggal_mulai', tanggal_selesai = '$tanggal_selesai', penerima = '$penerima', id_rombel = '$id_rombel', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Pengumuman";
			$keterangan = mysql_real_escape_string("Memperbarui '$nama_pengumuman' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT nama_pengumuman FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['nama_pengumuman'];
	
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Pengumuman";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Pengumuman Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Pengumuman Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pengumuman Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}
?>