<?php
session_start();
include "../../config/database.php";
include "../../libraries/fungsi_waktu.php";

$nama_tabel = "soal";

if($_POST['mod']=="simpanData")
{
	$id_paket_soal = $_POST['id_paket_soal'];
	$bab = mysql_real_escape_string($_POST['bab']);
	$pertanyaan = mysql_real_escape_string($_POST['pertanyaan']);
	$file = (isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "");
	$pilihan_1 = mysql_real_escape_string($_POST['pilihan_1']);
	$pilihan_2 = mysql_real_escape_string($_POST['pilihan_2']);
	$pilihan_3 = mysql_real_escape_string($_POST['pilihan_3']);
	$pilihan_4 = mysql_real_escape_string($_POST['pilihan_4']);
	$pilihan_5 = mysql_real_escape_string($_POST['pilihan_5']);
	$jawaban = $_POST['jawaban'];
	$pembahasan = mysql_real_escape_string($_POST['pembahasan']);
	
	if(empty($_POST['pertanyaan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pertanyaan Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['pilihan_1']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pilhan A Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_paket_soal, bab, pertanyaan, pilihan_1, pilihan_2, pilihan_3, pilihan_4, pilihan_5, jawaban, pembahasan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_paket_soal', '$bab', '$pertanyaan', '$pilihan_1', '$pilihan_2', '$pilihan_3', '$pilihan_4', '$pilihan_5', '$jawaban', '$pembahasan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
		
		if(!empty($_FILES['file']['name']))
		{
			function UploadFile($file)
			{
				$vdir_upload = "../../uploads/";
				$vfile_upload = $vdir_upload . $file;
				
				move_uploaded_file($_FILES["file"]["tmp_name"], $vfile_upload);
			}
				
			$lokasi_file = $_FILES['file']['tmp_name'];
			$nama_file = $_FILES['file']['name'];
			$acak = rand(1,99);
			$nama_file_unik = $acak . $nama_file;
			UploadFile($nama_file_unik);
			
			$soal = mysql_fetch_array(mysql_query("SELECT MAX(id) id FROM soal"));

			mysql_query("UPDATE soal SET file = '$nama_file_unik' WHERE id = '$soal[id]'");
		}
			
		if($simpanData)
		{
			$aktivitas = "Tambah Soal";
			$keterangan = mysql_real_escape_string("Menambahkan '$bab' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Ditambahkan!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menambahkan!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="perbaruiData")
{
	$id = $_POST['id'];
	$id_paket_soal = $_POST['id_paket_soal'];
	$bab = mysql_real_escape_string($_POST['bab']);
	$pertanyaan = mysql_real_escape_string($_POST['pertanyaan']);
	$file = (isset($_FILES['file']['name']) ? $_FILES['file']['name'] : "");
	$pilihan_1 = mysql_real_escape_string($_POST['pilihan_1']);
	$pilihan_2 = mysql_real_escape_string($_POST['pilihan_2']);
	$pilihan_3 = mysql_real_escape_string($_POST['pilihan_3']);
	$pilihan_4 = mysql_real_escape_string($_POST['pilihan_4']);
	$pilihan_5 = mysql_real_escape_string($_POST['pilihan_5']);
	$jawaban = $_POST['jawaban'];
	$pembahasan = mysql_real_escape_string($_POST['pembahasan']);
	
	if(empty($_POST['pertanyaan']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pertanyaan Tidak Boleh Kosong!');
		</script>
		";
	}
	else if(empty($_POST['pilihan_1']))
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Pilhan A Tidak Boleh Kosong!');
		</script>
		";
	}
	else
	{	
		$perbaruiData = mysql_query("UPDATE $nama_tabel SET bab = '$bab', pertanyaan = '$pertanyaan', pilihan_1 = '$pilihan_1', pilihan_2 = '$pilihan_2', pilihan_3 = '$pilihan_3', pilihan_4 = '$pilihan_4', pilihan_5 = '$pilihan_5', jawaban = '$jawaban', pembahasan = '$pembahasan', tanggal_diperbarui = '$tanggal_sekarang', jam_diperbarui = '$jam_sekarang', diperbarui_oleh = '$_SESSION[username]' WHERE id = '$id'");
		
		if(!empty($_FILES['file']['name']))
		{
			$ambilSoal = mysql_fetch_array(mysql_query("SELECT * FROM soal WHERE id = '$id'"));
		
			if($ambilSoal['file'] != "")
			{
				unlink ("../../uploads/$ambilSoal[file]");		
			}
		
			function UploadFile($file)
			{
				$vdir_upload = "../../uploads/";
				$vfile_upload = $vdir_upload . $file;
				
				move_uploaded_file($_FILES["file"]["tmp_name"], $vfile_upload);
			}
				
			$lokasi_file = $_FILES['file']['tmp_name'];
			$nama_file = $_FILES['file']['name'];
			$acak = rand(1,99);
			$nama_file_unik = $acak . $nama_file;
			UploadFile($nama_file_unik);
			
			mysql_query("UPDATE soal SET file = '$nama_file_unik' WHERE id = '$id'");
		}
			
		if($perbaruiData)
		{
			$aktivitas = "Perbarui Soal";
			$keterangan = mysql_real_escape_string("Memperbarui '$bab' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
			
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Diperbarui!');
				$('#form').modal('hide');
			</script>
			";
		}
		else
		{
			echo "
			<script>
				alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Memperbarui!');
			</script>
			";
		}
	}
}

if($_POST['mod']=="hapusData")
{
	$id = $_POST['id'];
	
	$data = mysql_query("SELECT bab FROM $nama_tabel WHERE id = '$id'");
	$ambilData = mysql_fetch_array($data);
	$field = $ambilData['bab'];
	
	$ambilSoal = mysql_fetch_array(mysql_query("SELECT * FROM soal WHERE id = '$id'"));
		
	if($ambilSoal['file'] != "")
	{
		$cekFile = mysql_num_rows(mysql_query("SELECT * FROM soal WHERE file = '$ambilSoal[file]'"));
		if($cekFile == 1)
		{
			unlink ("../../uploads/$ambilSoal[file]");
		}
	}
			
	$hapusData = mysql_query("DELETE FROM $nama_tabel WHERE id = '$id'");
		
	if($hapusData)
	{
		$aktivitas = "Hapus Soal";
		$keterangan = mysql_real_escape_string("Menghapus '$field' Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="hapusDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	
	$dataSoal = mysql_query("SELECT * FROM soal WHERE id IN ($data_terpilih)");
	while($ambilSoal = mysql_fetch_array($dataSoal))
	{
		if($ambilSoal['file'] != "")
		{
			$cekFile = mysql_num_rows(mysql_query("SELECT * FROM soal WHERE file = '$ambilSoal[file]'"));
			if($cekFile == 1)
			{
				unlink ("../../uploads/$ambilSoal[file]");	
			}
			mysql_query("UPDATE soal SET file = '' WHERE id = '$ambilSoal[id]'");
		}
	}
	
	$hapusDataTerpilih = mysql_query("DELETE FROM $nama_tabel WHERE id IN ($data_terpilih)");
		
	if($hapusDataTerpilih)
	{
		$aktivitas = "Hapus Soal Terpilih";
		$keterangan = mysql_real_escape_string("Menghapus Soal Terpilih Pada Tabel '$nama_tabel'");
		$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Dihapus!');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Gagal Menghapus!');
		</script>
		";
	}
}

if($_POST['mod']=="salinSoalDataTerpilih")
{
	$data_terpilih = $_POST['data_terpilih'];
	$id_paket_soal = $_POST['id_paket_soal'];
	
	$dataSoal = mysql_query("SELECT * FROM soal WHERE id IN ($data_terpilih)");
	while($ambilSoal = mysql_fetch_array($dataSoal))
	{
		$simpanData = mysql_query("INSERT INTO $nama_tabel (id_paket_soal, bab, pertanyaan, file, pilihan_1, pilihan_2, pilihan_3, pilihan_4, pilihan_5, jawaban, pembahasan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_paket_soal', '$ambilSoal[bab]', '$ambilSoal[pertanyaan]', '$ambilSoal[file]', '$ambilSoal[pilihan_1]', '$ambilSoal[pilihan_2]', '$ambilSoal[pilihan_3]', '$ambilSoal[pilihan_4]', '$ambilSoal[pilihan_5]', '$ambilSoal[jawaban]', '$ambilSoal[pembahasan]', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
			
		if($simpanData)
		{
			$aktivitas = "Tambah Soal";
			$keterangan = mysql_real_escape_string("Menambahkan '$ambilSoal[bab]' Pada Tabel '$nama_tabel'");
			$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
				
			
		}
	}
	
	echo "
	<script>
		alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'Soal Ditambahkan!');
		$('#form').modal('hide');
	</script>
	";
}

if($_POST['mod']=="simpanDataImport")
{
	$id_paket_soal = $_POST['id_paket_soal'];
	
	function jawaban($abjad)
	{
		if($abjad == "A")
		{
			return 1;
		}
		else if($abjad == "B")
		{
			return 2;
		}
		else if($abjad == "C")
		{
			return 3;
		}
		else if($abjad == "D")
		{
			return 4;
		}
		if($abjad == "E")
		{
			return 5;
		}
	}
	
	if(!empty($_FILES['file']['name']))
	{
		$target_dir = "../../uploads/import/";
		$name_file = basename($_FILES['file']['name']) ;
		$target_file = $target_dir . $name_file;
		move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

		require_once "../../libraries/PHPExcel.php";
		require_once "../../libraries/PHPExcel/IOFactory.php";

		$file = $target_dir . $name_file;

		$objPHPExcel = PHPExcel_IOFactory::load($file);
		
		$success = 0;
		$error = 0;
		$keterangan_error = "";
		
		foreach($objPHPExcel->getWorksheetIterator() as $worksheet)
		{
			$totalrow = $worksheet->getHighestRow();
			
			for($row = 2; $row <= $totalrow; $row++)
			{
				$bab = mysql_escape_string($worksheet->getCellByColumnAndRow(0, $row)->getValue());
				$pertanyaan = mysql_escape_string($worksheet->getCellByColumnAndRow(1, $row)->getValue());
				$pilihan_1 = mysql_escape_string($worksheet->getCellByColumnAndRow(2, $row)->getValue());
				$pilihan_2 = mysql_escape_string($worksheet->getCellByColumnAndRow(3, $row)->getValue());
				$pilihan_3 = mysql_escape_string($worksheet->getCellByColumnAndRow(4, $row)->getValue());
				$pilihan_4 = mysql_escape_string($worksheet->getCellByColumnAndRow(5, $row)->getValue());
				$pilihan_5 = mysql_escape_string($worksheet->getCellByColumnAndRow(6, $row)->getValue());
				$jawaban = mysql_escape_string($worksheet->getCellByColumnAndRow(7, $row)->getValue());
				$pembahasan = mysql_escape_string($worksheet->getCellByColumnAndRow(8, $row)->getValue());
				
				if($pertanyaan == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Pertanyaan Tidak Boleh Kosong.";
				}
				else if($pilihan_1 == "")
				{
					$error++;
					$keterangan_error .= "<br/>Pada Baris $row, Pilihan A Tidak Boleh Kosong.";
				}
				else
				{
					$simpanDataImport = mysql_query("INSERT INTO $nama_tabel (id_paket_soal, bab, pertanyaan, pilihan_1, pilihan_2, pilihan_3, pilihan_4, pilihan_5, jawaban, pembahasan, tanggal_ditambah, jam_ditambah, ditambah_oleh) VALUE ('$id_paket_soal', '$bab', '$pertanyaan', '$pilihan_1', '$pilihan_2', '$pilihan_3', '$pilihan_4', '$pilihan_5', '".jawaban($jawaban)."', '$pembahasan', '$tanggal_sekarang', '$jam_sekarang', '$_SESSION[username]')");
						
					if($simpanDataImport)
					{
						$aktivitas = "Tambah Soal";
						$keterangan = mysql_real_escape_string("Menambahkan '$bab' Pada Tabel '$nama_tabel'");
						$simpanRiwayat = mysql_query("INSERT INTO riwayat (username, tanggal, jam, aktivitas, keterangan) VALUE ('$_SESSION[username]', '$tanggal_sekarang', '$jam_sekarang', '$aktivitas', '$keterangan')");
							
						$success++;
					}
					else
					{
						$error++;
						$keterangan_error .= "<br/>Pada Baris $row, Query Ada Yang Salah.";
					}
				}	
			}
        }
		
		unlink($target_file);
		
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', '$success Soal Ditambahkan, $error Error! $keterangan_error');
			$('#form').modal('hide');
		</script>
		";
	}
	else
	{
		echo "
		<script>
			alertify.alert('KetanWare<i class=\"fa fa-info\" aria-hidden=\"true\" style=\"margin-left: 10px;\"></i>', 'File Tidak Boleh Kosong!');
		</script>
		";
	}
}
?>