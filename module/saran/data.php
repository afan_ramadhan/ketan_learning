<?php
session_start();
include "../../manajemen/config/database.php";

$gaSql['user'] = $username;
$gaSql['password'] = $password;
$gaSql['db'] = $database;
$gaSql['server'] = $host;
	
$gaSql['link'] =  mysql_pconnect($gaSql['server'], $gaSql['user'], $gaSql['password']) or die('Could not open connection to server');
	
mysql_select_db($gaSql['db'], $gaSql['link']) or die('Could not select database ' . $gaSql['db']);

$sTable = "saran";
$aColumns = array('id', 'saran', 'penilaian_aplikasi'); 
$sIndexColumn = "$sTable.id";

$s = "AND $sTable.ditambah_oleh = '$_SESSION[username]'";
$ss = "WHERE $sTable.ditambah_oleh = '$_SESSION[username]'";
	
$sLimit = "";

if(isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1')
{
	$sLimit = "LIMIT " . mysql_real_escape_string($_GET['iDisplayStart']) . ", " . mysql_real_escape_string($_GET['iDisplayLength']);
}
	
if(isset($_GET['iSortCol_0']))
{
	$sOrder = "ORDER BY ";
	for($i = 0; $i < intval($_GET['iSortingCols']); $i++)
	{
		if($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true")
		{
			$sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . " " . mysql_real_escape_string($_GET['sSortDir_' . $i]) . ", ";
		}
	}
		
	$sOrder = substr_replace($sOrder, "", -2);
	if($sOrder == "ORDER BY")
	{
		$sOrder = "";
	}
}
	
$sWhere = "";

if($_GET['sSearch'] != "")
{
	$sKeyword = strtolower(mysql_real_escape_string($_GET['sSearch']));

	if(strpos("memuaskan", $sKeyword) !== false)
	{
		$sValue = "3";
	}
	else if(strpos("cukup memuaskan", $sKeyword) !== false)
	{
		$sValue = "2";
	}
	else if(strpos("kurang memuaskan", $sKeyword) !== false)
	{
		$sValue = "1";
	}
	else if(strpos("sangat memuaskan", $sKeyword) !== false)
	{
		$sValue = "4";
	}
	else
	{
		$sValue = "@";
	}
	
	$sWhere = "WHERE
	$sTable.saran LIKE '%" . mysql_real_escape_string($_GET['sSearch']) . "%' $s OR
	$sTable.penilaian_aplikasi LIKE '%" . $sValue . "%' $s";
}
else
{
	$sWhere = "$ss";
}
	
for($i = 0; $i < count($aColumns); $i++)
{
	if($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '')
	{
		if($sWhere == "")
		{
			$sWhere = "WHERE ";
		}
		else
		{
			$sWhere .= " AND ";
		}
		
		if($i == 2)
		{
			$bKeyword = strtolower(mysql_real_escape_string($_GET['sSearch_' . $i]));
			
			if(strpos("memuaskan", $bKeyword) !== false)
			{
				$bValue = "3";
			}
			else if(strpos("cukup memuaskan", $bKeyword) !== false)
			{
				$bValue = "2";
			}
			else if(strpos("kurang memuaskan", $bKeyword) !== false)
			{
				$bValue = "1";
			}
			else if(strpos("sangat memuaskan", $bKeyword) !== false)
			{
				$bValue = "4";
			}
			else
			{
				$bValue = "@";
			}
			
			$sWhere .= $aColumns[$i] . " LIKE '%" . $bValue . "%' ";
		}
		else
		{
			$sWhere .= $aColumns[$i] . " LIKE '%" . mysql_real_escape_string($_GET['sSearch_' . $i]) . "%' ";
		}
	}
}
	
$sQuery = "
	SELECT SQL_CALC_FOUND_ROWS $sTable.id, $sTable.saran, $sTable.penilaian_aplikasi
	FROM
	$sTable
	$sWhere
	$sOrder
	$sLimit";

$rResult = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
	
$sQuery = "
	SELECT FOUND_ROWS()
	";
	
$rResultFilterTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
$iFilteredTotal = $aResultFilterTotal[0];
	
$sQuery = "
	SELECT COUNT(" . $sIndexColumn . ")
	FROM
	$sTable";
	
$rResultTotal = mysql_query($sQuery, $gaSql['link']) or die(mysql_error());
$aResultTotal = mysql_fetch_array($rResultTotal);
$iTotal = $aResultTotal[0];
	
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);
	
while($aRow = mysql_fetch_array($rResult))
{
	$row = array();
	for($i = 0; $i < count($aColumns); $i++)
	{
		if($aColumns[$i] == "version")
		{
			$row[] = ($aRow[$aColumns[$i]] == "0") ? '-' : $aRow[$aColumns[$i]];
		}
		else if($aColumns[$i] != ' ')
		{
			$row[] = $aRow[$aColumns[$i]];
		}
	}
	
	$output['aaData'][] = $row;
}
	
echo json_encode($output);
?>