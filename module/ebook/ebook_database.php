<?php
session_start();
include "../../manajemen/config/database.php";
include "../../manajemen/libraries/fungsi_user_agent.php";

$mobileSize = ($mobile == true ? "td{font-size: 12px; vertical-align: middle !important;}" : "");
?>

<style>
	<?=$mobileSize;?>
	
	.customHeader {
		font-weight: bold;
		padding-right: 10px;
	}
</style>

<script>
	//Aktifkan DataTables
	$(document).ready(function(){
		
		<?php
		if($mobile == false)
		{
		?>
			$('.data tfoot .filter').each(function(){
				var title = $(this).text();
				$(this).html( '<input type="text"/>' );
			});
		<?php
		}
		?>
		
		var table = $('.data').DataTable({
			//"scrollY": 310,
			//"scrollX": true,
			"paging": true,
			"lengthChange": true,
			"lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"colReorder": true,
			"order": [[ 1, "desc" ]],
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "module/ebook/data.php",
			"aoColumns": [
				<?php
				if($mobile == false)
				{
				?>
					null,
					null,
					null,
					null,
					{
						"mData": "4",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<form action='belajar.php' role='form' method='POST'><input type='hidden' name='jenis_materi' value='ebook'/><input type='hidden' name='id_materi' value='" + data + "'/><button type='submit' class='btn btn-info btn-sm'><i class='fa fa-eye' aria-hidden='true' style='margin-right: 10px;'></i>Lihat</button></form>";
							
							return aksi;
						}
					}
				<?php
				}
				else
				{
				?>
					{
						"mData": "0",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<table>" + data + "</table>";
							
							return aksi;
						}
					},
					{
						"mData": "1",
						"mRender": function(data, type, full)
						{	
							var aksi = "";
							aksi += "<form action='belajar.php' role='form' method='POST'><input type='hidden' name='jenis_materi' value='ebook'/><input type='hidden' name='id_materi' value='" + data + "'/><button type='submit' class='btn btn-info btn-sm'><i class='fa fa-eye' aria-hidden='true' style='margin-right: 10px;'></i>Lihat</button></form>";
							
							return aksi;
						}
					}
				<?php
				}
				?>
			]
		});
		
		<?php
		if($mobile == false)
		{
		?>
			table.columns().every(function(){
				var that = this;
	 
				$('input', this.footer()).on('keyup change',function(){
					if (that.search() !== this.value){
						that
						.search(this.value)
						.draw();
					}
				});
			});
		<?php
		}
		?>
	});
</script>

<table class="table table-bordered table-hover data">

	<thead>
		<tr>
			<?php
			if($mobile == false)
			{
			?>
				<th>Mapel</th>
				<th>Nama Ebook</th>
				<th>Keterangan</th>
				<th>Guru</th-->
			<?php
			}
			else
			{
			?>
				<th>Ebook</th>
			<?php
			}
			?>
			
			<th style="width: 150px;">#</th>
		</tr>
	</thead>

	<tbody></tbody>
	
	<?php
	if($mobile == false)
	{
	?>
		<tfoot>
			<tr>
				<th class="filter">Mapel</th>
				<th class="filter">Nama Ebook</th>
				<th class="filter">Keterangan</th>
				<th class="filter">Guru</th>
				<th>#</th>
			</tr>
		</tfoot>
	<?php
	}
	?>
	
</table>